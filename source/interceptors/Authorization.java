package interceptors;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.hospital.manage.enums.LoginStatus;

import java.util.Map;

public class Authorization extends ActionSupport implements Interceptor
{
    @Override
    public void destroy() {}

    @Override
    public void init() {}

    @Override
    public String intercept(ActionInvocation actionInvocation) throws Exception
    {
        Map<String,Object> session = actionInvocation.getInvocationContext().getSession();
        LoginStatus loginStatus = (LoginStatus)session.get("loginStatus");
        if(loginStatus==null) return ERROR;
        else if(loginStatus==LoginStatus.TRUE) return actionInvocation.invoke();
        else return ERROR;
    }

}

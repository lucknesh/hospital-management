package com.hospital.actionClasses.administrator;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.hospital.manage.database.UserDao;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.users.Receptionist;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;

public class RegisterReceptionist extends ActionSupport implements ModelDriven
{
    private static final String SUCCESS_REDIRECT = "manageUser";
    private static final String ERROR_REDIRECT = "newReceptionistForm";
    private static final String REDIRECT_URL = "redirectUrl";
    private Receptionist receptionist = new Receptionist();

    public String execute()
    {
        try
        {
            UserDao.insertUser(getReceptionist());
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, SUCCESS_REDIRECT);
            return SUCCESS;
        }

        catch (SQLException | DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, ERROR_REDIRECT);
            return ERROR;
        }
    }

    public Object getModel()
    {
        return receptionist;
    }

    public Receptionist getReceptionist() {
        return receptionist;
    }

    public void setReceptionist(Receptionist receptionist) {
        this.receptionist = receptionist;
    }
}

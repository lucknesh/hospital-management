package com.hospital.actionClasses.administrator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.ExaminationDao;
import com.hospital.manage.exception.DaoException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RemoveTest extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "manageTest";
    private static final String ERROR_REDIRECT = "removeTestForm";
    private static final String REDIRECT_URL = "redirectUrl";

    private Integer testId;

    public String execute()
    {
        try
        {
            ExaminationDao.removeTest(getTestId());
            ActionContext.getContext().getValueStack().set(REDIRECT_URL,SUCCESS_REDIRECT);
            return SUCCESS;

        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL,ERROR_REDIRECT);
            return ERROR;
        }
    }

    public Integer getTestId() {
        return testId;
    }

    public void setTestId(Integer testId) {
        this.testId = testId;
    }
}

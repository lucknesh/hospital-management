package com.hospital.actionClasses.administrator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.DoctorDao;
import com.hospital.manage.database.NurseDao;
import com.hospital.manage.enums.UserType;
import com.hospital.manage.exception.DaoException;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UpdateCabinAllocation extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "manageWard";
    private static final String ERROR_REDIRECT = "changeCabinForm";
    private static final String REDIRECT_URL = "redirectUrl";
    private UserType userType;
    private Integer userId;

    public String execute()
    {
            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession();
            UserType userType = UserType.valueOf((String)session.getAttribute("cabinUserKind"));
            Integer userId = Integer.parseInt(request.getParameter("cabinUserId"));
        try {
            if (userType.equals(UserType.DOCTOR))
            {
                ArrayList<String> availableDays = (ArrayList<String>) session.getAttribute("AvailableDays");
                HashMap<String, Integer> dayAndCabin = new HashMap<>();

                for (String day : availableDays)
                {
                    dayAndCabin.put(day, Integer.parseInt(request.getParameter(day + "ID")));
                }

                DoctorDao.reAllocateCabin(dayAndCabin, userId);
            }
            else if (userType.equals(UserType.NURSE))
            {
                Integer cabinId = Integer.parseInt(request.getParameter("cabinId"));
                NurseDao.reAllocateCabin(cabinId, userId);
            }

            ActionContext.getContext().getValueStack().set(REDIRECT_URL, SUCCESS_REDIRECT);
            return SUCCESS;

        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, ERROR_REDIRECT);
            return ERROR;
        }
    }


}

package com.hospital.actionClasses.administrator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.UserDao;
import com.hospital.manage.enums.NurseDuty;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.users.Nurse;
import org.apache.struts2.ServletActionContext;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;

public class RegisterNurse extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "manageUser";
    private static final String ERROR_REDIRECT = "newNurseForm";
    private static final String REDIRECT_URL = "redirectUrl";
    private static final String CABIN_ID = "cabinId";


    public String execute()
    {
            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession();
            Nurse nurse = (Nurse) session.getAttribute("nurse");
            NurseDuty dutyType = nurse.getNurseDuty();

            if (dutyType.equals(NurseDuty.ADMITTANCE_DUTY)) {
                String from = request.getParameter("from") + ":00";
                String to = request.getParameter("to") + ":00";
                nurse.setShiftFromTime(from);
                nurse.setShiftToTime(to);
            }
            else if (dutyType.equals(NurseDuty.CONSULTATION_DUTY))
            {
                Integer cabinId = Integer.parseInt(request.getParameter(CABIN_ID));
                nurse.setCabinId(cabinId);
            }
        try {
                UserDao.insertUser(nurse);
                ActionContext.getContext().getValueStack().set(REDIRECT_URL, SUCCESS_REDIRECT);
                return SUCCESS;
        }
        catch (DaoException|SQLException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, ERROR_REDIRECT);
            return ERROR;
        }
    }
}

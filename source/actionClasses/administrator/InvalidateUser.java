package com.hospital.actionClasses.administrator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.AuthenticatorDao;
import com.hospital.manage.database.UserDao;
import com.hospital.manage.enums.UserType;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.exception.NoDataFoundException;

public class InvalidateUser extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "removeUser";
    private static final String ERROR_REDIRECT = "removeUser";

    private Integer userId;
    private UserType adminType;

    public String execute()
    {

        try
        {
            UserType userType = UserType.valueOf(AuthenticatorDao.getUserType(userId));

            if (adminType.equals(UserType.ADMIN) && (userType.equals(UserType.ADMIN) || userType.equals(UserType.SUPER_ADMIN)))
            {
                ActionContext.getContext().getValueStack().set("redirectUrl", ERROR_REDIRECT);
                return ERROR;
            }
            else if (adminType.equals(UserType.SUPER_ADMIN) && userType.equals(UserType.SUPER_ADMIN))
            {
                ActionContext.getContext().getValueStack().set("redirectUrl", ERROR_REDIRECT);
                return ERROR;
            }
            else
            {
                UserDao.invalidateAccount(userId);
                ActionContext.getContext().getValueStack().set("redirectUrl", SUCCESS_REDIRECT);
                return SUCCESS;
            }
        }
        catch (DaoException | NoDataFoundException e)
        {
            ActionContext.getContext().getValueStack().set("redirectUrl", ERROR_REDIRECT);
            return ERROR;
        }
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public UserType getAdminType() {
        return adminType;
    }

    public void setAdminType(String adminType) {
        this.adminType = UserType.valueOf(adminType);
    }
}

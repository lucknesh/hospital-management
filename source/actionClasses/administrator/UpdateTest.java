package com.hospital.actionClasses.administrator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.hospital.manage.Test.Test;
import com.hospital.manage.database.ExaminationDao;
import com.hospital.manage.exception.DaoException;
import org.apache.catalina.TrackedWebResource;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UpdateTest extends ActionSupport implements ModelDriven
{
    private static final String SUCCESS_REDIRECT = "manageTest";
    private static final String ERROR_REDIRECT = "updateTestForm";
    private static final String REDIRECT_URL = "redirectUrl";
    private Test test = new Test();

    public String execute()
    {
        try
        {
                ExaminationDao.updateTest(getTest());
            ActionContext.getContext().getValueStack().set(REDIRECT_URL,SUCCESS_REDIRECT);
            return SUCCESS;

        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL,ERROR_REDIRECT);
            return ERROR;
        }
    }

    public Object getModel()
    {
        return test;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    //    private Test getTest(HttpServletRequest request)
//    {
//        Test test = new Test();
//
//        test.setName(request.getParameter("name"));
//        test.setFee(Float.parseFloat(request.getParameter("fee")));
//        test.setTestId(Integer.parseInt(request.getParameter("testId")));
//
//        return test;
//    }
}

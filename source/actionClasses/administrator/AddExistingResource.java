package com.hospital.actionClasses.administrator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.InventoryDao;
import com.hospital.manage.exception.DaoException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddExistingResource extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "manageInventory";
    private static final String ERROR_REDIRECT = "addResourceForm";
    private static final String REDIRECT_URL = "redirectUrl";
    private Integer id;
    private Integer quantity;

    public String execute()
    {
        try
        {
                InventoryDao.changeQuantity(getId(),getQuantity(),+1);
                ActionContext.getContext().getValueStack().set(REDIRECT_URL,SUCCESS_REDIRECT);
                return SUCCESS;
        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, ERROR_REDIRECT);
            return ERROR;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}

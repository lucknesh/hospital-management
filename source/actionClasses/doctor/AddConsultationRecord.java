package com.hospital.actionClasses.doctor;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.NoteDao;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.record.ConsultationReport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddConsultationRecord extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "addPrescription";
    private static final String ERROR_REDIRECT = "consultationReportForm";
    private static final String REDIRECT_URL = "redirectUrl";
    public String execute()
    {
        HttpServletRequest request = ServletActionContext.getRequest();
        try
        {
            NoteDao.addConsultationReport(getConsultationReport(request));
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, SUCCESS_REDIRECT);
            return SUCCESS;
        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL,ERROR_REDIRECT);
            return ERROR;
        }
    }

    private ConsultationReport getConsultationReport(HttpServletRequest request)
    {
        ConsultationReport report = new ConsultationReport();

        report.setConsultationId((Integer) request.getSession().getAttribute("appointmentId"));
        report.setNextVisitDate(request.getParameter("nextVisit"));
        report.setDiagnosisId(Integer.parseInt(request.getParameter("diagnosisId")));
        report.setDoctorNote(request.getParameter("doctorNote").trim());

        return report;
    }
}

package com.hospital.actionClasses.doctor;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.DoctorDao;
import com.hospital.manage.database.ExaminationDao;
import com.hospital.manage.enums.ConsultationStatus;
import com.hospital.manage.enums.DoctorType;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.users.Doctor;
import org.apache.struts2.ServletActionContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class RequestExaminations extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "doctorInterface";
    private static final String ERROR_REDIRECT = "requestExamination";
    private static final String REDIRECT_URL = "redirectUrl";

    public String execute()
    {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpSession session = request.getSession();
        Integer  consultationId = (Integer) session.getAttribute("appointmentId");

        try
        {

        ExaminationDao.addExaminationRequest(getTestIds(request),consultationId);

        DoctorDao.updateStatus(consultationId, ConsultationStatus.OUT_FOR_EXAMINATION.toString());

        ActionContext.getContext().getValueStack().set(REDIRECT_URL, SUCCESS_REDIRECT);
        return SUCCESS;

        }
        catch (DaoException|SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            ActionContext.getContext().getValueStack().set(REDIRECT_URL, ERROR_REDIRECT);
            return ERROR;
        }

    }

    private ArrayList<Integer> getTestIds(HttpServletRequest request) throws DaoException,SQLException
    {
        ArrayList<Integer> testIds = new ArrayList<>();
        ResultSet rs = ExaminationDao.getAllTest();
        Integer testId = -1;
        while (rs.next()) {
            try {
                String stringTestId = "" + rs.getInt(1);
                testId = Integer.parseInt(request.getParameter(stringTestId));
            } catch (Exception e)
            {
                continue;
            }
            testIds.add(testId);
        }
        return testIds;
    }
}

package com.hospital.actionClasses.doctor;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.AppointmentDao;
import com.hospital.manage.exception.DaoException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ToggleConsultationStatus extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "doctorInterface";
    private static final String ERROR_REDIRECT = "doctorInterface";
    private static final String REDIRECT_URL = "redirectUrl";
    private String status;
    private Integer userId;

    public  String execute()
    {

        try {

            if (getStatus().equals("OPEN BOOKING")) AppointmentDao.toggleBookingTo(true, getUserId());
            else AppointmentDao.toggleBookingTo(false, userId);

            ActionContext.getContext().getValueStack().set(REDIRECT_URL, SUCCESS_REDIRECT);
            return SUCCESS;

        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, ERROR_REDIRECT);
            return ERROR;
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}

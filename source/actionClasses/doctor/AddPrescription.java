package com.hospital.actionClasses.doctor;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.NoteDao;
import com.hospital.manage.enums.MedicineDurationUnit;
import com.hospital.manage.enums.MedicineFrequency;
import com.hospital.manage.enums.WhenToTakeMedicine;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.record.Prescription;
import org.apache.struts2.ServletActionContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class AddPrescription extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "addPrescription";
    private static final String ERROR_REDIRECT = "addPrescription";
    private static final String REDIRECT_URL = "redirectUrl";

    public String execute()
    {
        HttpServletRequest request = ServletActionContext.getRequest();
        try
        {
                NoteDao.addPrescription(getPrescription(request));

                ActionContext.getContext().getValueStack().set(REDIRECT_URL, SUCCESS_REDIRECT);
                return SUCCESS;
        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, ERROR_REDIRECT);
            return ERROR;
        }
    }

    public Prescription getPrescription(HttpServletRequest request)throws DaoException
    {
        Integer medId = Integer.parseInt(request.getParameter("medicineId"));

        ArrayList<Integer> medicineIds = new ArrayList<>();
        medicineIds.add(Integer.parseInt(request.getParameter("medicineId")));

        HashMap<Integer,Integer> duration = new HashMap<>();
        duration.put(medId,Integer.parseInt(request.getParameter("duration")));

        HashMap<Integer, MedicineDurationUnit> durationUnit = new HashMap<>();
        durationUnit.put(medId,MedicineDurationUnit.valueOf(request.getParameter("durationUnit")));

        HashMap<Integer,WhenToTakeMedicine> when = new HashMap<>();
        when.put(medId, WhenToTakeMedicine.valueOf(request.getParameter("when")));

        HashMap<Integer,Integer> totalQuantity = new HashMap<>();
        totalQuantity.put(medId,Integer.parseInt(request.getParameter("totalQuantity")));

        HashMap<Integer,String> note = new HashMap<>();
        note.put(medId,request.getParameter("note"));

        HashMap<Integer, MedicineFrequency> frequency = new HashMap<>();
        frequency.put(medId,MedicineFrequency.valueOf(request.getParameter("frequency")));

        Integer appointmentId = (Integer)(request.getSession().getAttribute("appointmentId"));
        Integer doctorId = Integer.parseInt(request.getParameter("userId"));

        Integer prescriptionId = -1;

        try
        {
            prescriptionId = NoteDao.getPrescriptionId(appointmentId, doctorId);
        }catch (DaoException e){};

        Prescription prescription = new Prescription();

        prescription.setPrescriptionId(prescriptionId);
        prescription.setAppointmentId(appointmentId);
        prescription.setDoctorId(doctorId);
        prescription.setMedicineIds(medicineIds);
        prescription.setDuration(duration);
        prescription.setDurationUnit(durationUnit);
        prescription.setWhen(when);
        prescription.setNotes(note);
        prescription.setFrequency(frequency);
        prescription.setTotalQuantity(totalQuantity);

        return prescription;
    }
}


package com.hospital.actionClasses.nurse;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.Test.Examination;
import com.hospital.manage.database.ExaminationDao;
import com.hospital.manage.enums.NurseDuty;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.users.Nurse;
import org.apache.struts2.ServletActionContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AddTestResult extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "nurseInterface";
    private static final String ERROR_REDIRECT = "nurseInterface";
    private static final String REDIRECT_URL = "redirectUrl";

    public String execute()
    {
             HttpServletRequest request = ServletActionContext.getRequest();
            String redirectUrl = "addRequestedTestResults";
            String dutyType = "";
            HttpSession session = request.getSession();
            dutyType = request.getParameter("dutyType");
        try {
            if(dutyType.equals(NurseDuty.ADMITTANCE_DUTY.toString())) redirectUrl="nurseInterface";

            ExaminationDao.updateExaminationResult(getResults(request));

            ActionContext.getContext().getValueStack().set(REDIRECT_URL, SUCCESS_REDIRECT);
            return SUCCESS;
        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, ERROR_REDIRECT);
            return ERROR;
        }
    }

    public ArrayList<Examination> getResults(HttpServletRequest request)
    {
        ArrayList<Examination> results = new ArrayList<>();
    try {
        Integer appointmentId = Integer.parseInt((request.getParameter("appointmentId")));
        ResultSet resultSet = ExaminationDao.getRequestedExaminations(appointmentId);

        while (resultSet.next()) {
            Examination examination = new Examination();
            examination.setAppointmentId(appointmentId);
            examination.setResult(Float.parseFloat(request.getParameter("" + resultSet.getInt(2))));
            examination.setTestId(resultSet.getInt(2));
            examination.setExaminationId(resultSet.getInt(3));
            results.add(examination);
        }
    }catch (DaoException|SQLException e)
    {
        System.out.println(e);
        e.printStackTrace();
    }

        return results;
    }
}

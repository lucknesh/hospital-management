package com.hospital.actionClasses.nurse;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.AppointmentDao;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.hospital.manage.enums.UserType;
import com.hospital.manage.exception.DaoException;
import org.apache.struts2.ServletActionContext;

public class UpdateConsultationStatus extends ActionSupport
{
    private static final String DOCTOR_REDIRECT = "viewMyAppointments";
    private static final String NURSE_REDIRECT = "consultationQueue";
    private static final String REDIRECT_URL = "redirectUrl";
    private Integer userId;
    private UserType userType;
    private String status;
    private Integer appointmentId;

    public String execute()
    {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpSession session = request.getSession();
        try {

            if (getUserType().equals(UserType.NURSE))
            {
                if(status.equals("ALLOW")) status = "CONSULTING";
            }
            else if (getUserType().equals(UserType.DOCTOR)) {
                setStatus("COMPLETED");
            }

            AppointmentDao.updateConsultationStatus(getAppointmentId(), getStatus());

            if(getUserType().equals(UserType.DOCTOR))
            {
                ActionContext.getContext().getValueStack().set(REDIRECT_URL, DOCTOR_REDIRECT);
                return SUCCESS;
            }
            else
            {
                ActionContext.getContext().getValueStack().set(REDIRECT_URL, NURSE_REDIRECT);
                return SUCCESS;
            }
        }
        catch (DaoException e)
        {
            if(getUserType().equals(UserType.DOCTOR))
            {
                ActionContext.getContext().getValueStack().set(REDIRECT_URL, DOCTOR_REDIRECT);
                return ERROR;
            }
            else
            {
                ActionContext.getContext().getValueStack().set(REDIRECT_URL, NURSE_REDIRECT);
                return ERROR;
            }
        }

    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Integer appointmentId) {
        this.appointmentId = appointmentId;
    }
}

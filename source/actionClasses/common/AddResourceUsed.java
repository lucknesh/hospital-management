package com.hospital.actionClasses.common;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.hospital.manage.database.InventoryDao;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.inventory.Item;
import org.apache.struts2.ServletActionContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddResourceUsed extends ActionSupport implements ModelDriven
{
    private static final String SUCCESS_REDIRECT = "addUsedResources";
    private static final String ERROR_REDIRECT = "addUsedResources";
    private static final String REDIRECT_URL = "redirectUrl";
    private Integer itemId;
    private Integer quantity;
    private Item item = new Item();

    public String execute()
    {
        HttpServletRequest request = ServletActionContext.getRequest();
        try
        {
            InventoryDao.addUsedResource((Integer) request.getSession().getAttribute("appointmentId"),getItem());
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, SUCCESS_REDIRECT);
            return SUCCESS;
        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, ERROR_REDIRECT);
            return ERROR;
        }
    }


    public Object getModel()
    {
        return item;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}

package com.hospital.actionClasses.authentication;

import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.AuthenticatorDao;
import com.hospital.manage.database.DoctorDao;
import com.hospital.manage.database.NurseDao;
import com.hospital.manage.database.UserDao;
import com.hospital.manage.enums.*;
import com.hospital.manage.exception.ApplicationException;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.users.*;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.ServletActionContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public class Authenticator extends ActionSupport implements SessionAware
{
    private static final String SUPER_ADMIN_HOME = "super_admin";
    private static final String ADMIN_HOME = "admin";
    private static final String PATIENT_HOME = "patient";
    private static final String DOCTOR_HOME = "doctor";
    private static final String NURSE_HOME = "nurse";
    private static final String RECEPTIONIST_HOME = "receptionist";


    private String username;
    private String password;
    private User user;
    private LoginStatus loginStatus;
    private DoctorType doctorType;
    private NurseDuty nurseDuty;
    private SessionMap<String,Object> sessionMap;
    public void setSession(Map<String, Object> map) {
        sessionMap=(SessionMap)map;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DoctorType getDoctorType() {
        return doctorType;
    }

    public void setDoctorType(DoctorType doctorType) {
        this.doctorType = doctorType;
    }

    public NurseDuty getNurseDuty() {
        return nurseDuty;
    }

    public void setNurseDuty(NurseDuty nurseDuty) {
        this.nurseDuty = nurseDuty;
    }
    public LoginStatus getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(LoginStatus loginStatus) {
        this.loginStatus = loginStatus;
    }


    public String execute() throws ServletException,IOException,ApplicationException
    {
        try {
                ResultSet rs = UserDao.getUserInfo(username, password);
                if (rs.next())
                {
                    int userId = rs.getInt(1);
                    UserType usertype = UserType.valueOf(rs.getString(2));
                    String name = rs.getString(3);

                    if(!AuthenticatorDao.isActiveAccount(userId))
                    {
                        HttpServletRequest request = ServletActionContext.getRequest();
                        HttpServletResponse response = ServletActionContext.getResponse();
                        request.setAttribute("title","failed");
                        request.setAttribute("to","index.jsp");
                        request.setAttribute("message","please try again with correct credentials");
                        RequestDispatcher rd = request.getRequestDispatcher("popUp.jsp");
                        rd.forward(request,response);
                    }

                    User user = new User();
                    user.setName(name);
                    user.setPassword(password);
                    user.setUsername(username);
                    user.setUserId(userId);
                    user.setUserType(usertype);

                    sessionMap.put("user",user);
                    sessionMap.put("loginStatus",LoginStatus.TRUE);

                    switch (usertype)
                    {
                        case SUPER_ADMIN:
                            return SUPER_ADMIN_HOME;

                        case ADMIN:
                            return ADMIN_HOME;
//                            Admin admin = new Admin();
//                            admin.setName(name);
//                            admin.setUserId(userId);
//                            session.setAttribute("admin",admin);
//                            rd = req.getRequestDispatcher("adminInterface");
//                            rd.include(req,res);
//                            break;

                        case DOCTOR:
                            DoctorType doctorType = DoctorType.valueOf(DoctorDao.getDoctorDuty(userId));
                            setDoctorType(doctorType);
                            sessionMap.put("doctorType",doctorType);
                            return DOCTOR_HOME;
//                            Doctor doctor = new Doctor();
//                            DoctorType doctorType = DoctorType.valueOf(DoctorDao.getDoctorDuty(userId));
//                            doctor.setDoctorType(doctorType);
//                            session.setAttribute("doctor",doctor);

//                            rd = req.getRequestDispatcher("doctorInterface");
//                            rd.forward(req,res);
//                            break;

                        case PATIENT:
                            return PATIENT_HOME;

                        case NURSE:
//                            out.print("WELCOME NURSE : "+name+"<br><br><br><br>");
//                            Nurse nurse = new Nurse();
                            NurseDuty nurseDuty = NurseDuty.valueOf(NurseDao.getNurseDuty(userId));
                            setNurseDuty(nurseDuty);
                            sessionMap.put("nurseDuty",nurseDuty);
//                            nurse.setNurseDuty(nurseDuty);
//                            session.setAttribute("nurse",nurse);
//                            rd = req.getRequestDispatcher("nurseInterface");
//                            rd.forward(req,res);
                            return NURSE_HOME;

                        case RECEPTIONIST:
//                            Receptionist receptionist = new Receptionist();
//                            receptionist.setName(name);
//                            receptionist.setUserId(userId);
//                            session.setAttribute("receptionist",receptionist);
//                            rd = req.getRequestDispatcher("receptionistInterface");
//                            rd.forward(req,res);
//                            break;
                            return RECEPTIONIST_HOME;
                    }
                }
                else
                {
                    HttpServletRequest request = ServletActionContext.getRequest();
                    HttpServletResponse response = ServletActionContext.getResponse();
                    request.setAttribute("title","failed");
                    request.setAttribute("to","index.jsp");
                    request.setAttribute("message","please try again with correct credentials");
                    RequestDispatcher rd = request.getRequestDispatcher("popUp.jsp");
                    rd.include(request,response);
                }

        }catch (DaoException|SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }
        return ERROR;
    }

    public String logout() {
        sessionMap.remove("user");
        sessionMap.remove("loginStatus");
        sessionMap.remove("doctorType");
        sessionMap.remove("nurseDuty");
        sessionMap.invalidate();
        return SUCCESS;
    }
}

package com.hospital.actionClasses.superAdmin;

import com.hospital.manage.app.ApplicationParameter;
import com.hospital.manage.database.GenericDao;
import com.hospital.manage.exception.DaoException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddParameter extends HttpServlet
{
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {

            ApplicationParameter parameter = getApplicationParameter(request);

        try
        {
            GenericDao.addAppParameter(parameter);

            request.setAttribute("title", "success");
            request.setAttribute("to", "addParameterForm");
            request.setAttribute("message", "You added a new application parameter");
            RequestDispatcher rd = request.getRequestDispatcher("popUp.jsp");
            rd.include(request, response);
        }
        catch (DaoException e)
        {
            request.setAttribute("title", "failed");
            request.setAttribute("to", "addParameterForm");
            request.setAttribute("message", e.getMessage());
            RequestDispatcher rd = request.getRequestDispatcher("popUp.jsp");
            rd.include(request, response);
        }
    }

    private ApplicationParameter getApplicationParameter(HttpServletRequest request)
    {
        ApplicationParameter parameter = new ApplicationParameter();

        parameter.setParameterName(request.getParameter("name"));
        parameter.setValue(Float.parseFloat(request.getParameter("value")));

        return parameter;
    }
}

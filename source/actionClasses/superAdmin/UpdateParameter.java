package com.hospital.actionClasses.superAdmin;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.GenericDao;
import com.hospital.manage.exception.DaoException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UpdateParameter extends ActionSupport
{
    private Float value;
    private Integer parameterId;
    private static final String SUCCESS_REDIRECT = "updateParamForm";
    private static final String ERROR_REDIRECT = "updateParamForm";

    public String execute()
    {
        try
        {
            GenericDao.updateParameter(getParameterId(),getValue());
            ActionContext.getContext().getValueStack().set("redirectUrl", SUCCESS_REDIRECT);
            return SUCCESS;

        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set("redirectUrl", ERROR_REDIRECT);
            return ERROR;
        }
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Integer getParameterId() {
        return parameterId;
    }

    public void setParameterId(Integer parameterId) {
        this.parameterId = parameterId;
    }
}

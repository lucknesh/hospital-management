package com.hospital.actionClasses.bill;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.hospital.manage.database.BillDao;
import com.hospital.manage.enums.AppointmentType;
import com.hospital.manage.enums.BillStatus;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.record.Bill;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class PayBill extends ActionSupport implements ModelDriven
{
    private static final String SUCCESS_REDIRECT = "receptionistInterface";
    private static final String ERROR_REDIRECT = "generateBillFormOne";
    private static final String REDIRECT_URL = "redirectUrl";
    private AppointmentType appointmentType;
    private Bill bill = new Bill();

    public String execute()
    {
    try
    {
        if (getAppointmentType().equals(appointmentType)) {
            BillDao.addBill(bill, true);
        }
        else
        {
            BillDao.addBill(bill,false);
        }
        ActionContext.getContext().getValueStack().set(REDIRECT_URL,SUCCESS_REDIRECT);
        return SUCCESS;
    }
    catch (DaoException e)
    {
        ActionContext.getContext().getValueStack().set(REDIRECT_URL,ERROR_REDIRECT);
        return ERROR;
    }
    }

    public Object getModel()
    {
        return bill;
    }

    public AppointmentType getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(AppointmentType appointmentType) {
        this.appointmentType = appointmentType;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    //    private Bill getBill(HttpServletRequest request)
//    {
//        Bill bill = new Bill();
//        bill.setBillAmount(Float.parseFloat(request.getParameter("billAmount")));
//        bill.setBillStatus(BillStatus.PAID);
//        bill.setAppointmentId(Integer.parseInt(request.getParameter("appointmentId")));
//        return bill;
//    }


}

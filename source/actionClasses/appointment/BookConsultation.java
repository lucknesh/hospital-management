package com.hospital.actionClasses.appointment;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.booking.Consultation;
import com.hospital.manage.database.AppointmentDao;
import com.hospital.manage.enums.AppointmentType;
import com.hospital.manage.enums.BookingType;
import com.hospital.manage.enums.ConsultationStatus;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.time.DateTime;
import com.hospital.manage.users.Patient;
import org.apache.struts2.ServletActionContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class BookConsultation extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "newConsultationForm";
    private static final String ERROR_REDIRECT = "newConsultationForm";
    private static final String REDIRECT_URL = "redirectUrl";

    public String execute()
    {
            HttpServletRequest request = ServletActionContext.getRequest();
            BookingType bookingType = BookingType.valueOf(request.getParameter("bookingType"));
        try
        {
                AppointmentDao.addAppointment(getConsultation(request,bookingType));
                ActionContext.getContext().getValueStack().set(REDIRECT_URL,SUCCESS_REDIRECT);
                return SUCCESS;
        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL,ERROR_REDIRECT);
            return ERROR;
        }
    }

    public Consultation getConsultation(HttpServletRequest request,BookingType bookingType)
    {
        Consultation consultation = new Consultation();

        HttpSession session = request.getSession();
        int patientId;
        try
        {
            Patient patient = (Patient) session.getAttribute("patient");
            patientId = patient.getUserId();
        }
        catch (Exception e)
        {
            patientId = Integer.parseInt(request.getParameter("patientId"));
        }

        consultation.setPatientId(patientId);
        consultation.setDoctorId(Integer.parseInt(request.getParameter("doctorId")));
        consultation.setBookingType(bookingType);
        consultation.setBookedDateTime(DateTime.getTimeStamp());
        consultation.setConsultationDate(request.getParameter("date"));
        consultation.setAppointmentType(AppointmentType.CONSULTATION);
        consultation.setConsultationStatus(ConsultationStatus.NEW);

        return consultation;
    }
}

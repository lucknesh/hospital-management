package com.hospital.manage.booking;

import com.hospital.manage.enums.AdmittanceType;

import java.util.ArrayList;

public class Admittance extends Appointment{
    private AdmittanceType admittanceType;
    private Integer primaryInchargeDoctorId;
    private String admissionDateTime;
    private String dischargeDate;
    private Integer dayInchargeNurseId;
    private Integer nightInchargeNurseId;
    private ArrayList<Integer> secondaryInchargeDoctorIds = new ArrayList<>();

    public Integer getDayInchargeNurseId() {
        return dayInchargeNurseId;
    }

    public void setDayInchargeNurseId(Integer dayInchargeNurseId) {
        this.dayInchargeNurseId = dayInchargeNurseId;
    }

    public Integer getNightInchargeNurseId() {
        return nightInchargeNurseId;
    }

    public void setNightInchargeNurseId(Integer nightInchargeNurseId) {
        this.nightInchargeNurseId = nightInchargeNurseId;
    }

    public ArrayList<Integer> getSecondaryInchargeDoctorIds() {
        return secondaryInchargeDoctorIds;
    }

    public void setSecondaryInchargeDoctorIds(ArrayList<Integer> secondaryInchargeDoctorIds) {
        this.secondaryInchargeDoctorIds = secondaryInchargeDoctorIds;
    }

    public AdmittanceType getAdmittanceType() {
        return admittanceType;
    }

    public void setAdmittanceType(AdmittanceType admittanceType) {
        this.admittanceType = admittanceType;
    }

    public Integer getPrimaryInchargeDoctorId() {
        return primaryInchargeDoctorId;
    }

    public void setPrimaryInchargeDoctorId(Integer primaryInchargeDoctorId) {
        this.primaryInchargeDoctorId = primaryInchargeDoctorId;
    }

    public String getAdmissionDateTime() {
        return admissionDateTime;
    }

    public void setAdmissionDateTime(String admissionDateTime) {
        this.admissionDateTime = admissionDateTime;
    }

    public String getDischargeDate() {
        return dischargeDate;
    }

    public void setDischargeDate(String dischargeDate) {
        this.dischargeDate = dischargeDate;
    }
}

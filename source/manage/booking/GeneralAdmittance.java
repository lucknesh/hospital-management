package com.hospital.manage.booking;

import java.util.ArrayList;

public class GeneralAdmittance extends Admittance {
    private ArrayList<Integer> diagnosisIds = new ArrayList<>();
    private Integer consultationId;

    public ArrayList<Integer> getDiagnosisIds() {
        return diagnosisIds;
    }

    public void setDiagnosisIds(ArrayList<Integer> diagnosisIds) {
        this.diagnosisIds = diagnosisIds;
    }

    public Integer getConsultationId() {
        return consultationId;
    }

    public void setConsultationId(Integer consultationId) {
        this.consultationId = consultationId;
    }
}

package com.hospital.manage.booking;

public class EmergencyAdmittance extends Admittance{
    private Integer diagnosisId;

    public Integer getDiagnosisId() {
        return diagnosisId;
    }

    public void setDiagnosisId(Integer diagnosisId) {
        this.diagnosisId = diagnosisId;
    }
}

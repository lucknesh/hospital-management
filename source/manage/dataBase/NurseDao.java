package com.hospital.manage.database;

import com.hospital.manage.booking.EBookingExtension;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.users.OnCallShift;

import javax.servlet.ServletException;
import java.security.interfaces.DSAKey;
import java.sql.*;
import java.time.LocalDate;

public class NurseDao {
    private static Connection connection = null;
    private static Statement statement = null;

    static
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/HOSPITAL_DATA", "root", "");
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        }catch (SQLException | ClassNotFoundException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }
    }
    public static String getNurseDuty(Integer nurseId)throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT DUTY FROM NURSE WHERE NURSE_ID=" + nurseId);
            resultSet.next();
            return resultSet.getString(1);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }
    public static Integer getCabinId(Integer nurseId)throws DaoException
    {
            try {
                ResultSet resultSet = statement.executeQuery("SELECT CABIN_ID FROM CONSULTATION_NURSE WHERE NURSE_ID =" + nurseId);
                resultSet.next();
                return resultSet.getInt(1);
            }
            catch (SQLException e)
            {
                System.out.println(e);
                e.printStackTrace();

                throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
            }

    }

    public static ResultSet getConsultations(Integer cabinId)throws DaoException
    {
        try {
            String dayOfWeek = LocalDate.now().getDayOfWeek().toString().substring(0, 3);
            String date = LocalDate.now().toString();
            ResultSet resultSet = statement.executeQuery("SELECT C.TOKEN_NUMBER,U.NAME,C.CONSULTATION_STATUS,C.CONSULTATION_ID FROM CONSULTATION C,USER U,APPOINTMENTS A WHERE CONSULTATION_DATE='" + date + "' AND A.APPOINTMENT_ID = C.CONSULTATION_ID AND A.PATIENT_ID = U.USER_ID AND C.DOCTOR_ID = (SELECT DOCTOR_ID FROM VISITING_DOCTOR WHERE CABIN_ID=" + cabinId + " AND AVAILABLE_DAY='" + dayOfWeek + "') ORDER BY C.TOKEN_NUMBER");
            return resultSet;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static ResultSet getConsultationTestRequests(Integer cabinId)throws DaoException
    {
        try {
            String dayOfWeek = LocalDate.now().getDayOfWeek().toString().substring(0, 3);
            String date = LocalDate.now().toString();
            String sql = "SELECT DISTINCT(APPOINTMENT_ID) FROM EXAMINATION WHERE STATUS='YET_TO_TAKE' AND APPOINTMENT_ID IN (SELECT C.CONSULTATION_ID FROM CONSULTATION C,USER U,APPOINTMENTS A WHERE CONSULTATION_DATE='" + date + "' AND A.APPOINTMENT_ID = C.CONSULTATION_ID AND A.PATIENT_ID = U.USER_ID AND C.DOCTOR_ID = (SELECT DOCTOR_ID FROM VISITING_DOCTOR WHERE CABIN_ID=" + cabinId + " AND AVAILABLE_DAY='" + dayOfWeek + "') ORDER BY C.TOKEN_NUMBER) ";
            System.out.println(sql);
            ResultSet resultSet = statement.executeQuery(sql);
            return resultSet;
        }catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static ResultSet getNurseAndId()throws DaoException
    {
        try {
            ResultSet rs = statement.executeQuery("SELECT U.NAME,U.USER_ID FROM USER U,CONSULTATION_NURSE N WHERE N.NURSE_ID = U.USER_ID");
            return rs;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static void setCabinIdNull(Integer uId )throws DaoException
    {
        try {
            statement.executeUpdate("UPDATE CONSULTATION_NURSE SET CABIN_ID = NULL WHERE NURSE_ID = " + uId);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public static void reAllocateCabin(Integer cabinId,Integer uId)throws DaoException
    {
        try
        {
            statement.executeUpdate("UPDATE CONSULTATION_NURSE SET CABIN_ID = " + cabinId + " WHERE NURSE_ID=" + uId);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO);
        }
    }

    public static ResultSet getDayNurses() throws DaoException
    {
        try {
            return statement.executeQuery("SELECT A.NURSE_ID,U.NAME,N.SPECIALISATION FROM USER U,ADMITTANCE_NURSE A,NURSE N WHERE U.USER_ID = N.NURSE_ID AND A.NURSE_ID=N.NURSE_ID AND A.FROM_TIME<='09:00:00'");
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }
    public static ResultSet getEveningNurses() throws DaoException
    {
        try {
            return statement.executeQuery("SELECT A.NURSE_ID,U.NAME,N.SPECIALISATION FROM USER U,ADMITTANCE_NURSE A,NURSE N WHERE U.USER_ID = N.NURSE_ID AND A.NURSE_ID=N.NURSE_ID AND A.FROM_TIME>='17:00:00'");
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public static ResultSet getMyInPatients(Integer nurseId)throws DaoException
    {
        try {
            return statement.executeQuery(" SELECT A.ADMITTANCE_ID,U.NAME,AP.PATIENT_ID FROM ADMITTANCE A,USER U,INCHARGE_NURSES INR,APPOINTMENTS AP WHERE AP.TYPE='ADMITTANCE' AND A.ADMITTANCE_ID IN (SELECT ADMITTANCE_ID FROM INCHARGE_NURSES WHERE ADMITTANCE_ID IN (SELECT ADMITTANCE_ID FROM ADMITTANCE WHERE DISCHARGE_DATE IS NULL)AND NURSE_ID=" + nurseId + ") AND INR.NURSE_ID =" + nurseId + " AND U.USER_ID = AP.PATIENT_ID AND A.ADMITTANCE_ID=AP.APPOINTMENT_ID AND AP.APPOINTMENT_ID=INR.ADMITTANCE_ID");
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

}

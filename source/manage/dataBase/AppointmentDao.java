package com.hospital.manage.database;

import com.hospital.manage.booking.*;
import com.hospital.manage.enums.AdmittanceType;
import com.hospital.manage.enums.AppointmentType;
import com.hospital.manage.enums.EbookingStatus;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.exception.NoDataFoundException;
import com.hospital.manage.time.DateTime;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class AppointmentDao {
    private static Connection connection = null;
    private static Statement statement = null;
    private static int maximumLimit;
    static
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/HOSPITAL_DATA", "root", "");
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            setMaximumEBookingLimit();
        }catch (SQLException | ClassNotFoundException|DaoException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }
    }

    public static void setMaximumEBookingLimit()throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT PARAMETER_VALUE VALUE FROM APPLICATION_PARAMETER WHERE PARAMETER_NAME='E_BOOKING_LIMIT'");
            resultSet.next();
            maximumLimit = resultSet.getInt(1);
        }catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }
    public static void addAppointment(Appointment appointment) throws DaoException
    {
        try {

            statement.executeUpdate("START TRANSACTION");

            AppointmentType appointmentType = appointment.getAppointmentType();
            Integer patientId = appointment.getPatientId();

            statement.executeUpdate("INSERT INTO APPOINTMENTS(PATIENT_ID,TYPE) VALUES(" + patientId + ",'" + appointmentType.toString() + "')");
            ResultSet rs = statement.executeQuery("SELECT MAX(APPOINTMENT_ID) FROM APPOINTMENTS");
            rs.next();
            Integer appointmentId = rs.getInt(1);


            switch (appointmentType) {
                case CONSULTATION:
                    Consultation consultation = (Consultation) appointment;

                    Integer doctorId = consultation.getDoctorId();
                    String consultationDate = consultation.getConsultationDate();
                    String bookedDatetime = consultation.getBookedDateTime();
                    String consultationStatus = consultation.getConsultationStatus().toString();

                    Integer onlineBookingStatus = AppointmentDao.isOnlineBookingAvailable(doctorId, LocalDate.now().toString(), consultation.getBookingType().toString());
                    Integer tokenNumber = AppointmentDao.getTokenNumber(doctorId, LocalDate.now().toString(), consultation.getBookingType().toString(), onlineBookingStatus);

                    statement.executeUpdate("INSERT INTO CONSULTATION(CONSULTATION_ID,DOCTOR_ID,TOKEN_NUMBER,CONSULTATION_DATE,BOOKED_DATE_TIME)" +
                            "VALUES(" + appointmentId + "," + doctorId + "," + tokenNumber + ",'" + consultationDate + "','" + bookedDatetime + "')");
                    break;

                case ADMITTANCE:
                    Admittance admittance = (Admittance) appointment;
                    Integer primDoc = admittance.getPrimaryInchargeDoctorId();
                    AdmittanceType admittanceType = admittance.getAdmittanceType();
                    String admissionDateTime = admittance.getAdmissionDateTime();
                    Integer dayNurse = admittance.getDayInchargeNurseId();
                    Integer eveNurse = admittance.getNightInchargeNurseId();

                    ArrayList<Integer> doctorIds = admittance.getSecondaryInchargeDoctorIds();

                    statement.executeUpdate("INSERT INTO ADMITTANCE VALUES(" + appointmentId + ",'" + admittanceType.toString() + "'," + primDoc + ",'" + admissionDateTime + "',NULL)");
                    statement.executeUpdate("INSERT INTO INCHARGE_NURSES VALUES(" + appointmentId + ",'DAY_INCHARGE'," + dayNurse + "),(" + appointmentId + ",'NIGHT_INCHARGE'," + eveNurse + ") ");

                    for (Integer dId : doctorIds) {
                        System.out.println("doctor Id :" + dId + " : " + appointmentId);
                        statement.executeUpdate("INSERT INTO ADMITTANCE_DOCTORS VALUES(" + appointmentId + "," + dId + ")");
                    }


                    switch (admittanceType) {
                        case GENERAL:
                            GeneralAdmittance gnAdmittance = (GeneralAdmittance) admittance;
                            Integer consultationId = gnAdmittance.getConsultationId();

                            ArrayList<Integer> diagnosisIds = gnAdmittance.getDiagnosisIds();

                            statement.executeUpdate("INSERT INTO GENERAL_ADMITTANCE VALUES(" + appointmentId + "," + consultationId + ")");

                            for (Integer diagnosisId : diagnosisIds) {
                                statement.executeUpdate("INSERT INTO GENERAL_ADMITTANCE_DIAGNOSIS VALUES(" + appointmentId + "," + diagnosisId + ")");
                            }

                            break;

                        case EMERGENCY:
                            EmergencyAdmittance emAdmittance = (EmergencyAdmittance) admittance;

                            Integer diagnosisId = emAdmittance.getDiagnosisId();
                            statement.executeUpdate("INSERT INTO EMERGENCY_ADMITTANCE VALUES(" + appointmentId + "," + diagnosisId + ")");
                            break;
                    }


            }
            statement.executeUpdate("COMMIT");
        }
        catch (SQLException e)
        {
            try
            {
                statement.executeUpdate("ROLLBACK");
            }catch (SQLException sqe){sqe.printStackTrace();}
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_APPOINTMENT);
        }
    }

    public static Integer getTokenNumber(Integer doctorId,String date,String type,Integer status) throws SQLException
    {
        Integer token = -1;
            switch (status)
            {
                case (0):
                    statement.executeUpdate("INSERT INTO CONSULTATION_COUNTER VALUES("+doctorId+",'"+date+"','"+type+"',1,'OPEN')");
                    ResultSet rs = statement.executeQuery("SELECT SUM(COUNT) FROM  CONSULTATION_COUNTER WHERE DATE = '"+date+"' AND DOCTOR_ID="+doctorId+" "); rs.next();
                    token = rs.getInt(1);
                    break;
                case (1):
                    statement.executeUpdate("UPDATE CONSULTATION_COUNTER SET COUNT = 1+COUNT WHERE DOCTOR_ID="+doctorId+" AND DATE='"+date+"' AND APPOINTMENT_TYPE='"+type+"' ");
                    ResultSet resultSet = statement.executeQuery("SELECT SUM(COUNT) FROM CONSULTATION_COUNTER WHERE DOCTOR_ID="+doctorId+" AND DATE='"+date+"' ");
                    resultSet.next();
                    token =  resultSet.getInt(1);
                    resultSet = statement.executeQuery("SELECT SUM(COUNT) FROM CONSULTATION_COUNTER WHERE DOCTOR_ID="+doctorId+" AND DATE='"+date+"' AND APPOINTMENT_TYPE='ONLINE'"); resultSet.next();
                    Integer onlineCount = resultSet.getInt(1);

                    if(onlineCount == maximumLimit)
                    {
                        statement.executeUpdate("UPDATE CONSULTATION_COUNTER SET STATUS='CLOSED' WHERE DOCTOR_ID="+doctorId+" AND DATE='"+date+"' AND APPOINTMENT_TYPE='ONLINE'");
                    }
                    break;
            }
            //statement.executeUpdate("COMMIT");

        return token;
    }

    public static Integer isOnlineBookingAvailable(Integer doctorId,String date,String type)throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT STATUS FROM CONSULTATION_COUNTER WHERE DOCTOR_ID=" + doctorId + " AND DATE='" + date + "' AND APPOINTMENT_TYPE='" + type + "'  ");
            if (!resultSet.next()) return 0;
            if (EbookingStatus.valueOf(resultSet.getString(1)).equals(EbookingStatus.OPEN)) return 1;
            return -1;
        }catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }


    public static void updateConsultationStatus(Integer cId,String status)throws DaoException
    {
        try
        {
            statement.executeUpdate("UPDATE CONSULTATION SET CONSULTATION_STATUS='" + status + "' WHERE CONSULTATION_ID=" + cId);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_APPOINTMENT);
        }

    }


    public static ResultSet getAllMyConsultations(Integer pId)throws SQLException
    {

            ResultSet resultSet = statement.executeQuery("SELECT C.CONSULTATION_ID,U.NAME,C.CONSULTATION_DATE FROM USER U,CONSULTATION C WHERE C.CONSULTATION_ID IN (SELECT APPOINTMENT_ID FROM APPOINTMENTS WHERE PATIENT_ID="+pId+") AND C.DOCTOR_ID = U.USER_ID AND CONSULTATION_STATUS='COMPLETED'");
            return resultSet;

    }
    public static void toggleBookingTo(Boolean status,Integer doctorId) throws DaoException
    {
        try
        {
            if (!status) {
                statement.executeUpdate("UPDATE CONSULTATION_COUNTER SET STATUS='CLOSED' WHERE DOCTOR_ID=" + doctorId + " AND DATE='" + DateTime.getDate() + "'");
            } else {
                statement.executeUpdate("UPDATE CONSULTATION_COUNTER SET STATUS='OPEN' WHERE DOCTOR_ID=" + doctorId + " AND DATE='" + DateTime.getDate() + "' AND APPOINTMENT_TYPE='OFFLINE'");
                statement.executeUpdate("UPDATE CONSULTATION_COUNTER SET STATUS='OPEN' WHERE DOCTOR_ID=" + doctorId + " AND DATE='" + DateTime.getDate() + "' AND APPOINTMENT_TYPE='ONLINE' AND COUNT<" + maximumLimit);
            }
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_APPOINTMENT);
        }
    }

    public static ResultSet getAllConsultations(Integer userId)throws DaoException
    {
        try {
            return statement.executeQuery("SELECT C.CONSULTATION_ID,C.CONSULTATION_DATE,U.NAME,C.CONSULTATION_STATUS FROM USER U,CONSULTATION C,APPOINTMENTS A WHERE A.APPOINTMENT_ID = C.CONSULTATION_ID AND A.PATIENT_ID = " + userId + " AND U.USER_ID = C.DOCTOR_ID");
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }
    public static ResultSet getAllAdmittance(Integer userId)throws DaoException
    {
        try {
            return statement.executeQuery("SELECT AD.ADMITTANCE_ID,AD.TYPE,AD.ADMISSION_DATE_TIME,IFNULL(AD.DISCHARGE_DATE,'-NIL-') FROM ADMITTANCE AD,APPOINTMENTS WHERE AD.ADMITTANCE_ID = APPOINTMENT_ID AND PATIENT_ID = " + userId);
        }catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }
    public static AppointmentType isValidateAppointmentType(Integer appointmentId)throws NoDataFoundException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT TYPE FROM APPOINTMENTS WHERE APPOINTMENT_ID=" + appointmentId);
            if (resultSet.next()) {
                switch (AppointmentType.valueOf(resultSet.getString(1))) {
                    case CONSULTATION:
                        resultSet = statement.executeQuery("SELECT DOCTOR_ID FROM CONSULTATION WHERE CONSULTATION_STATUS <> 'DELAYED' AND CONSULTATION_STATUS <> 'EXPIRED' AND CONSULTATION_ID NOT IN(SELECT APPOINTMENT_ID FROM BILL WHERE APPOINTMENT_ID=" + appointmentId + " AND BILL_STATUS='PAID') AND CONSULTATION_ID=" + appointmentId);
                        if (resultSet.next()) return AppointmentType.CONSULTATION;
                        else throw new NoDataFoundException("NOT A VALID APPOINTMENT ID");

                    case ADMITTANCE:
                        resultSet = statement.executeQuery("SELECT ADMITTANCE_ID FROM ADMITTANCE WHERE DISCHARGE_DATE IS NULL AND ADMITTANCE_ID=" + appointmentId);
                        if (resultSet.next()) return AppointmentType.ADMITTANCE;
                        else throw new NoDataFoundException("NOT A VALID APPOINTMENT ID");

                }
            }
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }
        throw new NoDataFoundException("NOT A VALID APPOINTMENT ID");
    }




}

package com.hospital.manage.database;

import com.hospital.manage.enums.AccountStatus;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.exception.NoDataFoundException;

import java.sql.*;

public class AuthenticatorDao {
    private static Connection connection = null;
    private static Statement statement = null;
    static
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/HOSPITAL_DATA", "root", "");
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        }catch (SQLException | ClassNotFoundException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }
    }
    public static String authenticate(String username,String password,Integer userId)throws DaoException
    {
        try {
            ResultSet rs = statement.executeQuery("SELECT USER_TYPE FROM USER WHERE PASSWORD='" + password + "' AND (EMAIL='" + username + "' OR MOBILE='" + username + "') AND USER_ID=" + userId);
            rs.next();
            return rs.getString(1);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_AUTHENTICATING);
        }
    }

//    public static String authenticate(String username,String password)throws SQLException
//    {
//
//            ResultSet rs = statement.executeQuery("SELECT USER_TYPE FROM USER WHERE PASSWORD='"+password+"' AND (EMAIL='"+username+"' OR MOBILE='"+username+"')");
//            rs.next();
//            return rs.getString(1);
//    }

    public static boolean isActiveAccount(Integer userId) throws DaoException
    {
        try
        {
            ResultSet resultSet = statement.executeQuery("SELECT ACCOUNT_STATUS FROM USER WHERE USER_ID=" + userId);
            if (resultSet.next()) {
                if (AccountStatus.valueOf(resultSet.getString(1)).equals(AccountStatus.ACTIVE)) return true;
                else return false;
            } else return false;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_AUTHENTICATING);
        }
    }

    public static boolean isValidPatient(Integer userId) throws DaoException {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT ACCOUNT_STATUS FROM USER WHERE USER_TYPE='PATIENT' AND USER_ID=" + userId);
            if (resultSet.next()) {
                if (AccountStatus.valueOf(resultSet.getString(1)).equals(AccountStatus.ACTIVE)) return true;
                else return false;
            } else return false;

        } catch (SQLException e) {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_AUTHENTICATING);
        }
    }

    public static String getUserType(Integer userId)throws NoDataFoundException
    {
        try
        {
            ResultSet resultSet = statement.executeQuery("SELECT USER_TYPE FROM USER WHERE USER_ID=" + userId);
            resultSet.next();
            return resultSet.getString(1);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new NoDataFoundException(ExceptionMessage.ERROR_AUTHENTICATING);
        }

    }


}

package com.hospital.manage.database;

import com.hospital.manage.enums.*;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.record.*;
import com.hospital.manage.time.DateTime;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class NoteDao {
    private static Connection connection = null;
    private static Statement statement = null;
    static
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/HOSPITAL_DATA", "root", "");
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        }catch (SQLException | ClassNotFoundException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }
    }
    public static void addPrescription(Prescription prescription)throws DaoException
    {

            try {
                statement.executeUpdate("START TRANSACTION");

                Integer appointmentId = prescription.getAppointmentId();
                Integer doctorId = prescription.getDoctorId();
                ArrayList<Integer> medicineIds = prescription.getMedicineIds();
                HashMap<Integer, Integer> duration = prescription.getDuration();
                HashMap<Integer, MedicineDurationUnit> durationUnit = prescription.getDurationUnit();
                HashMap<Integer, MedicineFrequency> frequency = prescription.getFrequency();
                HashMap<Integer, WhenToTakeMedicine> when = prescription.getWhen();
                HashMap<Integer, Integer> totalQuantity = prescription.getTotalQuantity();
                HashMap<Integer, String> notes = prescription.getNotes();


                Integer prescriptionId = prescription.getPrescriptionId();
                System.out.println("prescription I d: " + prescriptionId);
                if (prescriptionId <= 0) {
                    statement.executeUpdate("INSERT INTO PRESCRIPTION(APPOINTMENT_ID,DOCTOR_ID) VALUES(" + appointmentId + "," + doctorId + ")");
                    ResultSet rs = statement.executeQuery("SELECT MAX(PRESCRIPTION_ID) FROM PRESCRIPTION");
                    rs.next();
                    prescriptionId = rs.getInt(1);
                }

                for (Integer drugId : medicineIds) {
                    statement.executeUpdate("INSERT INTO PRESCRIBED_MEDICINE VALUES(" + prescriptionId + "," + drugId + "," + duration.get(drugId) + ",'" + durationUnit.get(drugId).toString() + "','" + frequency.get(drugId).toString() + "','" + when.get(drugId).toString() + "'," + totalQuantity.get(drugId) + ")");
                    if (notes.get(drugId) != null && notes.get(drugId).trim().length() != 0) {
                        statement.executeUpdate("INSERT INTO PRESCRIPTION_NOTE VALUES(" + prescriptionId + "," + drugId + ",'" + notes.get(drugId) + "')");
                    }
                }
                statement.executeUpdate("COMMIT");
                statement.executeUpdate("SET AUTOCOMMIT=1");
            } catch (SQLException e) {
                try
                {
                    statement.executeUpdate("ROLLBACK");
                }
                catch (SQLException se)
                {
                    System.out.println(se);
                    se.printStackTrace();
                }
                System.out.println(e);
                e.printStackTrace();

                throw new DaoException(ExceptionMessage.ERROR_UPDATING_RECORD);
            }

    }




    public static void addConsultationReport(ConsultationReport conReport)throws DaoException
    {
        try {
            Integer consultationId = conReport.getConsultationId();
            Integer diagnosisId = conReport.getDiagnosisId();
            String nextVisitDate = conReport.getNextVisitDate().trim();
            String doctorNote = conReport.getDoctorNote();

            if (nextVisitDate.length() <= 0) {
                statement.executeUpdate("INSERT INTO CONSULTATION_REPORT VALUES(" + consultationId + "," + diagnosisId + ",NULL,'" + doctorNote + "')");
            } else {
                statement.executeUpdate("INSERT INTO CONSULTATION_REPORT VALUES(" + consultationId + "," + diagnosisId + ",'" + nextVisitDate + "','" + doctorNote + "')");
            }
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_RECORD);
        }

    }

    public static void addAdmittanceNote(AdmittanceNote note) throws DaoException
    {
    try {
        statement.executeUpdate("START TRANSACTION");

        AdmittanceNoteType noteType = note.getAdmittanceNoteType();
        Integer admittanceId = note.getAdmittanceId();
        String dateTime = DateTime.getTimeStamp();
        String patientNote = note.getNote().trim();
        Integer writerId = note.getWriterId();

        statement.executeUpdate("INSERT INTO NOTES(ADMITTANCE_ID,DATE_TIME,NOTE,TYPE) VALUES(" + admittanceId + ",'" + dateTime + "','" + patientNote + "','" + noteType + "')");


        ResultSet rs = statement.executeQuery("SELECT MAX(NOTE_ID) FROM NOTES");
        rs.next();
        Integer noteId = rs.getInt(1);

        switch (noteType) {
            case DOCTOR_NOTE:
                statement.executeUpdate("INSERT INTO DOCTOR_NOTES VALUES(" + noteId + "," + writerId + ")");
                break;
            case NURSE_NOTE:
                statement.executeUpdate("INSERT INTO NURSE_NOTES VALUES(" + noteId + "," + writerId + ")");
                break;
        }
        statement.executeUpdate("COMMIT");
    }
    catch (SQLException e)
    {
    try
    {
    statement.executeUpdate("ROLLBACK");
    }
    catch (SQLException se)
    {
        System.out.println(se);
        se.printStackTrace();
    }
    System.out.println(e);
    e.printStackTrace();

    throw new DaoException(ExceptionMessage.ERROR_UPDATING_RECORD);
    }
    }

    public static ResultSet getPrescription(Integer appointmentId,Integer doctorID)throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT M.NAME,P.DURATION,P.DURATION_UNIT,P.FREQUANCY,P.WHEN,P.TOTAL_QUANTITY FROM PRESCRIPTION R,PRESCRIBED_MEDICINE P,PRESCRIPTION_NOTE N,MEDICINE M  WHERE R.DOCTOR_ID=" + doctorID + " AND R.APPOINTMENT_ID=" + appointmentId + " AND R.PRESCRIPTION_ID = P.PRESCRIPTION_ID AND  M.MEDICINE_ID = P.MEDICINE_ID");
            return resultSet;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }
    public static ResultSet getAllPrescription(Integer appointmentId)throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT U.NAME,M.NAME,P.DURATION,P.DURATION_UNIT,P.FREQUANCY,P.WHEN,P.TOTAL_QUANTITY FROM PRESCRIPTION R,PRESCRIBED_MEDICINE P,PRESCRIPTION_NOTE N,MEDICINE M,USER U  WHERE U.USER_ID = R.DOCTOR_ID AND R.APPOINTMENT_ID=" + appointmentId + " AND R.PRESCRIPTION_ID = P.PRESCRIPTION_ID AND  M.MEDICINE_ID = P.MEDICINE_ID ORDER BY R.DOCTOR_ID");
            return resultSet;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static ResultSet getMedicineList()throws DaoException
    {
            try {
                ResultSet resultSet = statement.executeQuery("SELECT NAME,MEDICINE_ID FROM MEDICINE");
                return resultSet;
            }
            catch (SQLException e)
            {
                System.out.println(e);
                e.printStackTrace();

                throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
            }

    }

    public static Integer getPrescriptionId(Integer appointmentId,Integer docId)throws DaoException
    {
    try
    {
        ResultSet resultSet = statement.executeQuery("SELECT PRESCRIPTION_ID FROM PRESCRIPTION WHERE DOCTOR_ID=" + docId + " AND APPOINTMENT_ID=" + appointmentId);
        resultSet.next();
        return resultSet.getInt(1);
    }
    catch (SQLException e)
    {
        System.out.println(e);
        e.printStackTrace();
    }
        throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
    }

    public static ResultSet getAdmittanceNurseNotes(Integer appointmentId) throws DaoException
    {
        try
        {
            return statement.executeQuery("SELECT N.NOTE,N.TYPE,N.DATE_TIME,U.NAME FROM NOTES N,NURSE_NOTES NN,USER U WHERE N.TYPE='NURSE_NOTE' AND NN.NOTE_ID = N.NOTE_ID AND U.USER_ID=NN.NURSE_ID AND N.ADMITTANCE_ID="+appointmentId);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public static ResultSet getAdmittanceDoctorNotes(Integer appointmentId) throws DaoException
    {
        try
        {
            return statement.executeQuery("SELECT N.NOTE,N.TYPE,N.DATE_TIME,U.NAME FROM NOTES N,DOCTOR_NOTES DN,USER U WHERE N.TYPE='DOCTOR_NOTE' AND DN.NOTE_ID = N.NOTE_ID AND U.USER_ID=DN.DOCTOR_ID AND N.ADMITTANCE_ID="+appointmentId);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public static ResultSet getConsultationNote(Integer appointmentId)throws DaoException
    {
        try {
            return statement.executeQuery("SELECT IFNULL(NEXT_VISIT,'-NIL-'),DOCTOR_NOTE FROM CONSULTATION_REPORT WHERE CONSULTATION_ID=" + appointmentId);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

}

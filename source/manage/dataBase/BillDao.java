package com.hospital.manage.database;

import com.hospital.manage.enums.BillStatus;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.record.Bill;
import com.hospital.manage.time.DateTime;

import java.security.interfaces.DSAKey;
import java.sql.*;

public class BillDao {
    private static Connection connection = null;
    private static Statement statement = null;
    static
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/HOSPITAL_DATA", "root", "");
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        }catch (SQLException | ClassNotFoundException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }
    }


    public static void addBill(Bill bill,boolean admittance)throws DaoException
    {
        try {
            statement.executeUpdate("START TRANSACTION");
            Integer appointmentId = bill.getAppointmentId();
            Float amount = bill.getBillAmount();
            String billStatus = BillStatus.PAID.toString();

            if(bill.getBillScriptPath()!=null)
            {
                String billPath = bill.getBillScriptPath();
                statement.executeUpdate("INSERT INTO BILL VALUES("+appointmentId+","+amount+",'"+billStatus+"','"+billPath+"')");
            }
            else
            {
                statement.executeUpdate("INSERT INTO BILL(APPOINTMENT_ID,BILL_AMOUNT,BILL_STATUS) VALUES("+appointmentId+","+amount+",'"+billStatus+"')");
            }
            if (admittance)
            {
                statement.executeUpdate("UPDATE ADMITTANCE SET DISCHARGE_DATE='" + DateTime.getDate() + "' WHERE ADMITTANCE_ID=" + appointmentId);
            }
            else
            {
                statement.executeUpdate("UPDATE CONSULTATION SET CONSULTATION_STATUS='COMPLETED' WHERE CONSULTATION_ID=" + appointmentId);

            }
            statement.executeUpdate("COMMIT");
        }catch (SQLException e)
        {
            try
            {
                statement.executeUpdate("ROLLBACK");
            }catch (SQLException se){
                System.out.println(se);
                e.printStackTrace();
            }
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_BILL);
        }

    }

//    public static boolean updateBill(Bill bill) throws SQLException
//    {
//
//            Integer appointmentId = bill.getAppointmentId();
//            Float amount = bill.getBillAmount();
//            String billStatus = bill.getBillStatus().toString();
//            if (bill.getBillScriptPath() != null) {
//                String billPath = bill.getBillScriptPath();
//                statement.executeUpdate("UPDATE BILL SET BILL_AMOUNT=" + amount + ",BILL_STATUS='" + billStatus + "',BILL_SCRIPT_PATH='" + bill.getBillScriptPath() + "' WHERE APPOINTMENT_ID=" + appointmentId);
//            } else {
//                statement.executeUpdate("UPDATE BILL SET BILL_AMOUNT=" + amount + ",BILL_STATUS='" + billStatus + "' WHERE APPOINTMENT_ID=" + appointmentId);
//            }
//            return true;
//    }

//    public static ResultSet getBill(Integer appointmentId)throws SQLException
//    {
//
//            ResultSet rs = statement.executeQuery("SELECT * FROM BILL WHERE APPOINTMENT_ID="+appointmentId);
//            return rs;
//
//    }
}


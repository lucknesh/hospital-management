package com.hospital.manage.database;

import com.hospital.manage.booking.EBookingExtension;
import com.hospital.manage.enums.EbookingStatus;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.time.DateTime;
import com.hospital.manage.users.OnCallShift;

import javax.servlet.ServletException;
import java.security.interfaces.DSAKey;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DoctorDao {
    private static Connection connection = null;
    private static Statement statement = null;
    static
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/HOSPITAL_DATA", "root", "");
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        }catch (SQLException | ClassNotFoundException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }
    }





     public static ResultSet getAvailableDoctors(String day,String specialisation)throws DaoException
     {
         try {
             String sql = "SELECT U.NAME,D.DOCTOR_ID,D.IN_SERVICE_SINCE,D.CONSULTATION_FEE FROM DOCTOR D,USER U WHERE  D.SPECIALISATION='" + specialisation + "' AND U.USER_ID=D.DOCTOR_ID AND U.ACCOUNT_STATUS = 'ACTIVE' AND D.DOCTOR_ID IN(SELECT DOCTOR_ID FROM VISITING_DOCTOR WHERE AVAILABLE_DAY='" + day + "')";
             ResultSet rs = statement.executeQuery(sql);
             return rs;
         }
         catch (SQLException e)
         {
             System.out.println(e);
             e.printStackTrace();

             throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
         }

     }
    public static ResultSet getAllAvailableDoctors(String dayOfWeek,String date,String specialisation,String bookingType)throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT DOCTOR_ID FROM VISITING_DOCTOR WHERE AVAILABLE_DAY='" + dayOfWeek + "' AND DOCTOR_ID IN (SELECT DOCTOR_ID FROM DOCTOR WHERE SPECIALISATION='" + specialisation + "') AND DOCTOR_ID NOT IN (SELECT IFNULL(DOCTOR_ID,0) FROM CONSULTATION_COUNTER WHERE DATE='" + date + "' AND APPOINTMENT_TYPE='" + bookingType + "' AND STATUS='CLOSED')");
            return resultSet;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static ResultSet getDoctorAndName()throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT DISTINCT(D.DOCTOR_ID),U.NAME FROM VISITING_DOCTOR D,USER U WHERE D.DOCTOR_ID = U.USER_ID");
            return resultSet;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }
    public static void setCabinIdNull(Integer uId )throws DaoException
    {
        try {
            statement.executeUpdate("UPDATE VISITING_DOCTOR SET CABIN_ID = NULL WHERE DOCTOR_ID = " + uId);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }

    }

    public static ResultSet getAvailableDays(Integer uId)throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT AVAILABLE_DAY FROM VISITING_DOCTOR WHERE DOCTOR_ID=" + uId);
            return resultSet;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static void reAllocateCabin(HashMap<String,Integer> daysAndCabin , Integer uId)throws DaoException
    {

        try {
            for (Map.Entry<String, Integer> entry : daysAndCabin.entrySet()) {
                String day = entry.getKey();
                Integer cabin = entry.getValue();
                statement.executeUpdate("UPDATE VISITING_DOCTOR SET CABIN_ID = " + cabin + " WHERE AVAILABLE_DAY='" + day + "' AND DOCTOR_ID=" + uId);
            }
        }catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO);
        }

    }

    public static String getDoctorDuty(Integer userId)throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT TYPE FROM DOCTOR WHERE DOCTOR_ID = " + userId);
            resultSet.next();
            return resultSet.getString(1);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static Integer getTodayAppointmentsCount(Integer docId)throws DaoException
    {

        try {
            ResultSet resultSet = statement.executeQuery("SELECT COUNT(COUNT) FROM CONSULTATION_COUNTER WHERE DATE='" + LocalDate.now().toString() + "' AND DOCTOR_ID=" + docId);
            resultSet.next();
            return resultSet.getInt(1);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static ResultSet getMyAppointments(String date ,Integer docId)throws SQLException
    {

            ResultSet resultSet = statement.executeQuery("SELECT U.NAME,C.CONSULTATION_ID FROM CONSULTATION C,USER U,APPOINTMENTS A WHERE C.CONSULTATION_DATE='"+ date +"' AND C.CONSULTATION_ID = A.APPOINTMENT_ID AND A.PATIENT_ID = U.USER_ID AND C.CONSULTATION_STATUS = 'CONSULTING' AND C.DOCTOR_ID="+docId);
            return resultSet;

    }

    public static void updateStatus(Integer consultationId,String status)throws DaoException
    {
        try {
            statement.executeUpdate("UPDATE CONSULTATION SET CONSULTATION_STATUS='" + status + "' WHERE CONSULTATION_ID=" + consultationId);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_APPOINTMENT);
        }

    }

    public static ResultSet getDutyDoctorDetails()throws DaoException
    {
        try {
            String dayOfWeek = DateTime.getDayOfWeek();
            String time = DateTime.getTime();

            ResultSet resultSet = statement.executeQuery("SELECT D.DOCTOR_ID ,U.NAME,D.SPECIALISATION FROM  USER U,DOCTOR D WHERE U.ACCOUNT_STATUS='ACTIVE' AND U.USER_ID = D.DOCTOR_ID AND D.DOCTOR_ID IN (SELECT D.DOCTOR_ID FROM DUTY_DOCTOR D WHERE (FROM_TIME <= '" + time + "' OR FROM_TIME>TO_TIME) AND (TO_TIME >= '" + time + "' OR FROM_TIME>TO_TIME) AND AVAILABLE_DAY='" + dayOfWeek + "')");
            return resultSet;
        }catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public static ResultSet getDutyDoctors(Integer primaryDoctorId)throws DaoException
    {
        try
        {
            return statement.executeQuery("SELECT D.DOCTOR_ID ,U.NAME,D.SPECIALISATION FROM  USER U,DOCTOR D WHERE U.ACCOUNT_STATUS='ACTIVE' AND U.USER_ID = D.DOCTOR_ID AND D.DOCTOR_ID IN (SELECT D.DOCTOR_ID FROM DUTY_DOCTOR D WHERE D.DOCTOR_ID <> " + primaryDoctorId + ")");
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public static ResultSet getMyInPatients(Integer doctorId)throws DaoException
    {
        try {
            return statement.executeQuery("SELECT A.ADMITTANCE_ID,U.NAME,AP.PATIENT_ID FROM USER U,ADMITTANCE A,ADMITTANCE_DOCTORS AD,APPOINTMENTS AP WHERE AP.APPOINTMENT_ID = A.ADMITTANCE_ID AND A.DISCHARGE_DATE IS NULL AND (A.PRIMARY_INCHARGE_DOCTOR=" + doctorId + " OR AD.DOCTOR_ID=" + doctorId + ") AND AP.PATIENT_ID=U.USER_ID AND AP.TYPE = 'ADMITTANCE' AND AD.ADMITTANCE_ID=A.ADMITTANCE_ID");
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public static Boolean getMyAppointmentAvailabilityStatus(Integer doctorId) throws DaoException
    {
        try {
            EbookingStatus onlineBookingStatus = EbookingStatus.CLOSED;
            EbookingStatus offlineBookingStatus = EbookingStatus.CLOSED;


            ResultSet resultSet = statement.executeQuery("SELECT STATUS FROM CONSULTATION_COUNTER WHERE DOCTOR_ID=" + doctorId + "  AND APPOINTMENT_TYPE='OFFLINE' AND DATE = '" + DateTime.getDate() + "' ");
            if (resultSet.next()) offlineBookingStatus = EbookingStatus.valueOf(resultSet.getString(1));
            else {
                statement.executeUpdate("INSERT INTO CONSULTATION_COUNTER VALUES('" + doctorId + "','" + DateTime.getDate() + "','OFFLINE',0,'OPEN')");
                offlineBookingStatus = EbookingStatus.OPEN;
            }


            resultSet = statement.executeQuery("SELECT STATUS FROM CONSULTATION_COUNTER WHERE DOCTOR_ID=" + doctorId + "  AND APPOINTMENT_TYPE='ONLINE' AND DATE = '" + DateTime.getDate() + "' ");
            if (resultSet.next()) onlineBookingStatus = EbookingStatus.valueOf(resultSet.getString(1));
            else {
                statement.executeUpdate("INSERT INTO CONSULTATION_COUNTER VALUES('" + doctorId + "','" + DateTime.getDate() + "','ONLINE',0,'OPEN')");
                onlineBookingStatus = EbookingStatus.OPEN;
            }


            if (offlineBookingStatus.equals(EbookingStatus.CLOSED) && onlineBookingStatus.equals(EbookingStatus.CLOSED))
                return false;

            return true;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public static Float getConsultationFee(Integer consultationId)throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT D.CONSULTATION_FEE FROM DOCTOR D WHERE D.DOCTOR_ID=(SELECT DOCTOR_ID FROM CONSULTATION WHERE CONSULTATION_ID=" + consultationId + ")");
            resultSet.next();
            return resultSet.getFloat(1);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public static Float getAdmittanceDoctorFee(Integer appointmentId)throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT SUM(CONSULTATION_FEE) FROM DOCTOR WHERE DOCTOR_ID IN (SELECT DOCTOR_ID FROM ADMITTANCE_DOCTORS WHERE ADMITTANCE_ID=" + appointmentId + ") OR DOCTOR_ID=(SELECT PRIMARY_INCHARGE_DOCTOR FROM ADMITTANCE WHERE ADMITTANCE_ID=" + appointmentId + ")");
            resultSet.next();
            Float feePerDay = resultSet.getFloat(1);
            resultSet = statement.executeQuery("SELECT TIMESTAMPDIFF(SECOND,ADMISSION_DATE_TIME,'" + DateTime.getTimeStamp() + "') FROM ADMITTANCE WHERE ADMITTANCE_ID=" + appointmentId);
            resultSet.next();
            Integer epochSeconds = resultSet.getInt(1);
            Integer days = (((epochSeconds / 60) / 60) / 24);
            if (days == 0) return feePerDay;
            return days * feePerDay;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }
}

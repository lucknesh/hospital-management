package com.hospital.manage.database;

import com.hospital.manage.app.ApplicationParameter;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;

import java.sql.*;

public class GenericDao {
    private static Connection connection = null;
    private static Statement statement = null;
    static
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/HOSPITAL_DATA", "root", "");
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        }catch (SQLException | ClassNotFoundException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }
    }

    public static void addAppParameter(ApplicationParameter param)throws DaoException
    {
        try {
            String name = param.getParameterName();
            Float value = param.getValue();
            statement.executeUpdate("INSERT INTO APPLICATION_PARAMETER(PARAMETER_NAME,PARAMETER_VALUE) VALUES('" + name + "'," + value + ")");
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }
    public static void updateParameter(ApplicationParameter param)throws DaoException
    {
    try {
    Integer id = param.getParameterId();
    String name = param.getParameterName();
    Float value = param.getValue();

    statement.executeUpdate("UPDATE APPLICATION_PARAMETER SET PARAMETER_NAME='" + name + "',PARAMETER_VALUE=" + value + " WHERE ID=" + id);
    AppointmentDao.setMaximumEBookingLimit();
    }
    catch (SQLException e)
    {
        System.out.println(e);
        e.printStackTrace();

        throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
    }


    }

    public static void updateParameter(Integer id,Float value)throws DaoException
    {
        try {
            statement.executeUpdate("UPDATE APPLICATION_PARAMETER SET PARAMETER_VALUE=" + value + " WHERE ID=" + id);
            AppointmentDao.setMaximumEBookingLimit();
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }


//    public static ResultSet selectParameter(Integer id)throws SQLException
//    {
//
//        ResultSet rs = statement.executeQuery("SELECT * FROM APPLICATION_PARAMETER WHERE ID="+id);
//        return rs;
//
//    }

    public static ResultSet getAllParameter()throws DaoException
    {
        try {
            ResultSet rs = statement.executeQuery("SELECT * FROM APPLICATION_PARAMETER ");
            return rs;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

//    public static boolean removeParameter(Integer id)throws DaoException
//    {
//
//            statement.executeUpdate("DELETE FROM APPLICATION_PARAMETER WHERE ID="+id);
//            return true;
//
//    }

    public static ResultSet getAllStates()throws DaoException
    {
            try {
                ResultSet rs = statement.executeQuery("SELECT * FROM STATE");
                return rs;
            }
            catch (SQLException e)
            {
                System.out.println(e);
                e.printStackTrace();

                throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
            }

    }
    public static ResultSet getAllDistrict()throws DaoException
    {
        try {
            ResultSet rs = statement.executeQuery("SELECT * FROM DISTRICT");
            return rs;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static ResultSet getDiagnosisIdAndName()throws DaoException
    {
            try {
                ResultSet resultSet = statement.executeQuery("SELECT * FROM DIAGNOSIS");
                return resultSet;
            }
            catch (SQLException e)
            {
                System.out.println(e);
                e.printStackTrace();

                throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
            }

    }

    public static Float getTax()throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT PARAMETER_VALUE FROM APPLICATION_PARAMETER WHERE PARAMETER_NAME = 'TAX'");
            resultSet.next();
            return resultSet.getFloat(1);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();
             throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }
}

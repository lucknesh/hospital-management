package com.hospital.manage.database;

import com.hospital.manage.enums.*;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.users.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class UserDao {
    private static Connection connection = null;
    private static Statement statement = null;
    static
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/HOSPITAL_DATA", "root", "");
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        }catch (SQLException | ClassNotFoundException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }
    }

    public static void insertUser(User user) throws SQLException,DaoException
    {
        UserType userType = user.getUserType();

        String password= user.getPassword();

        String name = user.getName();
        String email = user.getEmail();
        String mobile = user.getMobile();

        try {
            statement.executeUpdate("SET AUTOCOMMIT=0");
            statement.executeUpdate("START TRANSACTION");

            statement.executeUpdate("INSERT INTO USER(NAME,MOBILE,EMAIL,PASSWORD,USER_TYPE) VALUES('"+name+"','"+mobile+"','"+email+"','"+password+"','"+userType.toString()+"')");
            ResultSet rs = statement.executeQuery("SELECT USER_ID FROM USER WHERE EMAIL='" + email + "'");
            rs.next();
            Integer uID = rs.getInt(1);

            switch (userType) {
                case PATIENT :
                    Patient patient = (Patient) user;
                    String dob = patient.getDateOfBirth();
                    String bgroup = patient.getBbloodGroup().toString();
                    Address address = patient.getAddress();

                    String door = address.getDoorNumber();
                    String street = address.getStreet();
                    String city = address.getCity();
                    Integer district = address.getDistrictId();
                    Integer state = address.getStateId();
                    String pincode = address.getPincode();

                    statement.executeUpdate("INSERT INTO PATIENT VALUES(" + uID + ",'" + dob + "','" + bgroup + "')");
                    statement.executeUpdate("INSERT INTO ADDRESS VALUES("+uID+",'"+door+"','"+street+"','"+city+"',"+district+","+state+",'"+pincode+"')");
                    break;

                case DOCTOR:
                    Doctor doctor = (Doctor) user;
                    ArrayList<Days> availableDays = doctor.getDaysAvailable();

                    DoctorType docType = doctor.getDoctorType();
                    String specialization = doctor.getSpecialisation().toString();
                    String inServiceFrom = doctor.getDateSincePractising();
                    Float consultationFee = doctor.getConsultationFee();

                    statement.executeUpdate("INSERT INTO DOCTOR VALUES("+uID+",'"+specialization+"','"+inServiceFrom+"',"+consultationFee+",'"+docType+"')");

                    HashMap<Days,String> shiftStart = doctor.getShiftStartTime();
                    HashMap<Days,String> shiftEnd = doctor.getShiftEndTime();
                    HashMap<Days,Integer> cabinNumber = doctor.getCabinNumbers();

                    if(docType.equals(DoctorType.DUTY_DOCTOR))
                    {
                        for (Days day:availableDays)
                        {
                            statement.executeUpdate("INSERT INTO DUTY_DOCTOR VALUES("+uID+",'"+day.toString()+"','"+shiftStart.get(day)+"','"+shiftEnd.get(day)+"')");
                        }
                    }

                    else if (docType.equals(DoctorType.VISITING_DOCTOR))
                    {
                        for (Days day:availableDays)
                        {

                            statement.executeUpdate("INSERT INTO VISITING_DOCTOR VALUES("+uID+",'"+day.toString()+"','"+shiftStart.get(day)+"','"+shiftEnd.get(day)+"',"+cabinNumber.get(day)+")");
                        }
                    }
                    break;

                case NURSE:
                    Nurse nurse = (Nurse) user;
                    NurseDuty dutyType = nurse.getNurseDuty();
                    String specialisation = nurse.getSpecialisation().toString();
                    statement.executeUpdate("INSERT INTO NURSE VALUES("+uID+",'"+specialisation+"','"+dutyType+"','AVAILABLE')");
                    if(dutyType.equals(NurseDuty.CONSULTATION_DUTY))
                    {
                        int cabinId = nurse.getCabinId();
                        statement.executeUpdate("INSERT INTO CONSULTATION_NURSE VALUES("+uID+","+cabinId+")");
                    }
                    else if(dutyType.equals(NurseDuty.ADMITTANCE_DUTY))
                    {
                        String from = nurse.getShiftFromTime();
                        String to = nurse.getShiftToTime();
                        statement.executeUpdate("INSERT INTO ADMITTANCE_NURSE VALUES("+uID+",'"+from+"','"+to+"')");
                    }
                    break;
            }

            statement.executeUpdate("COMMIT");
            statement.executeUpdate("SET AUTOCOMMIT=1");
        }catch (SQLException e)
        {
            statement.executeUpdate("ROLLBACK");
            statement.executeUpdate("SET AUTOCOMMIT=1");
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_USER_INFO.toString());
        }
    }

    public static ResultSet getUserInfo(String username,String password)throws DaoException
    {
        try {
            ResultSet rs = statement.executeQuery("SELECT USER_ID,USER_TYPE,NAME FROM USER WHERE PASSWORD='" + password + "' AND (EMAIL='" + username + "' OR MOBILE='" + username + "')");
            return rs;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_AUTHENTICATING);
        }
    }

    public static void invalidateAccount(Integer userId)throws DaoException
    {
        try
        {
            statement.executeUpdate("UPDATE USER SET ACCOUNT_STATUS='INACTIVE' WHERE USER_ID=" + userId);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_USER_INFO);
        }
    }

}

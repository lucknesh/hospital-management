package com.hospital.manage.database;

import com.hospital.manage.Test.Examination;
import com.hospital.manage.Test.ExaminationRequest;
import com.hospital.manage.Test.Test;
import com.hospital.manage.enums.ExaminationStatus;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.time.DateTime;

import java.net.DatagramSocket;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

public class ExaminationDao {
    private static Connection connection = null;
    private static Statement statement = null;
    static
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/HOSPITAL_DATA", "root", "");
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        }catch (SQLException | ClassNotFoundException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }
    }

    public static void addTest(Test test)throws DaoException
    {
        try
        {
            String name = test.getName();
            Float fee = test.getFee();
            statement.executeUpdate("INSERT INTO TEST(NAME,FEE) VALUES('" + name + "'," + fee + ")");
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_EXAMINATION.toString());
        }
    }

    public static void updateTest(Test test)throws DaoException
    {
        try {
            Integer testId = test.getTestId();
            String name = test.getName();
            Float fee = test.getFee();
            statement.executeUpdate("UPDATE TEST SET NAME='" + name + "',FEE=" + fee + " WHERE TEST_ID=" + testId);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_EXAMINATION);
        }
    }

    public static void removeTest(Integer testId)throws DaoException
    {
        try {
            statement.executeUpdate("DELETE FROM TEST WHERE TEST_ID=" + testId);
            ResultSet rs = statement.executeQuery("SELECT MAX(TEST_ID) FROM TEST");
            rs.next();
            Integer autoIncrement = rs.getInt(1) + 1;
            statement.executeUpdate("ALTER TABLE TEST AUTO_INCREMENT =" + autoIncrement);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_EXAMINATION);
        }

    }

    public static ResultSet getAllTest()throws DaoException
    {
        try
        {
            ResultSet rs = statement.executeQuery("SELECT * FROM TEST");
            return rs;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }
    public static boolean addExamination(Examination examination) throws SQLException
    {
            statement.executeUpdate("START TRANSACTION");
            Integer appointmentId = examination.getAppointmentId();
            Integer testId = examination.getTestId();
            Float result = examination.getResult();
            String dateTime = DateTime.getTimeStamp();
            String resultFileName = examination.getResultFileName();
            String resultFilePath = examination.getResultFilePath();

            statement.executeUpdate("INSERT INTO EXAMINATION(APPOINTMENT_ID,TEST_ID,STATUS) VALUES("+appointmentId+","+testId+",'TAKEN')");
            ResultSet rs = statement.executeQuery("SELECT MAX(EXAMINATION_ID) FROM EXAMINATION");
            Integer examinationId = rs.getInt(1);

            statement.executeUpdate("INSERT INTO EXAMINATION_RESULT VALUES("+examinationId+",'"+dateTime+"',"+result+")");

            if(resultFileName != null && resultFilePath != null)
            {
                statement.executeUpdate("INSERT INTO EXAMINATION_RESULT_SCRIPT VALUES("+examinationId+",'"+resultFileName+"','"+resultFilePath+"')");
            }

            statement.executeUpdate("COMMIT");
            return true;
    }

            public static void updateExaminationResult(ArrayList<Examination> examinations) throws DaoException
    {
        try {
            statement.executeUpdate("START TRANSACTION");
            for (Examination examination : examinations) {
                Integer examinationId = examination.getExaminationId();
                Integer appointmentId = examination.getAppointmentId();
                Integer testId = examination.getTestId();
                Float result = examination.getResult();
                String dateTime = DateTime.getTimeStamp();
                String resultFileName = null;
                String resultFilePath = null;
                try {
                    resultFileName = examination.getResultFileName();
                    resultFilePath = examination.getResultFilePath();
                } catch (Exception e) {

                }

                statement.executeUpdate("UPDATE EXAMINATION SET STATUS='TAKEN' WHERE EXAMINATION_ID=" + examinationId);

                statement.executeUpdate("INSERT INTO EXAMINATION_RESULT VALUES(" + examinationId + ",'" + dateTime + "'," + result + ")");

                if (resultFileName != null && resultFilePath != null) {
                    statement.executeUpdate("INSERT INTO EXAMINATION_RESULT_SCRIPT VALUES(" + examinationId + ",'" + resultFileName + "','" + resultFilePath + "')");
                }
            }

            statement.executeUpdate("COMMIT");
        }catch (SQLException e)
        {
            try
            {
                statement.executeUpdate("ROLLBACK");
            }
            catch (SQLException se)
            {
                System.out.println(e);
                e.printStackTrace();
            }

            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_EXAMINATION);
        }
    }

//    public static boolean addExaminationRequest(ExaminationRequest request)throws DaoException
//    {
//            Integer appointmentId = request.getAppointMentId();
//            ArrayList<Integer> testIds = request.getTestIds();
//
//            for(Integer testId : testIds)
//            {
//                statement.executeUpdate("INSERT INTO EXAMINATION(APPOINTMENT_ID,TEST_ID) VALUES("+appointmentId+","+testId+")");
//            }
//            return true;
//
//    }
    public static void addExaminationRequest(ArrayList<Integer> testIds,Integer consultationId)throws DaoException
    {
        try
        {
            for (Integer testId : testIds) {
                statement.executeUpdate("INSERT INTO EXAMINATION(APPOINTMENT_ID,TEST_ID) VALUES(" + consultationId + "," + testId + ")");
            }
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_EXAMINATION);
        }

    }

    public static ResultSet getRequestedExaminations(Integer appointmentId )throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT T.NAME,T.TEST_ID,E.EXAMINATION_ID FROM TEST T,EXAMINATION E WHERE T.TEST_ID IN (SELECT TEST_ID FROM EXAMINATION WHERE APPOINTMENT_ID=" + appointmentId + " AND STATUS='YET_TO_TAKE') AND E.TEST_ID = T.TEST_ID AND E.APPOINTMENT_ID=" + appointmentId);
            return resultSet;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }
    public static ResultSet getExaminationResult(Integer appointmentId)throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT T.NAME,R.RESULT,R.DATE_TIME FROM TEST T,EXAMINATION_RESULT R,EXAMINATION E WHERE E.TEST_ID = T.TEST_ID AND E.APPOINTMENT_ID=" + appointmentId + " AND E.EXAMINATION_ID=R.EXAMINATION_ID");
            return resultSet;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static ResultSet getMyExaminations(Integer appointmentId)throws DaoException
    {
        try {
            return statement.executeQuery("SELECT DISTINCT(E.TEST_ID),T.NAME,COUNT(E.TEST_ID),T.FEE*COUNT(E.TEST_ID) FROM TEST T,EXAMINATION E WHERE E.APPOINTMENT_ID = " + appointmentId + " AND E.TEST_ID=T.TEST_ID GROUP BY E.TEST_ID");
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }
}

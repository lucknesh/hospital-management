package com.hospital.manage.database;

import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.inventory.Item;

import javax.servlet.jsp.tagext.TryCatchFinally;
import java.sql.*;

public class InventoryDao {
    private static Connection connection = null;
    private static Statement statement = null;
    static
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/HOSPITAL_DATA", "root", "");
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        }catch (SQLException | ClassNotFoundException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }
    }
    public static void addItem(Item item)throws DaoException
    {
    try {
        String name = item.getName();
        Integer quantity = item.getQuantity();
        Float price = item.getPrice();

        statement.executeUpdate("INSERT INTO INVENTORY(NAME,QUANTITY,PRICE) VALUES('" + name + "'," + quantity + "," + price + ")");
    }
    catch (SQLException e)
    {
        System.out.println(e);
        e.printStackTrace();
        throw new DaoException(ExceptionMessage.ERROR_UPDATING_INVENTORY);
    }

    }

    public static void updateItem(Item item)throws DaoException
    {
        try {
            String name = item.getName();
            Integer itemId = item.getItemId();
            Float price = item.getPrice();
            statement.executeUpdate("UPDATE INVENTORY SET NAME='" + name + "',PRICE=" + price + " WHERE ITEM_ID=" + itemId);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_INVENTORY);
        }
    }

    public static void changeQuantity(Integer itemId,Integer quantity,Integer sign)throws DaoException
    {
        try
        {
            quantity *= sign;
            statement.executeUpdate("UPDATE INVENTORY SET QUANTITY = QUANTITY + " + quantity + " WHERE ITEM_ID=" + itemId);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_INVENTORY.toString());
        }

    }

    public static void removeItem(Integer itemId)throws DaoException
    {
        try
        {
            statement.executeUpdate("DELETE FROM INVENTORY WHERE ITEM_ID=" + itemId);
            ResultSet rs = statement.executeQuery("SELECT MAX(ITEM_ID) FROM INVENTORY");
            rs.next();
            Integer autoIncrement = rs.getInt(1) + 1;
            statement.executeUpdate("ALTER TABLE INVENTORY AUTO_INCREMENT =" + autoIncrement);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_INVENTORY);
        }

    }

    public static ResultSet getAvailableItems()throws SQLException
    {

            ResultSet rs = statement.executeQuery("SELECT * FROM INVENTORY WHERE QUANTITY > 0");
            return rs;

    }
    public static ResultSet getAllItems()throws SQLException
    {

            ResultSet rs = statement.executeQuery("SELECT * FROM INVENTORY");
            return rs;

    }
    public static ResultSet getAddedResourced(Integer appointmentId)throws DaoException
    {
    try {
        ResultSet resultSet = statement.executeQuery("SELECT I.NAME,R.QUANTITY FROM RESOURCES_USED R,INVENTORY I WHERE R.ITEM_ID=I.ITEM_ID AND R.APPOINTMENT_ID=" + appointmentId);
        return resultSet;
    }catch (SQLException e)
    {
        System.out.println(e);
        e.printStackTrace();

        throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
    }

    }

    public static void addUsedResource(Integer appointmentId,Item item)throws DaoException
    {
            try {
                statement.executeUpdate("START TRANSACTION");

                    Integer itemId = item.getItemId();
                    ResultSet resultSet = statement.executeQuery("SELECT * FROM RESOURCES_USED WHERE  APPOINTMENT_ID=" + appointmentId + " AND ITEM_ID=" + itemId);
                    Integer quantity = item.getQuantity();
                    if (resultSet.next()) {
                        statement.executeUpdate("UPDATE RESOURCES_USED SET QUANTITY = QUANTITY+" + quantity + " WHERE ITEM_ID=" + itemId + " AND APPOINTMENT_ID=" + appointmentId);
                    } else {
                        statement.executeUpdate("INSERT INTO RESOURCES_USED VALUES(" + appointmentId + "," + itemId + "," + quantity + ")");
                    }
                    statement.executeUpdate("UPDATE INVENTORY SET QUANTITY = QUANTITY - " + quantity + " WHERE ITEM_ID=" + itemId);

                statement.executeUpdate("COMMIT");
            } catch (SQLException e)
            {
                try
                {
                    statement.executeUpdate("ROLLBACK");
                }
                catch (SQLException se)
                {
                    System.out.println(e);
                    e.printStackTrace();
                }
                System.out.println(e);
                e.printStackTrace();

                throw new DaoException(ExceptionMessage.ERROR_UPDATING_INVENTORY);
            }

    }

    public static ResultSet getUsedResources(Integer appointmentId)throws DaoException
    {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT I.NAME,R.QUANTITY,I.PRICE*R.QUANTITY FROM INVENTORY I,RESOURCES_USED R WHERE I.ITEM_ID = R.ITEM_ID AND R.APPOINTMENT_ID =" + appointmentId);
            return resultSet;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }



}

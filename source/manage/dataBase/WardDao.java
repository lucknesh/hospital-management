package com.hospital.manage.database;

import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.ward.Cabin;
import com.hospital.manage.ward.Ward;

import java.sql.*;

public class WardDao {
    private static Connection connection = null;
    private static Statement statement = null;
    static
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/HOSPITAL_DATA", "root", "");
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        }catch (SQLException | ClassNotFoundException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }
    }
    public static void addWard(Ward ward) throws DaoException
    {
        String name = ward.getName();
        Integer from = ward.getFromCabinNumber();
        Integer to = ward.getToCabinNumber();

        try {
            statement.executeUpdate("START TRANSACTION");
            statement.executeUpdate("INSERT INTO WARD(NAME) VALUES('" + name + "')");
            if (from > 0 && to > 0) {
                ResultSet rs = statement.executeQuery("SELECT MAX(WARD_ID) FROM WARD");
                rs.next();
                Integer ward_id = rs.getInt(1);
                for (Integer i = from; i <= to; i++)
                {
                    statement.executeUpdate("INSERT INTO CABIN(WARD_ID,CABIN_NUMBER) VALUES(" + ward_id + "," + i + ")");
                }
            }
            statement.executeUpdate("COMMIT");
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO);
        }

    }

    public static void addCabin(Cabin cabin)throws DaoException
    {
        try
        {
            Integer cabinNumber = cabin.getCabinNumber();
            Integer wardId = cabin.getWardId();
            statement.executeUpdate("INSERT INTO CABIN(WARD_ID,CABIN_NUMBER) VALUES(" + wardId + "," + cabinNumber + ")");
        }catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO.toString());
        }
    }

    public static void updateWard(Ward ward)throws DaoException
    {
        try
        {

            Integer wardId = ward.getWardId();
            String newName = ward.getName();
            statement.executeUpdate("UPDATE WARD SET NAME='"+newName+"' WHERE WARD_ID="+wardId);
        }catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO.toString());
        }
    }

    public static void removeWard(Integer wardId)throws DaoException
    {
        try {
            statement.executeUpdate("DELETE FROM WARD WHERE WARD_ID=" + wardId);
            ResultSet rs = statement.executeQuery("SELECT MAX(WARD_ID) FROM WARD");
            rs.next();
            Integer autoIncrement = rs.getInt(1) + 1;
            statement.executeUpdate("ALTER TABLE WARD AUTO_INCREMENT =" + autoIncrement);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO);
        }
    }

    public static void removeCabin(Integer cabinNumber)throws DaoException
    {
        try
        {
            statement.executeUpdate("DELETE FROM CABIN WHERE CABIN_NUMBER=" + cabinNumber);
            ResultSet rs = statement.executeQuery("SELECT MAX(ID) FROM CABIN");
            rs.next();
            Integer autoIncrement = rs.getInt(1) + 1;
            statement.executeUpdate("ALTER TABLE CABIN AUTO_INCREMENT =" + autoIncrement);
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO);
        }
    }
    public static ResultSet getAllWards()throws DaoException
    {
        try {
            ResultSet rs = statement.executeQuery("SELECT * FROM WARD");
            return rs;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public static ResultSet getAvailableCabins(Integer wardId,String day)throws DaoException
    {
        try {
            ResultSet rs = statement.executeQuery("SELECT ID,CABIN_NUMBER FROM CABIN WHERE WARD_ID=" + wardId + " AND ID NOT IN (SELECT IFNULL(CABIN_ID,0) FROM VISITING_DOCTOR WHERE AVAILABLE_DAY='" + day + "')");
            return rs;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public static ResultSet getNoNurseCabins()throws DaoException
    {
        try {
            ResultSet rs = statement.executeQuery("SELECT ID,CABIN_NUMBER FROM CABIN WHERE ID NOT IN (SELECT CABIN_ID FROM CONSULTATION_NURSE) ");
            return rs;
        }
        catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }
    public static ResultSet getNoNurseCabins(Integer wardId)throws DaoException
    {
        try {
            ResultSet rs = statement.executeQuery("SELECT ID,CABIN_NUMBER FROM CABIN WHERE WARD_ID=" + wardId + " AND ID NOT IN (SELECT IFNULL(CABIN_ID,0) FROM CONSULTATION_NURSE) ");
            return rs;
        }catch (SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();

            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }
}

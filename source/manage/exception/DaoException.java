package com.hospital.manage.exception;

import com.hospital.manage.enums.ExceptionMessage;

public class DaoException extends Exception
{
    public DaoException (String message)
    {
        super(message);
    }

    public DaoException (ExceptionMessage message)
    {
        super(message.toString());
    }
}


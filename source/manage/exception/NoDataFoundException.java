package com.hospital.manage.exception;

import com.hospital.manage.enums.ExceptionMessage;

public class NoDataFoundException extends Exception
{
    public NoDataFoundException (ExceptionMessage exceptionMessage)
    {

        super(exceptionMessage.toString());
    }
    public NoDataFoundException(String message)
    {
        super(message);
    }

}


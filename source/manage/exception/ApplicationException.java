package com.hospital.manage.exception;

import com.hospital.manage.enums.ExceptionMessage;

public class ApplicationException extends Exception{
    public ApplicationException(ExceptionMessage message)
    {
        super(message.toString());
    }
}

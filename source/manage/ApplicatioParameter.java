package manage;

public class ApplicatioParameter {
    private static Float E_BOOKING_LIMIT;
    private static Float RESERVATION_OPEN_IN_ADVANCE;
    private static Float TAX;
    private static Float NEW_PARAMETER;

    public static Float geteBookingLimit() {
        return E_BOOKING_LIMIT;
    }

    public static void seteBookingLimit(Float eBookingLimit) {
        E_BOOKING_LIMIT = eBookingLimit;
    }

    public static Float getReservationOpenInAdvance() {
        return RESERVATION_OPEN_IN_ADVANCE;
    }

    public static void setReservationOpenInAdvance(Float reservationOpenInAdvance)
    {
        RESERVATION_OPEN_IN_ADVANCE = reservationOpenInAdvance;
    }
    public static Float getTAX() {
        return TAX;
    }
    public static void setTAX(Float TAX) {
        ApplicatioParameter.TAX = TAX;
    }
}

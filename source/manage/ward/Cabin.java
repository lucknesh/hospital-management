package com.hospital.manage.ward;

public class Cabin {
    private Integer wardId;
    private Integer cabinId;
    private Integer cabinNumber;

    public Integer getWardId() {
        return wardId;
    }

    public void setWardId(Integer wardId) {
        this.wardId = wardId;
    }

    public Integer getCabinId() {
        return cabinId;
    }

    public void setCabinId(Integer cabinId) {
        this.cabinId = cabinId;
    }
    public Integer getCabinNumber() {
        return cabinNumber;
    }
    public void setCabinNumber(Integer cabinNumber) {
        this.cabinNumber = cabinNumber;
    }
}

package com.hospital.manage.ward;

public class Ward {
    private String name;
    private Integer wardId;
    private Integer fromCabinNumber;
    private Integer toCabinNumber;

    public Integer getWardId() {
        return wardId;
    }

    public void setWardId(Integer wardId) {
        this.wardId = wardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFromCabinNumber() {
        return fromCabinNumber;
    }

    public void setFromCabinNumber(Integer fromCabinNumber) {
        this.fromCabinNumber = fromCabinNumber;
    }

    public Integer getToCabinNumber() {
        return toCabinNumber;
    }

    public void setToCabinNumber(Integer toCabinNumber) {
        this.toCabinNumber = toCabinNumber;
    }

}

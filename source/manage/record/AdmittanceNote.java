package com.hospital.manage.record;

import com.hospital.manage.enums.AdmittanceNoteType;

public class AdmittanceNote {

    private Integer writerId;
    private Integer noteId;
    private Integer admittanceId;
    private String dateTime;
    private String note;
    private AdmittanceNoteType admittanceNoteType;

    public Integer getWriterId() {
        return writerId;
    }

    public void setWriterId(Integer writerId) {
        this.writerId = writerId;
    }

    public Integer getNoteId() {
        return noteId;
    }

    public void setNoteId(Integer noteId) {
        this.noteId = noteId;
    }

    public Integer getAdmittanceId() {
        return admittanceId;
    }

    public void setAdmittanceId(Integer admittanceId) {
        this.admittanceId = admittanceId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public AdmittanceNoteType getAdmittanceNoteType() {
        return admittanceNoteType;
    }

    public void setAdmittanceNoteType(AdmittanceNoteType admittanceNoteType) {
        this.admittanceNoteType = admittanceNoteType;
    }
}

package com.hospital.manage.record;

import com.hospital.manage.Test.Test;

import java.util.HashMap;

public class OutPatientEntry {

    private HashMap<String,Integer> resourceAndQuantity = new HashMap<>();
    private String illnessName;
    private String doctorsNote;
    private String nextVisit;
    private Float amount;
    private Integer appointmentID;
    private Test tests;

    public String getIllnessName() {
        return illnessName;
    }

    public HashMap<String, Integer> getResourceAndQuantity() {
        return resourceAndQuantity;
    }

    public void setResourceAndQuantity(HashMap<String, Integer> resourceAndQuantity) {
        this.resourceAndQuantity = resourceAndQuantity;
    }

    public String getDoctorsNote() {
        return doctorsNote;
    }

    public void setDoctorsNote(String doctorsNote) {
        this.doctorsNote = doctorsNote;
    }

    public void setIllnessName(String illnessName) {
        this.illnessName = illnessName;
    }

    public Integer getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(Integer appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getNextVisit() {
        return nextVisit;
    }

    public void setNextVisit(String nextVisit) {
        this.nextVisit = nextVisit;
    }

    public Test getTests() {
        return tests;
    }

    public void setTests(Test tests) {
        this.tests = tests;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }


}

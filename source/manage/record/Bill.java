package com.hospital.manage.record;

import com.hospital.manage.enums.BillStatus;

public class Bill {
    private Integer appointmentId;
    private Float billAmount;
    private BillStatus billStatus;
    private String billScriptPath;

    public Integer getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Integer appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Float getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(Float billAmount) {
        this.billAmount = billAmount;
    }

    public BillStatus getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(BillStatus billStatus) {
        this.billStatus = billStatus;
    }

    public String getBillScriptPath() {
        return billScriptPath;
    }

    public void setBillScriptPath(String billScriptPath) {
        this.billScriptPath = billScriptPath;
    }
}

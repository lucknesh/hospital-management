package com.hospital.manage.Test;

import com.hospital.manage.enums.ExaminationStatus;

import java.util.ArrayList;
import java.util.HashMap;

public class ExaminationRequest {
    private Integer appointMentId;
    private ArrayList<Integer> testIds = new ArrayList<>();
    private HashMap<Integer, ExaminationStatus> requestStatus = new HashMap<>();
    private HashMap<Integer,Integer> testIdAndExaminationId = new HashMap<>();

    public Integer getAppointMentId() {
        return appointMentId;
    }

    public void setAppointMentId(Integer appointMentId) {
        this.appointMentId = appointMentId;
    }

    public ArrayList<Integer> getTestIds() {
        return testIds;
    }

    public void setTestIds(ArrayList<Integer> testIds) {
        this.testIds = testIds;
    }

    public HashMap<Integer, ExaminationStatus> getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(HashMap<Integer, ExaminationStatus> requestStatus) {
        this.requestStatus = requestStatus;
    }

    public HashMap<Integer, Integer> getTestIdAndExaminationId() {
        return testIdAndExaminationId;
    }

    public void setTestIdAndExaminationId(HashMap<Integer, Integer> testIdAndExaminationId) {
        this.testIdAndExaminationId = testIdAndExaminationId;
    }
}

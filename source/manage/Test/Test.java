package com.hospital.manage.Test;

public class Test {
    private Integer testId;
    private Float fee;
    private String name;

    public void setTestId(Integer testId) {
        this.testId = testId;
    }

    public void setFee(Float fee) {
        this.fee = fee;
    }

    public Integer getTestId() {
        return testId;
    }


    public Float getFee() {
        return fee;
    }

    public void setFee(float fee) {
        this.fee = fee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.hospital.manage.enums;

public enum BloodGroup {
    A_POSITIVE,
    B_POSITIVE,
    O_POSITIVE,
    AB_POSITIVE,
    A_NEGATIVE,
    B_NEGATIVE,
    O_NEGATIVE,
    AB_NEGATIVE;
}
//'A_POSITIVE','B_POSITIVE','O_POSITIVE','AB_POSITIVE','A_NEGATIVE','B_NEGATIVE','O_NEGATIVE','AB_NEGATIVE'


package com.hospital.manage.enums;

public enum NurseDuty {
    CONSULTATION_DUTY, ADMITTANCE_DUTY;
}

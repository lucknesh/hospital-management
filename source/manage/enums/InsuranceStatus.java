package com.hospital.manage.enums;

public enum InsuranceStatus {
    INSURANCE_CLAIM_INITIATED,INSURANCE_AMOUNT_RECEIVED,YET_TO_CLAIM;
}

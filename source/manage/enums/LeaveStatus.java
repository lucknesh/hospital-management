package com.hospital.manage.enums;

public enum LeaveStatus {
    REQUESTED,APPROVED,DECLINED;
}

package com.hospital.manage.enums;

public enum BillStatus {
    PAID, YET_TO_PAY;
}

package com.hospital.manage.users;

import com.hospital.manage.enums.NurseDuty;
import com.hospital.manage.enums.NurseSpecialisation;
import com.hospital.manage.enums.UserType;

public class Nurse extends User {
    private NurseSpecialisation Specialization;
    private NurseDuty nurseDuty;
    private Integer cabinId;
    private String shiftFromTime;
    private String shiftToTime;
    public NurseSpecialisation getSpecialization() {
        return Specialization;
    }
    public void setSpecialization(NurseSpecialisation specialization) {
        Specialization = specialization;
    }
    public String getShiftFromTime() {
        return shiftFromTime;
    }
    public void setShiftFromTime(String shiftFromTime) {
        this.shiftFromTime = shiftFromTime;
    }
    public String getShiftToTime() {
        return shiftToTime;
    }
    public void setShiftToTime(String shiftToTime) {
        this.shiftToTime = shiftToTime;
    }
    public NurseDuty getNurseDuty() {
        return nurseDuty;
    }
    public void setNurseDuty(NurseDuty nurseDuty) {
        this.nurseDuty = nurseDuty;
    }

    public Integer getCabinId() {
        return cabinId;
    }

    public void setCabinId(Integer cabinId) {
        this.cabinId = cabinId;
    }

    public Nurse(){super(UserType.NURSE);}

    public NurseSpecialisation getSpecialisation() {
        return Specialization;
    }

    public void setSpecialisation(NurseSpecialisation specialization) {
        this.Specialization = specialization;
    }
}

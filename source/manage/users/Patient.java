package com.hospital.manage.users;


import com.hospital.manage.database.UserDao;
import com.hospital.manage.enums.*;

public class Patient extends User
{
    private Address address;
    private Integer age;
    private String fullAddress;
    private String dateOfBirth;
    private BloodGroup bloodGroup;

    public Patient(String name,Integer userID)
    {
        super(name,userID,UserType.PATIENT);
    }

    public Patient(){super(UserType.PATIENT);}
    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }
    public BloodGroup getBloodGroup() {
        return bloodGroup;
    }
    public String getDateOfBirth()
    {
        return dateOfBirth;
    }
    public void setDateOfBirth(String dateOfBirth)
    {
        this.dateOfBirth = dateOfBirth;
    }
    public BloodGroup getBbloodGroup()
    {
        return bloodGroup;
    }
    public void setBloodGroup(BloodGroup bloodGroup)
    {
        this.bloodGroup = bloodGroup;
    }
    public Integer getAge()
    {
        return age;
    }
    public void setAge(Integer age)
    {
        this.age = age;
    }
    public String getFullAddress()
    {
        return fullAddress;
    }
    public void setFullAddress(String fullAddress)
    {
        this.fullAddress = fullAddress;
    }


}

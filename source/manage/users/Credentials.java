package com.hospital.manage.users;

import java.io.PrintWriter;

public class Credentials
{
    private String username;
    private String password;
    private Credentials(String username,String password)
    {
        this.username = username;
        this.password = password;
    }

    public static Credentials getInstance(String username,String password)
    {
        return new Credentials(username,password);
    }
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
}

package com.hospital.manage.users;

import com.hospital.manage.enums.Days;
import com.hospital.manage.enums.DoctorSpecialisation;
import com.hospital.manage.enums.DoctorType;
import com.hospital.manage.enums.UserType;

import java.util.ArrayList;
import java.util.HashMap;

public class Doctor extends User
{
    private ArrayList<Days> daysAvailable = new ArrayList<>();
    private HashMap<Days,String> shiftStartTime = new HashMap<>();
    private HashMap<Days,String> shiftEndTime = new HashMap<>();
    private HashMap<Days,Integer> cabinNumbers = new HashMap<>();
    private DoctorType doctorType;
    private DoctorSpecialisation specialisation;
    private String dateSincePractising;
    private Float consultationFee;
    public Doctor(){
        super(UserType.DOCTOR);
    }

    public DoctorType getDoctorType() {
        return doctorType;
    }
    public void setDoctorType(DoctorType doctorType) {
        this.doctorType = doctorType;
    }
    public HashMap<Days, Integer> getCabinNumbers() {
        return cabinNumbers;
    }
    public void setCabinNumbers(HashMap<Days, Integer> cabinNumbers) {
        this.cabinNumbers = cabinNumbers;
    }
    public String getDateSincePractising() {
        return dateSincePractising;
    }
    public void setDateSincePractising(String dateSincePractising) {
        this.dateSincePractising = dateSincePractising;
    }
    public HashMap<Days, String> getShiftStartTime() {
        return shiftStartTime;
    }
    public void setShiftStartTime(HashMap<Days, String> shiftStartTime) {
        this.shiftStartTime = shiftStartTime;
    }
    public HashMap<Days, String> getShiftEndTime() {
        return shiftEndTime;
    }
    public void setShiftEndTime(HashMap<Days, String> shiftEndTime) {
        this.shiftEndTime = shiftEndTime;
    }

    public DoctorSpecialisation getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(DoctorSpecialisation specialisation) {
        this.specialisation = specialisation;
    }
    public Float getConsultationFee()
    {
        return consultationFee;
    }
    public void setConsultationFee(Float consultationFee)
    {
        this.consultationFee = consultationFee;
    }
    public void addDay(Days day){
        daysAvailable.add(day);
    }
    public ArrayList<Days> getDaysAvailable(){
        return daysAvailable;
    }
    public void setDaysAvailable(ArrayList<Days> list)
    {
        this.daysAvailable = list;
    }
}

package com.hospital.manage.users;

import com.hospital.manage.enums.UserType;

public class User
{

    private String username;
    private String name;
    private String mobile;
    private String email;
    private Integer userId;
    private UserType userType;

    private String password;
    private Credentials credentials;

    public User(UserType userType)
    {
        this.setUserType(userType);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User(String name, Credentials credentials, UserType userType)
    {
        this.userType = userType;
        this.name = name;
        this.credentials = credentials;
    }
    public User(String name,Integer userID,UserType userType)
    {
        this.name = name;
        this.userType = userType;
        this.userId = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public User(){}

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Integer getUserId()
    {
        return userId;
    }

    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
    public Credentials getCredentials()
    {
        return credentials;
    }
    public void setCredentials(Credentials credentials)
    {
        this.credentials = credentials;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}

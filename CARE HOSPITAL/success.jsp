<%@ page import="com.opensymphony.xwork2.util.ValueStack" %>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%
    String redirectUrl = (String)(ActionContext.getContext().getValueStack().findValue("redirectUrl"));
%>
<!DOCTYPE html>
<html>
<style>
.pop-up-button {
  background-color: orange;
  border: none;
  color: white;
  padding: 12px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  transition-duration: 0.4s;
  cursor: pointer;
  border-radius : 2px;
}

.pop-up-button1 {
  background-color: orange;
  color: white;
}

.pop-up-button1:hover {
  color: #1f456e ;
}
</style>
<head>
<title>Popup Message</title>

</head>
<body>
<link rel="stylesheet" href="design.css" >
<div class="popup-container">

  <div class="popup-box">

    <div class="popup-message">
      <h2 class="popup-title"> SUCCESS :-) </h2>
      <p class="popup-text"> PROCESS COMPLETED SUCCESSFULLY </p>
    </div>
    <a href=<% out.print(redirectUrl);  %> class='pop-up-button pop-up-button1'>OK</a>
  </div>
</div>
</body>
</html>

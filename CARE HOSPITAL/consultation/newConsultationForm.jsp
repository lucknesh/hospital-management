<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='s' uri='/struts-tags' %>
<%@page import="java.time.*" %>
<%@ page import="com.hospital.manage.users.Admin,com.hospital.manage.database.*" %>
<%@ page import="com.hospital.manage.users.*,com.hospital.manage.enums.*" %>
          <%
                User user = (User)session.getAttribute("user");
                Integer userId = user.getUserId();
                UserType userType = user.getUserType();
                String userName = user.getName();

                String homeUrl = "patientInterface";
                if(userType.equals(UserType.RECEPTIONIST)) homeUrl = "receptionistInterface";

          %>
<html lang="en" xmlns="http://www.w3.org/1999/html">
    <body style="background-color:#1f456e;">
    <link rel="stylesheet" type="text/css" href="design.css">
<div>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <div class="header">
  <a href= <% out.print(homeUrl); %> ><div class="logo_holder"><h class="hospital"><b>CARE + </b></h></div></a>
  <div class="logout_holder">
  <form action="Logout">
  <input type="submit" value="Logout" class="button button1">
  </form>
  <div>
  </div><br>
        <div class="header_bottom"><div class='name-holder'><% out.print(userType+" : "+userName); %></div></div>
  <title>appointment</title>
</head>
  <div class="frame">
<body><pre>
<form action="DoctorSelectingForm" method="post">
<%
    if(userType.equals(UserType.RECEPTIONIST))
    {
        out.print("<h class='form-text'>PATIENT ID  :   </h><input class='get' type='number' name='patientId' min=1 required><br><br><br>");
    }

  LocalDate today = LocalDate.now();
  out.print("<h class='form-text'>DATE            :   </h><input class='get' type='date' name='date' value='"+today+"' min='"+today+"' max='"+today.plusDays(10)+"'><br>");
%>

<br><label for="specialisation"><h class='form-text'>DOCTOR      : </h></label> <select class='get' name="specialisation">
  <option value="ENT">ENT</option>
  <option value="DENTIST">DENTIST</option>
  <option value="UROLOGIST">UROLOGIST</option>
  <option value="GENERAL">GENERAL</option>
  <option value="PSYCHIATRIST">PSYCHIATRIST</option>
</select><br><br>
        <input  class="form-button form-button1" type="submit" value="GO"> <% if(userType.equals(UserType.RECEPTIONIST)){out.print("<a href='newPatientRegistrationForm' class='form-button form-button1'  >REGISTER PATIENT</a>");} %>
</form></pre>
</body>
</div>
</html>
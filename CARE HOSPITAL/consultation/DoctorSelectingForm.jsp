
<%@ page import="com.hospital.manage.users.Patient,java.sql.ResultSet,java.time.LocalDate,java.util.ArrayList" %>
<%@ page import="com.hospital.manage.users.Admin,com.hospital.manage.database.*" %>
<%@ page import="com.hospital.manage.users.*" %>
<%@ page import="com.hospital.manage.enums.*" %>
          <%
                User user = (User)session.getAttribute("user");
                Integer userId = user.getUserId();
                UserType userType = user.getUserType();
                String userName = user.getName();

                String homeUrl = "patientInterface";
                int patientId = userId;
                if(userType.equals(UserType.RECEPTIONIST))
                {
                homeUrl = "receptionistInterface";
                patientId = Integer.parseInt(request.getParameter("patientId"));
                }

                if(!(AuthenticatorDao.isValidPatient(patientId)))
                {
                    RequestDispatcher rd = request.getRequestDispatcher("newConsultationForm.jsp");
                    rd.forward(request,response);
                }

          %>
<html>
    <body style="background-color:#1f456e;">
    <link rel="stylesheet" type="text/css" href="design.css">
<head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <div class="header">
        <a href= <% out.print(homeUrl); %> ><div class="logo_holder"><h class="hospital"><b>CARE + </b></h></div></a>
        <div class="logout_holder">
        <form action="Logout">
        <input type="submit" value="Logout" class="button button1">
        </form>
        <div>
        </div><br>
        <div class="header_bottom"><div class='name-holder'><% out.print(userType+" : "+userName); %></div></div>
        <title>appointment</title>
</head>
<div class="frame">
    <body>
          <%

                String date       = request.getParameter("date");
                String specialisation = request.getParameter("specialisation");

                String bookingType = null;

                if(userType.equals(UserType.PATIENT)) bookingType="ONLINE";
                if(userType.equals(UserType.RECEPTIONIST)) bookingType="OFFLINE";


                int year  = Integer.parseInt(date.substring(0, 4));
                int month = Integer.parseInt(date.substring(5, 7));
                int day   = Integer.parseInt(date.substring(8, 10));

                LocalDate local = LocalDate.of(year, month, day);

                String dayOfWeek = local.getDayOfWeek().toString().substring(0, 3);

                ResultSet rs = DoctorDao.getAllAvailableDoctors(dayOfWeek,date,specialisation,bookingType);
                //ResultSet rs=DoctorDao.getAvailableDoctors(dayOfWeek,specialisation);
                ArrayList<Integer> AvailableDoctorIDs = new ArrayList<>();

                while(rs.next())
                {
                    AvailableDoctorIDs.add(rs.getInt(1));
                    //AvailableDoctorIDs.add(rs.getInt(2));
                }

                session.setAttribute("date",date);
                session.setAttribute("doctorType",specialisation);
                rs = DoctorDao.getAvailableDoctors(dayOfWeek,specialisation);

                local = LocalDate.now();
                int localYear = Integer.parseInt(local.toString().substring(0,4));

                if(rs.next())
                {
                      rs.previous();
                      out.print("<pre>");
                      while (rs.next())
                      {
                        int experience = localYear-Integer.parseInt(rs.getString(3).substring(0,4));
                        String buttonValue = null;
                        String name = rs.getString(1).toUpperCase();
                        float fee = rs.getFloat(4);
                        int doctorId = rs.getInt(2);



                        if(AvailableDoctorIDs.contains(doctorId))
                        {
                          buttonValue = "NAME                          : "+name+"\nEXPERIENCE             : "+experience+" YEARS\nCONSULTATION FEE : "+fee+" RS";
                          out.print("<form action='bookConsultation' method='post'>");
                        }
                        else if(bookingType.equals("ONLINE"))
                        {
                          buttonValue = "NAME                          : "+rs.getString(1).toUpperCase()+"\nEXPERIENCE             : "+experience+" YEARS\nCONSULTATION FEE : "+rs.getFloat(4)+" RS\n\nONLINE BOOKING NOT AVAILABLE ! \n                   TRY OFFLINE .";
                          out.print("<form  method='post'>");
                        }
                        else if(bookingType.equals("OFFLINE"))
                        {
                          buttonValue = "NAME                          : "+rs.getString(1).toUpperCase()+"\nEXPERIENCE             : "+experience+" YEARS\nCONSULTATION FEE : "+rs.getFloat(4)+" RS\n\nOFFLINE BOOKING CLOSED !";
                          out.print("<form  method='post'>");
                        }
                        System.out.println(doctorId);
                        out.print("<input type='hidden' name='doctorId' value="+doctorId+">");
                        out.print("<input type='hidden' name='bookingType' value='"+bookingType+"'>");
                        out.print("<input type='hidden' name='patientId' value='"+patientId+"'>");
                        out.print("<input type='hidden' name='date' value='"+date+"'>");
                        if(AvailableDoctorIDs.contains(doctorId))
                        {
                            out.print("<input class='select select1' type='submit' value='"+buttonValue+"' ></form><br><br>");
                        }
                        else
                        {
                            out.print("</form><input class='select select1' type='submit' value='"+buttonValue+"' style='background-color: #faa0a0;' ><br><br>");                        }
                      }
                      out.print("</pre>");
                }
                else
                {
                      request.setAttribute("title", "Sorry !");
                      request.setAttribute("to", "newConsultationForm");
                      request.setAttribute("message", "No "+specialisation.toLowerCase()+" doctor available on "+date+"");
                      RequestDispatcher rd = request.getRequestDispatcher("popUp.jsp");
                      rd.include(request, response);
                }

          %>

        </pre>
    </body>
    </div>
</html>
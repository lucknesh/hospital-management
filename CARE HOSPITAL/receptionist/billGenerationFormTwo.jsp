<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page import="java.sql.*" %>
<%@ page import="com.hospital.manage.database.*" %>
<%@ page import="com.hospital.manage.users.*" %>
<%@ page import="com.hospital.manage.exception.*" %>
<%@ page import="com.hospital.manage.users.*,com.hospital.manage.enums.*" %>
<%@ page import="javax.sql.rowset.CachedRowSet" %>
<%@ page import="javax.sql.rowset.RowSetProvider" %>
          <%
                User user = (User)session.getAttribute("user");
                Integer userId = user.getUserId();
                UserType userType = user.getUserType();
                String userName = user.getName();

                int appointmentId = Integer.parseInt(request.getParameter("appointmentId"));
                AppointmentType appointmentType = null;
                try
                {
                appointmentType = AppointmentDao.isValidateAppointmentType(appointmentId);
                }
                catch(NoDataFoundException e)
                {
                %>
                <c:redirect url = "billGenerationFormOne"/>
                <%
                }
          %>
<html xmlns="http://www.w3.org/1999/html">
<style>
        .table-frame {
        height: 500px;
        width: 800px;

        position: fixed;
        top: 20%;
        left: 21%;
        }

        .table-element{
        background-color:white;
        border : solid grey;
        font-family: Arial, Helvetica, sans-serif;
        }

        .pop-up-button {
          background-color: orange;
          border: none;
          color: white;
          padding: 12px 32px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
          transition-duration: 0.4s;
          cursor: pointer;
          border-radius : 2px;
        }

        .pop-up-button1 {
          background-color: orange;
          color: white;
        }

        .pop-up-button1:hover {
          color: #1f456e ;
        }
</style>
        <body style="background-color:#1f456e;">
    <link rel="stylesheet" type="text/css" href="design.css">
<head>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <div class="header">
        <a href="receptionistInterface"><div class="logo_holder"><h class="hospital"><b>CARE + </b></h></div></a>
        </div><br>
        <div class="logout_holder">
        <form action="Logout">
        <input type="submit" value="Logout" class="button button1">
        </form>
        </div>
                <div class="header_bottom"><div class='name-holder'><% out.print(userType+" : "+userName); %></div></div>
        <div class='frame'>
  <title>receptionist</title>
</head>
<body>
<%
    float total = 0;
    float tax =0;

         out.print("<html> <style> th, td{   border-style:solid; border-color: grey; }</style>");
         out.print("<table class='table-element' style='width:100%'>");
         out.print("<tr> <th>EXPENSE NAME</th> <th>QUANTITY</th> <th>PRICE</th> </tr>");
  try
        {
    if(appointmentType.equals(AppointmentType.CONSULTATION))
    {
        total += DoctorDao.getConsultationFee(appointmentId);
        out.print("<tr> <td class='table-element' style='text-align: center'>CONSULTATION FEE</td> <td style='text-align: center'>"+"-"+"</td> <td style='text-align: center'>"+total+"</td></tr>");
    }
    else if(appointmentType.equals(AppointmentType.ADMITTANCE))
    {

        total += DoctorDao.getAdmittanceDoctorFee(appointmentId);
        out.print("<tr> <td class='table-element' style='text-align: center'>DOCTOR FEE</td> <td style='text-align: center'>"+"-"+"</td> <td style='text-align: center'>"+total+"</td></tr>");

    }

        CachedRowSet cs = InventoryDao.getUsedResources(appointmentId);
        while(rs.next())
        {
        out.print("<tr> <td class='table-element' style='text-align: center'>"+cs.getString(1)+"</td> <td style='text-align: center'>"+cs.getInt(2)+"</td> <td style='text-align: center'>"+cs.getFloat(3)+"</td></tr>");
        total+=cs.getFloat(3);
        }

        ResultSet rs = ExaminationDao.getMyExaminations(appointmentId);
        while(rs.next())
        {
        out.print("<tr> <td class='table-element' style='text-align: center'>"+rs.getString(2)+"</td> <td style='text-align: center'>"+rs.getInt(3)+"</td> <td style='text-align: center'>"+rs.getFloat(4)+"</td></tr>");
        total += rs.getFloat(4);
        }
        out.print("</table><br>");
        tax = GenericDao.getTax();
        out.print("<table class='table-element' style='width:100%'>");
        out.print("<tr> <td class='table-element' style='text-align: center'><B>TOTAL</B></td> <td style='text-align: center'>"+total+"</td></tr>");
        out.print("<tr> <td class='table-element' style='text-align: center'><B>TAX("+tax+"%)</B></td> <td style='text-align: center'>"+total*(tax*0.01)+"</td></tr>");
        out.print("<tr> <td class='table-element' style='text-align: center'><B>GRAND TOTAL</B></td> <td style='text-align: center'>"+( total + (total * ( tax*0.01 ) ) )+"</td></tr></table>");
        }
        catch(SQLException e)
        {
        System.out.println(e);
        e.printStackTrace();
        }
%>
<pre>
                    <form action='PayBill' method='post'>
                    <input type='hidden' name='billAmount' value=<% out.print(( total + (total * ( tax*0.01 ) ) )); %> >
                    <input type='hidden' name='appointmentType' value=<% out.print(appointmentType); %> >
                    <input type='hidden' name='appointmentId' value=<% out.print(appointmentId); %> >
                    <input type='submit' class="form-button form-button1" value='PAID'></form>
</pre>

</body>
</div>
</html>
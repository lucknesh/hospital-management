package com.hospital.manage.time;

import java.time.LocalDate;
import java.util.Date;

public class DateTime {
    public static String getTimeStamp()
    {
        Date date = new Date();
        String timeStamp = String.format("%d-%02d-%02d %d:%02d:%02d",date.getYear()+1900,date.getMonth()+1,date.getDate(),date.getHours(),date.getMinutes(),date.getSeconds());
        return timeStamp;
    }

    public static String getDate()
    {
        return getTimeStamp().substring(0,10);
    }

    public static String dateAfter(Integer days)
    {
        return LocalDate.now().plusDays(days).toString();
    }

    public static String yearBefore(Integer years)
    {
        return LocalDate.now().minusYears(years).toString();
    }
    public static String getDayOfWeek()
    {
        return LocalDate.now().getDayOfWeek().toString().substring(0,3);
    }
    public static String getTime()
    {
        return getTimeStamp().substring(11);
    }
}

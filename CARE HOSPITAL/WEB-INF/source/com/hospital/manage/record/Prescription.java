package com.hospital.manage.record;

import com.hospital.manage.enums.MedicineDurationUnit;
import com.hospital.manage.enums.MedicineFrequency;
import com.hospital.manage.enums.WhenToTakeMedicine;

import java.util.ArrayList;
import java.util.HashMap;

public class Prescription {
    private Integer appointmentId;
    private Integer prescriptionId;
    private Integer doctorId;
    private ArrayList<Integer> medicineIds = new ArrayList<>();
    private HashMap<Integer,Integer> duration = new HashMap<>();
    private HashMap<Integer, MedicineDurationUnit> durationUnit = new HashMap<>();
    private HashMap<Integer, MedicineFrequency> frequency = new HashMap<>();
    private HashMap<Integer, WhenToTakeMedicine> when = new HashMap<>();
    private HashMap<Integer,Integer> totalQuantity = new HashMap<>();
    private HashMap<Integer,String> notes = new HashMap<>();

    public Integer getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Integer appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Integer getPrescriptionId() {
        return prescriptionId;
    }

    public void setPrescriptionId(Integer prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public ArrayList<Integer> getMedicineIds() {
        return medicineIds;
    }

    public void setMedicineIds(ArrayList<Integer> medicineIds) {
        this.medicineIds = medicineIds;
    }

    public HashMap<Integer, Integer> getDuration() {
        return duration;
    }

    public void setDuration(HashMap<Integer, Integer> duration) {
        this.duration = duration;
    }

    public HashMap<Integer, MedicineDurationUnit> getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(HashMap<Integer, MedicineDurationUnit> durationUnit) {
        this.durationUnit = durationUnit;
    }

    public HashMap<Integer, MedicineFrequency> getFrequency() {
        return frequency;
    }

    public void setFrequency(HashMap<Integer, MedicineFrequency> frequency) {
        this.frequency = frequency;
    }

    public HashMap<Integer, WhenToTakeMedicine> getWhen() {
        return when;
    }

    public void setWhen(HashMap<Integer, WhenToTakeMedicine> when) {
        this.when = when;
    }

    public HashMap<Integer, Integer> getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(HashMap<Integer, Integer> totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public HashMap<Integer, String> getNotes() {
        return notes;
    }

    public void setNotes(HashMap<Integer, String> notes) {
        this.notes = notes;
    }
}

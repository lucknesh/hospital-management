package com.hospital.manage.record;

import com.hospital.manage.Test.Test;

import java.util.HashMap;

public class OutPatientEntry {

    private String doctorsNote;
    private String nextVisit;
    private Integer appointmentID;

    public String getDoctorsNote() {
        return doctorsNote;
    }

    public void setDoctorsNote(String doctorsNote) {
        this.doctorsNote = doctorsNote;
    }


    public Integer getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(Integer appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getNextVisit() {
        return nextVisit;
    }

    public void setNextVisit(String nextVisit) {
        this.nextVisit = nextVisit;
    }




}

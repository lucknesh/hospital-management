package com.hospital.manage.Test;

import com.hospital.manage.enums.ExaminationStatus;

import java.util.ArrayList;
import java.util.HashMap;

public class ExaminationRequest {
    private Integer appointMentId;
    private ArrayList<Integer> testIds = new ArrayList<>();
    private HashMap<Integer, ExaminationStatus> requestStatus = new HashMap<>();
    private HashMap<Integer,Integer> testIdAndExaminationId = new HashMap<>();


    public void setAppointMentId(Integer appointMentId) {
        this.appointMentId = appointMentId;
    }


    public void setTestIds(ArrayList<Integer> testIds) {
        this.testIds = testIds;
    }


    public void setRequestStatus(HashMap<Integer, ExaminationStatus> requestStatus) {
        this.requestStatus = requestStatus;
    }


    public void setTestIdAndExaminationId(HashMap<Integer, Integer> testIdAndExaminationId) {
        this.testIdAndExaminationId = testIdAndExaminationId;
    }
}

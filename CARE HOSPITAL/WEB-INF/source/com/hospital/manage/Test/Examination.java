package com.hospital.manage.Test;

import com.hospital.manage.enums.ExaminationStatus;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

public class Examination {
    private Integer examinationId;
    private Integer appointmentId;
    private Integer testId;
    private Float result;
    private Timestamp dateTime;
    private ExaminationStatus examinationStatus;

    public ExaminationStatus getExaminationStatus() {
        return examinationStatus;
    }

    public void setExaminationStatus(ExaminationStatus examinationStatus) {
        this.examinationStatus = examinationStatus;
    }

    public Integer getExaminationId() {
        return examinationId;
    }

    public void setExaminationId(Integer examinationId) {
        this.examinationId = examinationId;
    }

    public Integer getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Integer appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Integer getTestId() {
        return testId;
    }

    public void setTestId(Integer testId) {
        this.testId = testId;
    }

    public Float getResult() {
        return result;
    }

    public void setResult(Float result) {
        this.result = result;
    }

    public String getDateTime() {
        return dateTime.toString();
    }

    public void setDateTime() {

        this.dateTime = new Timestamp(System.currentTimeMillis());
    }

}

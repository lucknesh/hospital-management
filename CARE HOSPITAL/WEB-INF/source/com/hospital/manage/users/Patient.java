package com.hospital.manage.users;


import com.hospital.manage.enums.*;
import com.hospital.manage.enums.BloodGroup;
import com.hospital.manage.enums.UserType;

import java.time.LocalDate;

public class Patient extends User
{
    private Integer age;
    private LocalDate dateOfBirth;
    private BloodGroup bloodGroup;
    private Address address;
    private String fullAddress;


    public Patient(){super(UserType.PATIENT);}
    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }
    public BloodGroup getBloodGroup() {
        return bloodGroup;
    }
    public BloodGroup getBbloodGroup()
    {
        return bloodGroup;
    }
    public void setBloodGroup(BloodGroup bloodGroup)
    {
        this.bloodGroup = bloodGroup;
    }
    public Integer getAge()
    {
        return age;
    }
    public void setAge(Integer age)
    {
        this.age = age;
    }
    public String getFullAddress()
    {
        return fullAddress;
    }
    public void setFullAddress(String fullAddress)
    {
        this.fullAddress = fullAddress;
    }
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = LocalDate.parse(dateOfBirth);
    }


}

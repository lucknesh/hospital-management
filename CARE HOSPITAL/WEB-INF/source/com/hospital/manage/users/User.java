package com.hospital.manage.users;

import com.hospital.manage.enums.AccountStatus;
import com.hospital.manage.enums.UserType;

public class User
{
    private String name;
    private UserType userType;
    private Integer userId;
    private String username;
    private String mobile;
    private String email;
    private String password;
    private AccountStatus accountStatus;

    public User(UserType userType)
    {
        this.setUserType(userType);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User(String name, UserType userType)
    {
        this.userType = userType;
        this.name = name;
    }
    public User(String name,Integer userID,UserType userType)
    {
        this.name = name;
        this.userType = userType;
        this.userId = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public User(){}

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Integer getUserId()
    {
        return userId;
    }

    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }
}

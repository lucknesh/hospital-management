package com.hospital.manage.booking;

import com.hospital.manage.enums.BookingType;
import com.hospital.manage.enums.ConsultationStatus;

import java.sql.Timestamp;
import java.time.LocalDate;

public class Consultation extends Appointment{
    private Integer doctorId;
    private Integer tokenNumber;
    private LocalDate consultationDate;
    private Timestamp bookedDateTime;
    private ConsultationStatus consultationStatus = ConsultationStatus.NEW;
    private BookingType bookingType;

    public BookingType getBookingType() {
        return bookingType;
    }

    public void setBookingType(BookingType bookingType) {
        this.bookingType = bookingType;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public Integer getTokenNumber() {
        return tokenNumber;
    }

    public void setTokenNumber(Integer tokenNumber) {
        this.tokenNumber = tokenNumber;
    }

    public String getConsultationDate() {
        return consultationDate.toString();
    }

    public void setConsultationDate(String consultationDate) {
        this.consultationDate = LocalDate.parse(consultationDate);
    }

    public String getBookedDateTime() {
        return bookedDateTime.toString();
    }

    public void setBookedDateTime() {
        this.bookedDateTime = new Timestamp(System.currentTimeMillis());
    }

    public ConsultationStatus getConsultationStatus() {
        return consultationStatus;
    }

    public void setConsultationStatus(ConsultationStatus consultationStatus) {
        this.consultationStatus = consultationStatus;
    }

}

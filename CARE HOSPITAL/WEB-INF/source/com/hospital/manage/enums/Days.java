package com.hospital.manage.enums;

public enum Days {
    SUN(1),MON(2),TUE(3),WED(4),THU(5),FRI(6),SAT(7);
    private Integer dayId;
    private Days(Integer id)
    {
        this.dayId = id;
    }

    public Integer getDayId()
    {
        return this.dayId;
    }
}
//'SUN','MON','TUE','WED','THU','FRI','SAT'
//1100110 ->
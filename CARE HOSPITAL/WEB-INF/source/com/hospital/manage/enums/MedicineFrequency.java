package com.hospital.manage.enums;

public enum MedicineFrequency {
    IF_REQUIRED,ONCE_DAILY,TWICE_DAILY,THRICE_DAILY,FOUR_TIMES_A_DAY,ALTERNATIVE_DAYS,ONCE_A_WEEK,OTHER;
}

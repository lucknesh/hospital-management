package com.hospital.manage.enums;

public enum ConsultationStatus {
    NEW,COMPLETED,EXPIRED,CONSULTING,DELAYED,OUT_FOR_EXAMINATION;
}


package com.hospital.manage.enums;

public enum MedicineDurationUnit {
    DAY,WEEK,MONTH,YEAR;
}

package com.hospital.manage.enums;

public enum NurseSpecialisation {
    CRITICAL_CARE_NURSE, GENERAL_NURSE, PEDIATRIC_NURSE, CARDIAC_NURSE, ANESTHETIST_NURSE;
}

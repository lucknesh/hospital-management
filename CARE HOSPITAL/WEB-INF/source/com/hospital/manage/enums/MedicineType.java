package com.hospital.manage.enums;

public enum MedicineType {
    TABLET,SYRUP,INHALER,INJECTION,TOPICAL,DROP,OTHER;
}

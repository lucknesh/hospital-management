package com.hospital.manage.enums;

public enum BookingType {
    ONLINE(1),OFFLINE(2);
    private Integer typeId;
    private BookingType(Integer id)
    {
        this.typeId = id;
    }

    public Integer getTypeId()
    {
        return this.typeId;
    }
}

package com.hospital.manage.enums;

public enum InchargeNurseType {
    DAY_INCHARGE(1),NIGHT_INCHARGE(2);
    private Integer typeId;
    private InchargeNurseType(Integer id)
    {
        this.typeId = id;
    }

    public Integer getTypeId()
    {
        return this.typeId;
    }
}

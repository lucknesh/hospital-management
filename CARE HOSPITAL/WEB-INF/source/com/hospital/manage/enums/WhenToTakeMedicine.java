package com.hospital.manage.enums;

public enum WhenToTakeMedicine {
    AFTER_FOOD,BEFORE_FOOD,BEFORE_SLEEP,WHILE_BATHING,IF_REQUIRED,OTHER;
}

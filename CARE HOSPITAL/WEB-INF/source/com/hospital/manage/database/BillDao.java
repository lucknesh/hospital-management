package com.hospital.manage.database;

import com.adventnet.mfw.bean.BeanUtil;
import com.hospital.manage.database.bean.BillDaoImplInterface;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.record.Bill;

public class BillDao extends DbMeta{

    public static void addBill(Bill bill,boolean admittance)throws DaoException
    {
        try
        {
            ((BillDaoImplInterface) BeanUtil.lookup("BillDaoBean")).addBill(bill, admittance);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }

}


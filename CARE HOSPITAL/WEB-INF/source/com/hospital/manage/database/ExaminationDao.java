package com.hospital.manage.database;

import com.adventnet.mfw.bean.BeanUtil;
import com.hospital.manage.Test.Examination;
import com.hospital.manage.Test.Test;
import com.hospital.manage.database.bean.ExaminationDaoImpl;
import com.hospital.manage.database.bean.ExaminationDaoImplInterface;
import com.hospital.manage.exception.DaoException;

import java.sql.ResultSet;
import java.util.ArrayList;

public class ExaminationDao {

    public  static void addTest(Test test) throws DaoException
    {
        try
        {
            ((ExaminationDaoImplInterface) BeanUtil.lookup("ExaminationDaoBean")).addTest( test);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static void updateTest(Test test) throws DaoException {
        try
        {
            ((ExaminationDaoImplInterface) BeanUtil.lookup("ExaminationDaoBean")).updateTest( test);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static void removeTest(Integer testId) throws DaoException {
        try
        {
            ((ExaminationDaoImplInterface) BeanUtil.lookup("ExaminationDaoBean")).removeTest( testId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }

    public  static ResultSet getAllTest() throws DaoException {
        try
        {
            return new ExaminationDaoImpl().getAllTest();
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


//    public static boolean addExamination(Examination examination) throws DaoException {
//        try
//        {
//            return ((ExaminationDaoImplInterface) BeanUtil.lookup("ExaminationDaoBean")).addExamination( examination);
//        }
//        catch (Exception e)
//        {
//            throw new DaoException(e.getMessage());
//        }
//    }


    public  static void updateExaminationResult(ArrayList<Examination> examinations) throws DaoException {
        try
        {
             ((ExaminationDaoImplInterface) BeanUtil.lookup("ExaminationDaoBean")).updateExaminationResult( examinations);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static void addExaminationRequest(ArrayList<Integer> testIds, Integer consultationId) throws DaoException {
        try
        {
            ((ExaminationDaoImplInterface) BeanUtil.lookup("ExaminationDaoBean")).addExaminationRequest(testIds,  consultationId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static ResultSet getRequestedExaminations(Integer appointmentId) throws DaoException {
        try
        {
            return new ExaminationDaoImpl().getRequestedExaminations(appointmentId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static ResultSet getExaminationResult(Integer appointmentId) throws DaoException {
        try
        {
            return new ExaminationDaoImpl().getExaminationResult(appointmentId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static ResultSet getMyExaminations(Integer appointmentId) throws DaoException {
        try
        {
            return new ExaminationDaoImpl().getMyExaminations(appointmentId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }
}


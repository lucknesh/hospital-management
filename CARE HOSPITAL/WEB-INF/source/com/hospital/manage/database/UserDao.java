package com.hospital.manage.database;

import com.adventnet.mfw.bean.BeanUtil;
import com.hospital.manage.database.bean.UserDaoInterface;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.users.User;

//implements UserDaoBean
public class UserDao
{
    public static void insertUser(User user) throws Exception
    {
        ((UserDaoInterface) BeanUtil.lookup("UserDaoBean")).insertUser(user);
    }
    public static void invalidateAccount(Integer userId)throws DaoException
    {
        try
        {
            ((UserDaoInterface) BeanUtil.lookup("UserDaoBean")).invalidateAccount(userId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }
}






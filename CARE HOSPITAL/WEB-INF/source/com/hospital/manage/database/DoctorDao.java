package com.hospital.manage.database;

import com.adventnet.mfw.bean.BeanUtil;
import com.hospital.manage.database.bean.DoctorDaoImpl;
import com.hospital.manage.database.bean.DoctorDaoImplInterface;
import com.hospital.manage.exception.DaoException;

import java.sql.ResultSet;
import java.util.HashMap;

//
//import com.adventnet.db.api.RelationalAPI;
//import com.adventnet.ds.query.*;
//import com.adventnet.mfw.bean.BeanUtil;
//import com.adventnet.persistence.*;
//import com.hospital.com.hospital.com.hospital.manage.enums.*;
//import com.hospital.com.hospital.com.hospital.manage.exception.DaoException;
//import com.hospital.com.hospital.com.hospital.manage.time.DateTime;
//
//import java.sql.*;
//import java.time.LocalDate;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//
public class DoctorDao {


    public static ResultSet getAvailableDoctors(String day, String specialisation) throws DaoException {
        try
        {
            return new DoctorDaoImpl().getAvailableDoctors( day,  specialisation);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static ResultSet getAllAvailableDoctors(String dayOfWeek, String date, String specialisation, String bookingType) throws DaoException {
        try
        {
            return new DoctorDaoImpl().getAllAvailableDoctors( dayOfWeek,  date,  specialisation,  bookingType);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static ResultSet getDoctorAndName() throws DaoException {
        try
        {
            return new DoctorDaoImpl().getDoctorAndName();
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static void setCabinIdNull(Integer uId) throws DaoException {
        try
        {
            ((DoctorDaoImplInterface) BeanUtil.lookup("DoctorDaoBean")).setCabinIdNull( uId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static ResultSet getAvailableDays(Integer uId) throws DaoException
    {
        try
        {
            return new DoctorDaoImpl().getAvailableDays(uId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static void reAllocateCabin(HashMap<String, Integer> daysAndCabin, Integer uId) throws DaoException
    {
        try
        {
             ((DoctorDaoImplInterface) BeanUtil.lookup("DoctorDaoBean")).reAllocateCabin( daysAndCabin,  uId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static String getDoctorDuty(Integer userId) throws DaoException {
        try
        {
            return ((DoctorDaoImplInterface) BeanUtil.lookup("DoctorDaoBean")).getDoctorDuty( userId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public  static Integer getTodayAppointmentsCount(Integer docId) throws DaoException {
        try
        {
            return ((DoctorDaoImplInterface) BeanUtil.lookup("DoctorDaoBean")).getTodayAppointmentsCount( docId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public  static ResultSet getMyAppointments(String date, Integer docId) throws DaoException {
        try
        {
            return new DoctorDaoImpl().getMyAppointments( date,  docId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public  static void updateStatus(Integer consultationId, String status) throws DaoException {
            try
            {
                 ((DoctorDaoImplInterface) BeanUtil.lookup("DoctorDaoBean")).updateStatus( consultationId,  status);
            }
            catch (Exception e)
            {
                throw new DaoException(e.getMessage());
            }
    }


    public  static ResultSet getDutyDoctorDetails() throws DaoException {
        try
        {
            return new DoctorDaoImpl().getDutyDoctorDetails();
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public  static ResultSet getDutyDoctors(Integer primaryDoctorId) throws DaoException {
        try
        {
            return new DoctorDaoImpl().getDutyDoctors( primaryDoctorId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public  static ResultSet getMyInPatients(Integer doctorId) throws DaoException {
        try
        {
            return new DoctorDaoImpl().getMyInPatients( doctorId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static Boolean getMyAppointmentAvailabilityStatus(Integer doctorId) throws DaoException {
        try
        {
            return ((DoctorDaoImplInterface) BeanUtil.lookup("DoctorDaoBean")).getMyAppointmentAvailabilityStatus( doctorId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public  static Float getConsultationFee(Integer consultationId) throws DaoException {
        try
        {
            return ((DoctorDaoImplInterface) BeanUtil.lookup("DoctorDaoBean")).getConsultationFee( consultationId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static Float getAdmittanceDoctorFee(Integer appointmentId) throws DaoException {
        try
        {
            return ((DoctorDaoImplInterface) BeanUtil.lookup("DoctorDaoBean")).getAdmittanceDoctorFee(appointmentId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }
}

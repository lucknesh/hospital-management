package com.hospital.manage.database;

import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.DataAccess;
import com.hospital.manage.enums.AppointmentType;
import com.hospital.manage.enums.Days;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class NurseDao extends DbMeta{
    private static final RelationalAPI RELATIONAL_API = RelationalAPI.getInstance();

    public static String getNurseDuty(Integer nurseId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(NURSE));
            selectQuery.addSelectColumn(new Column(NURSE,DUTY));
            selectQuery.setCriteria(new Criteria(new Column(NURSE,NURSE_ID),nurseId,QueryConstants.EQUAL));
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            dataSet.next();
            return (String) dataSet.getValue(DUTY);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }
    public static Integer getCabinId(Integer nurseId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
                SelectQuery selectQuery = new SelectQueryImpl(new Table(CONSULTATION_NURSE));
                selectQuery.addSelectColumn(new Column(CONSULTATION_NURSE,CABIN_ID));
                selectQuery.setCriteria(new Criteria(new Column(CONSULTATION_NURSE,NURSE_ID),nurseId,QueryConstants.EQUAL));

                DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
                dataSet.next();
                return (Integer) dataSet.getValue(CABIN_ID);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
            }

    }

    public static ResultSet getConsultations(Integer cabinId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            String date = LocalDate.now().toString();
            Integer doctorId = findDoctorId(cabinId);
            SelectQuery selectQuery = new SelectQueryImpl(new Table(CONSULTATION));

            selectQuery.addSelectColumn(new Column(CONSULTATION,TOKEN_NUMBER));
            selectQuery.addSelectColumn(new Column(USER,NAME));
            selectQuery.addSelectColumn(new Column(CONSULTATION,CONSULTATION_STATUS));
            selectQuery.addSelectColumn(new Column(CONSULTATION,CONSULTATION_ID));

            Join join = new Join(CONSULTATION,APPOINTMENTS,new String[]{CONSULTATION_ID},new String[]{APPOINTMENT_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(APPOINTMENTS,USER,new String[]{PATIENT_ID},new String[]{USER_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);

            Criteria criteria = new Criteria(new Column(CONSULTATION,DOCTOR_ID),doctorId,QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(CONSULTATION,CONSULTATION_DATE), date,QueryConstants.EQUAL));

            selectQuery.setCriteria(criteria);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static ResultSet getConsultationTestRequests(Integer cabinId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            String dayOfWeek = LocalDate.now().getDayOfWeek().toString().substring(0, 3);
            Integer dayId = Days.valueOf(dayOfWeek).getDayId();
            String date = LocalDate.now().toString();
            String sql = "SELECT DISTINCT(APPOINTMENT_ID) FROM EXAMINATION WHERE STATUS='YET_TO_TAKE' AND APPOINTMENT_ID IN (SELECT C.CONSULTATION_ID FROM CONSULTATION C,USER U,APPOINTMENTS A WHERE CONSULTATION_DATE='" + date + "' AND A.APPOINTMENT_ID = C.CONSULTATION_ID AND A.PATIENT_ID = U.USER_ID AND C.DOCTOR_ID = (SELECT DOCTOR_ID FROM VISITINGDOCTOR WHERE CABIN_ID=" + cabinId + " AND AVAILABLE_DAY_ID='" + dayId + "') ORDER BY C.TOKEN_NUMBER) ";
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(RELATIONAL_API.executeQuery(sql,connection).getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static ResultSet getNurseAndId()throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(USER));

            selectQuery.addSelectColumn(new Column(USER,NAME));
            selectQuery.addSelectColumn(new Column(USER,USER_ID));

            Join join = new Join(USER,CONSULTATION_NURSE,new String[]{USER_ID},new String[]{NURSE_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static void setCabinIdNull(Integer uId )throws DaoException
    {
        try
        {
            UpdateQuery updateQuery = new UpdateQueryImpl(CONSULTATION_NURSE);
            updateQuery.setCriteria(new Criteria(new Column(CONSULTATION_NURSE,NURSE_ID),uId,QueryConstants.EQUAL));
            updateQuery.setUpdateColumn(CABIN_ID,null);
            DataAccess.update(updateQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public static void reAllocateCabin(Integer cabinId,Integer uId)throws DaoException
    {
        try
        {
            UpdateQuery updateQuery = new UpdateQueryImpl(CONSULTATION_NURSE);
            updateQuery.setCriteria(new Criteria(new Column(CONSULTATION_NURSE,NURSE_ID),uId,QueryConstants.EQUAL));
            updateQuery.setUpdateColumn(CABIN_ID,cabinId);
            DataAccess.update(updateQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO);
        }
    }

    public static ResultSet getDayNurses() throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(ADMITTANCE_NURSE));

            selectQuery.addSelectColumn(new Column(ADMITTANCE_NURSE,NURSE_ID));
            selectQuery.addSelectColumn(new Column(USER, NAME));
            selectQuery.addSelectColumn(new Column(NURSE,SPECIALISATION));

            Join join = new Join(ADMITTANCE_NURSE,USER,new String[]{NURSE_ID},new String[]{USER_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(ADMITTANCE_NURSE,NURSE,new String[]{NURSE_ID},new String[]{NURSE_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);

            selectQuery.setCriteria(new Criteria(new Column(ADMITTANCE_NURSE,FROM_TIME),"09:00:00",QueryConstants.LESS_EQUAL));

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }
    public static ResultSet getEveningNurses() throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(ADMITTANCE_NURSE));

            selectQuery.addSelectColumn(new Column(ADMITTANCE_NURSE,NURSE_ID));
            selectQuery.addSelectColumn(new Column(USER, NAME));
            selectQuery.addSelectColumn(new Column(NURSE,SPECIALISATION));

            Join join = new Join(ADMITTANCE_NURSE,USER,new String[]{NURSE_ID},new String[]{USER_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(ADMITTANCE_NURSE,NURSE,new String[]{NURSE_ID},new String[]{NURSE_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);


            selectQuery.setCriteria(new Criteria(new Column(ADMITTANCE_NURSE,FROM_TIME),"17:00:00",QueryConstants.GREATER_EQUAL));

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public static ResultSet getMyInPatients(Integer nurseId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQueryImpl selectQuery;
            ArrayList<Integer> admittanceIds = getAllMyAdmittanceIds(nurseId);

            selectQuery = new SelectQueryImpl(new Table(ADMITTANCE));
            Join join = new Join(ADMITTANCE,APPOINTMENTS,new String[]{ADMITTANCE_ID},new String[]{APPOINTMENT_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(APPOINTMENTS,INCHARGE_NURSES,new String[]{APPOINTMENT_ID},new String[]{ADMITTANCE_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(APPOINTMENTS,USER,new String[]{PATIENT_ID},new String[]{USER_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);

            selectQuery.addSelectColumn(new Column(ADMITTANCE,ADMITTANCE_ID));
            selectQuery.addSelectColumn(new Column(USER,NAME));
            selectQuery.addSelectColumn(new Column(APPOINTMENTS,PATIENT_ID));

            Criteria criteria = new Criteria(new Column(APPOINTMENTS,TYPE), AppointmentType.ADMITTANCE.toString(),QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(INCHARGE_NURSES,NURSE_ID), nurseId,QueryConstants.EQUAL));
            criteria = criteria.and(new Criteria(new Column(ADMITTANCE,ADMITTANCE_ID),admittanceIds.toArray(new Integer[admittanceIds.size()]),QueryConstants.IN));

            selectQuery.setCriteria(criteria);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (QueryConstructionException|SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    private static Integer findDoctorId(Integer cabinId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            String dayOfWeek = LocalDate.now().getDayOfWeek().toString().substring(0, 3);
            SelectQuery selectQuery = new SelectQueryImpl(new Table(VISITING_DOCTOR));
            selectQuery.addSelectColumn(new Column(VISITING_DOCTOR,DOCTOR_ID));

            Criteria criteria = new Criteria(new Column(VISITING_DOCTOR,CABIN_ID),cabinId,QueryConstants.EQUAL);
             criteria = criteria.and(new Criteria(new Column(VISITING_DOCTOR,AVAILABLE_DAY_ID), Days.valueOf(dayOfWeek).getDayId(),QueryConstants.EQUAL));

            selectQuery.setCriteria(criteria);
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            dataSet.next();
            return  (Integer) dataSet.getValue(DOCTOR_ID);
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    private static ArrayList<Integer> getAllAdmittanceIds()throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            DataSet dataSet;
            ArrayList<Integer> admittanceIds = new ArrayList<>();
            admittanceIds.add(0);

            SelectQuery selectQuery;
            selectQuery = new SelectQueryImpl(new Table(ADMITTANCE));
            selectQuery.addSelectColumn(new Column(ADMITTANCE,ADMITTANCE_ID));
            selectQuery.setCriteria(new Criteria(new Column(ADMITTANCE,DISCHARGE_DATE), null,QueryConstants.EQUAL));
            dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            while (dataSet.next()){
                admittanceIds.add((Integer) dataSet.getValue(ADMITTANCE_ID));
            }

            return admittanceIds;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    private static ArrayList<Integer> getAllMyAdmittanceIds(Integer nurseId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            ArrayList<Integer> admittanceIds = getAllAdmittanceIds();

            SelectQueryImpl selectQuery;

            selectQuery = new SelectQueryImpl(new Table(INCHARGE_NURSES));
            selectQuery.addSelectColumn(new Column(INCHARGE_NURSES,ADMITTANCE_ID));

            Criteria criteria = new Criteria(new Column(INCHARGE_NURSES,NURSE_ID), nurseId,QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(INCHARGE_NURSES,ADMITTANCE_ID),admittanceIds.toArray(new Integer[admittanceIds.size()]),QueryConstants.IN));

            selectQuery.setCriteria(criteria);
            admittanceIds.clear();
            admittanceIds.add(0);

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            while (dataSet.next()){
                admittanceIds.add((Integer) dataSet.getValue(ADMITTANCE_ID));
            }

            return admittanceIds;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }



}

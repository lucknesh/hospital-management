package com.hospital.manage.database;
//

import com.adventnet.mfw.bean.BeanUtil;
import com.hospital.manage.database.bean.WardDaoImpl;
import com.hospital.manage.database.bean.WardDaoImplInterface;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.ward.Cabin;
import com.hospital.manage.ward.Ward;

import java.sql.ResultSet;
import java.sql.SQLException;

//
public class WardDao
{


    public static void addWard(Ward ward) throws DaoException, SQLException {
        try
        {
            ((WardDaoImplInterface) BeanUtil.lookup("WardDaoBean")).addWard(ward);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static void addCabin(Cabin cabin) throws DaoException {
        try
        {
            ((WardDaoImplInterface) BeanUtil.lookup("WardDaoBean")).addCabin( cabin);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }

    public static void updateWard(Ward ward) throws DaoException {
        try
        {
            ((WardDaoImplInterface) BeanUtil.lookup("WardDaoBean")).updateWard( ward);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }

    public static void removeWard(Integer wardId) throws DaoException {
        try
        {
            ((WardDaoImplInterface) BeanUtil.lookup("WardDaoBean")).removeWard( wardId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public  static void removeCabin(Integer cabinNumber) throws DaoException {
        try
        {
            ((WardDaoImplInterface) BeanUtil.lookup("WardDaoBean")).removeCabin( cabinNumber);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static ResultSet getAllWards() throws DaoException {
        try
        {
            return new WardDaoImpl().getAllWards();
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static ResultSet getAvailableCabins(Integer wardId, String day) throws DaoException {
        try
        {
            return new WardDaoImpl().getAvailableCabins( wardId, day);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(e.getMessage());
        }
    }


    public static ResultSet getNoNurseCabins() throws DaoException {
        try
        {
            return new WardDaoImpl().getNoNurseCabins();
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static ResultSet getNoNurseCabins(Integer wardId) throws DaoException {
        try
        {
            return new WardDaoImpl().getNoNurseCabins( wardId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }
}

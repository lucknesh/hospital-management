package com.hospital.manage.database;

import com.adventnet.mfw.bean.BeanUtil;
import com.hospital.manage.database.bean.NoteDaoImpl;
import com.hospital.manage.database.bean.NoteDaoImplInterface;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.record.AdmittanceNote;
import com.hospital.manage.record.ConsultationReport;
import com.hospital.manage.record.Prescription;

import java.sql.ResultSet;

//
//
public class NoteDao
{

    public static void addPrescription(Prescription prescription) throws DaoException {
        try
        {
            ((NoteDaoImplInterface) BeanUtil.lookup("NoteDaoBean")).addPrescription( prescription);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public  static void addConsultationReport(ConsultationReport conReport) throws DaoException {
        try
        {
            ((NoteDaoImplInterface) BeanUtil.lookup("NoteDaoBean")).addConsultationReport( conReport);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public  static void addAdmittanceNote(AdmittanceNote note) throws DaoException {
        try
        {
            ((NoteDaoImplInterface) BeanUtil.lookup("NoteDaoBean")).addAdmittanceNote( note);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }

    public  static ResultSet getPrescription(Integer appointmentId, Integer doctorID) throws DaoException {
        try
        {
            return new NoteDaoImpl().getPrescription( appointmentId,  doctorID);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public  static ResultSet getAllPrescription(Integer appointmentId) throws DaoException {
        try
        {
            return new NoteDaoImpl().getAllPrescription( appointmentId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public ResultSet getMedicineList() throws DaoException {
        try
        {
            return new NoteDaoImpl().getMedicineList();
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static Integer getPrescriptionId(Integer appointmentId, Integer docId) throws DaoException {
        try
        {
            return ((NoteDaoImplInterface) BeanUtil.lookup("NoteDaoBean")).getPrescriptionId( appointmentId,  docId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static ResultSet getAdmittanceNurseNotes(Integer appointmentId) throws DaoException {
        try
        {
            return new NoteDaoImpl().getAdmittanceNurseNotes( appointmentId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static ResultSet getAdmittanceDoctorNotes(Integer appointmentId) throws DaoException {
        try
        {
            return new NoteDaoImpl().getAdmittanceDoctorNotes( appointmentId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }


    public static ResultSet getConsultationNote(Integer appointmentId) throws DaoException {
        try
        {
            return new NoteDaoImpl().getConsultationNote( appointmentId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }
}



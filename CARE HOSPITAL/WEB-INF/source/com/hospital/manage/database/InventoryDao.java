package com.hospital.manage.database;

import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.*;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.inventory.Item;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;
import java.sql.*;

public class InventoryDao extends DbMeta{

    private static final RelationalAPI RELATIONAL_API = RelationalAPI.getInstance();
    public static void addItem(Item item)throws DaoException
    {
        try {
            InsertQueryImpl insertQuery = new InsertQueryImpl(INVENTORY,1);
            Row row = new Row(INVENTORY);
            row.set(NAME,item.getName());
            row.set(QUANTITY,item.getQuantity());
            row.set(PRICE,item.getPrice());
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_INVENTORY);
        }

    }

    public static void updateItem(Item item)throws DaoException
    {
        try {
            UpdateQuery updateQuery = new UpdateQueryImpl(INVENTORY);

            Criteria criteria = new Criteria(new Column(INVENTORY,ITEM_ID),item.getItemId(),QueryConstants.EQUAL);
            updateQuery.setCriteria(criteria);

            updateQuery.setUpdateColumn(NAME,item.getName());
            updateQuery.setUpdateColumn(PRICE,item.getPrice());

            DataAccess.update(updateQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_INVENTORY);
        }
    }

    public static void changeQuantity(Integer itemId,Integer quantity,Integer sign)throws DaoException
    {
        try
        {
            quantity *= sign;
            RelationalAPI relationalAPI = RelationalAPI.getInstance();
            relationalAPI.execute("UPDATE INVENTORY SET QUANTITY = QUANTITY + " + quantity + " WHERE ITEM_ID=" + itemId);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_INVENTORY.toString());
        }

    }

    public static void removeItem(Integer itemId)throws DaoException
    {
        try
        {
            DeleteQuery deleteQuery = new DeleteQueryImpl(INVENTORY);
            Criteria criteria = new Criteria(new Column(INVENTORY,ITEM_ID),itemId,QueryConstants.EQUAL);
            deleteQuery.setCriteria(criteria);
            DataAccess.delete(deleteQuery);
        }
        catch (DataAccessException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_INVENTORY);
        }

    }


    public static CachedRowSet getAvailableItems()throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(INVENTORY));
            selectQuery.addSelectColumn(new Column(INVENTORY,"*"));
            selectQuery.setCriteria(new Criteria(new Column(INVENTORY,QUANTITY),0,QueryConstants.GREATER_THAN));
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());

            return cachedRowSet;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }
    public static CachedRowSet getAllItems()throws DaoException
    {

        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(INVENTORY));
            selectQuery.addSelectColumn(new Column(INVENTORY,"*"));
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());

            return cachedRowSet;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static CachedRowSet getAddedResourced(Integer appointmentId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(INVENTORY));
            selectQuery.addSelectColumn(new Column(INVENTORY,NAME));
            selectQuery.addSelectColumn(new Column(RESOURCES_USED,QUANTITY));
            Join join = new Join(INVENTORY,RESOURCES_USED,new String[]{ITEM_ID},new String[]{ITEM_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            selectQuery.setCriteria(new Criteria(new Column(RESOURCES_USED,APPOINTMENT_ID),appointmentId,QueryConstants.EQUAL));

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());

            return cachedRowSet;

        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static void addUsedResource(Integer appointmentId,Item item)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
                Integer itemId = item.getItemId();

                SelectQuery selectQuery = new SelectQueryImpl(new Table(RESOURCES_USED));
                selectQuery.addSelectColumn(new Column(RESOURCES_USED,"*"));
                selectQuery.setCriteria(new Criteria(new Column(RESOURCES_USED,APPOINTMENT_ID),appointmentId,QueryConstants.EQUAL).and(new Criteria(new Column(RESOURCES_USED,ITEM_ID),itemId,QueryConstants.EQUAL)));
                DataSet dataSet = RelationalAPI.getInstance().executeQuery(selectQuery,connection);
                ResultSet resultSet = dataSet.getResultSetAdapter().getResultSet();
                Integer quantity = item.getQuantity();
                if (resultSet.next())
                {
                    updateUsedResource(appointmentId,quantity+resultSet.getInt(QUANTITY),itemId);
                }
                else {
                    addNewUsedResource(appointmentId,itemId,quantity);
                }
                changeQuantity(itemId,quantity,-1);

            } catch (Exception e)
            {
                e.printStackTrace();
                throw new DaoException(ExceptionMessage.ERROR_UPDATING_INVENTORY);
            }

    }



    public static CachedRowSet getUsedResources(Integer appointmentId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            DataSet dataSet = RELATIONAL_API.executeQuery("SELECT I.NAME,R.QUANTITY,I.PRICE*R.QUANTITY FROM INVENTORY I,RESOURCESUSED R WHERE I.ITEM_ID = R.ITEM_ID AND R.APPOINTMENT_ID =" + appointmentId,connection);
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (QueryConstructionException|SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    private static void updateUsedResource(Integer appointmentId,Integer quantity,Integer itemId) throws DaoException
    {
        try
        {
            UpdateQuery updateQuery = new UpdateQueryImpl(RESOURCES_USED);
            updateQuery.setCriteria(new Criteria(new Column(RESOURCES_USED,APPOINTMENT_ID),appointmentId,QueryConstants.EQUAL).and(new Criteria(new Column(RESOURCES_USED,ITEM_ID),itemId,QueryConstants.EQUAL)));
            updateQuery.setUpdateColumn(QUANTITY,quantity);
            DataAccess.update(updateQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static void addNewUsedResource(Integer appointmentId,Integer itemId,Integer quantity) throws DaoException
    {
        try
        {
            InsertQueryImpl insertQuery = new InsertQueryImpl(RESOURCES_USED,1);
            Row row = new Row(RESOURCES_USED);
            row.set(APPOINTMENT_ID,appointmentId);
            row.set(ITEM_ID,itemId);
            row.set(QUANTITY,quantity);
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }


}

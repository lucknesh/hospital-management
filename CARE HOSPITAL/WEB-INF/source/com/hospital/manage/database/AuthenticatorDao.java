package com.hospital.manage.database;

import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.*;
import com.hospital.manage.enums.AccountStatus;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.enums.UserType;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.exception.NoDataFoundException;
import com.hospital.manage.users.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AuthenticatorDao extends DbMeta
{
    private static final RelationalAPI RELATIONAL_API = RelationalAPI.getInstance();
    public static String authenticate(String username,String password,Integer userId)throws DaoException
    {
        try
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(USER));

            Column column = new Column(USER,USER_TYPE);

            Criteria criteria = new Criteria(new Column(USER,EMAIL),username,QueryConstants.EQUAL);
            criteria = criteria.or(new Criteria(new Column(USER,MOBILE),username,QueryConstants.EQUAL));
            criteria = criteria.and(new Criteria(new Column(USER,PASSWORD),password,QueryConstants.EQUAL));

            selectQuery.setCriteria(criteria);
            selectQuery.addSelectColumn(column);

            DataObject dataObject = DataAccess.get(selectQuery);
            Row row = dataObject.getRow(USER);

            return (String) row.get(USER_TYPE);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_AUTHENTICATING);
        }
    }


    public static boolean isActiveAccount(Integer userId) throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(USER));

            selectQuery.addSelectColumn(new Column(USER,ACCOUNT_STATUS));
            selectQuery.setCriteria(new Criteria(new Column(USER,USER_ID),userId,QueryConstants.EQUAL));

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            dataSet.next();

            AccountStatus accountStatus = AccountStatus.valueOf((String) dataSet.getValue(ACCOUNT_STATUS));

            return accountStatus==AccountStatus.ACTIVE;
        }
        catch (SQLException e)
        {
            return false;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_AUTHENTICATING);
        }
    }

    public static boolean isValidPatient(Integer userId) throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(USER));

            selectQuery.addSelectColumn(new Column(USER,USER_TYPE));
            selectQuery.addSelectColumn(new Column(USER,ACCOUNT_STATUS));
            selectQuery.setCriteria(new Criteria(new Column(USER,USER_ID),userId,QueryConstants.EQUAL));

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            dataSet.next();

            UserType userType = UserType.valueOf((String) dataSet.getValue(USER_TYPE));
            AccountStatus accountStatus = AccountStatus.valueOf((String) dataSet.getValue(ACCOUNT_STATUS));

            return userType==UserType.PATIENT && accountStatus==AccountStatus.ACTIVE;
        }
        catch (SQLException e)
        {
            return false;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_AUTHENTICATING);
        }
    }

public static String getUserType(Integer userId)throws NoDataFoundException
{
    try(Connection connection = RELATIONAL_API.getConnection())
    {
        SelectQuery selectQuery = new SelectQueryImpl(new Table(USER));
        Column column1 = new Column(USER,USER_TYPE);
        Column column2 = new Column(USER,USER_ID);
        Criteria criteria = new Criteria(new Column(USER,USER_ID),userId,QueryConstants.EQUAL);

        selectQuery.addSelectColumn(column1);
        selectQuery.addSelectColumn(column2);
        selectQuery.setCriteria(criteria);

        DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
        dataSet.next();

        return dataSet.getAsString(USER_TYPE);
    }
    catch (QueryConstructionException|SQLException e)
    {
        e.printStackTrace();
        throw new NoDataFoundException(ExceptionMessage.ERROR_AUTHENTICATING);
    }

}

    public static User getUserInfo(String username, String password)throws Exception
    {
        User user = new User();
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(USER));

            Column col1 = new Column(USER,USER_ID);
            Column col2 = new Column(USER,USER_TYPE);
            Column col3 = new Column(USER,NAME);
            Column col4 = new Column(USER,ACCOUNT_STATUS);

            List selectColumns = new ArrayList();

            selectColumns.add(col1);
            selectColumns.add(col2);
            selectColumns.add(col3);
            selectColumns.add(col4);

            selectQuery.addSelectColumns(selectColumns);

            Criteria criteria = new Criteria(new Column(USER,EMAIL),username,QueryConstants.EQUAL);
            criteria = criteria.or(new Criteria(new Column(USER,MOBILE),username,QueryConstants.EQUAL));
            criteria = criteria.and(new Criteria(new Column(USER,PASSWORD),password,QueryConstants.EQUAL));

            selectQuery.setCriteria(criteria);

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            dataSet.next();

            user.setName((String) dataSet.getValue(NAME));
            user.setUserId((Integer) dataSet.getValue(USER_ID));
            user.setUserType(UserType.valueOf((String)dataSet.getValue(USER_TYPE)));
            user.setAccountStatus(AccountStatus.valueOf((String) dataSet.getValue(ACCOUNT_STATUS)));

            return user;
        }
        catch (QueryConstructionException|SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_AUTHENTICATING);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new Exception(e);
        }
    }



}

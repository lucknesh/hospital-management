package com.hospital.manage.database;

public class DbMeta {

    //User table
    public static final String USER = "User";

    public static final String NAME = "NAME";
    public static final String USER_ID = "USER_ID";
    public static final String USER_TYPE = "USER_TYPE";
    public static final String MOBILE = "MOBILE";
    public static final String EMAIL = "EMAIL";
    public static final String PASSWORD = "PASSWORD";
    public static final String ACCOUNT_STATUS = "ACCOUNT_STATUS";

    //Patient table

    public static final String PATIENT = "Patient";
    public static final String PATIENT_ID = "PATIENT_ID";
    public static final String BLOOD_GROUP = "BLOOD_GROUP";
    public static final String DATE_OF_BIRTH = "DOB";

    //Address table

    public static final String ADDRESS = "Address";
    public static final String DOOR_NO = "DOOR_NO";
    public static final String STREET = "STREET";
    public static final String CITY = "CITY";
    public static final String DISTRICT_ID = "DISTRICT_ID";
    public static final String STATE_ID = "STATE_ID";
    public static final String PINCODE = "PINCODE";


    //Nurse table

    public static final String NURSE = "Nurse";
    public static final String NURSE_ID = "NURSE_ID";
    public static final String SPECIALISATION = "SPECIALISATION";//Doctor table also using this .
    public static final String DUTY = "DUTY";
    public static final String STATUS = "STATUS";

    //ConsultationNurse table
    public static final String CONSULTATION_NURSE = "ConsultationNurse";

    //AdmittanceNurse table
    public static final String ADMITTANCE_NURSE = "AdmittanceNurse";

    //Doctor table

    public static final String DOCTOR = "Doctor";
    public static final String DOCTOR_ID = "DOCTOR_ID";
    public static final String IN_SERVICE_SINCE = "IN_SERVICE_SINCE";
    public static final String CONSULTATION_FEE = "CONSULTATION_FEE";
    public static final String TYPE = "TYPE";

    //DutyDoctor Table
    public static final String DUTY_DOCTOR = "DutyDoctor";
    public static final String AVAILABLE_DAY_ID = "AVAILABLE_DAY_ID";
    public static final String FROM_TIME = "FROM_TIME";
    public static final String TO_TIME = "TO_TIME";

    //VisitingDoctor

    public static final String VISITING_DOCTOR = "VisitingDoctor";
    public static final String CABIN_ID = "CABIN_ID";

    //State table

//  STATE_ID defined in Address table .
    public static final String STATE = "State";
    public static final String STATE_NAME = "STATE_NAME";
    public static final String COUNTRY = "COUNTRY";

    //District table

    //DISTRICT_ID is defined Address table

    //STATE_IS already defined in Address table

    //NAME already defined in user table
    public static final String DISTRICT = "District";

    //Ward table
    public static final String WARD = "Ward";
    public static final String WARD_ID = "WARD_ID";

    //Cabin table
    public static final String CABIN = "Cabin";
    public static final String ID = "ID";
    public static final String CABIN_NUMBER = "CABIN_NUMBER";

    //Inventory table
    public static final String ITEM_ID = "ITEM_ID";
    public static final String INVENTORY = "Inventory";
    public static final String QUANTITY = "QUANTITY";
    public static final String PRICE = "PRICE";

    //ResourcesUsed table
     public static final String RESOURCES_USED = "ResourcesUsed";
     public static final String APPOINTMENT_ID = "APPOINTMENT_ID";

     //Diagnosis table
    public static final String DIAGNOSIS = "Diagnosis";
    public static final String DIAGNOSIS_ID = "DIAGNOSIS_ID";

    //ApplicationParameter table
    public static final String APPLICATION_PARAMETER = "ApplicationParameter";
    public static final String PARAMETER_NAME = "PARAMETER_NAME";
    public static final String PARAMETER_VALUE = "PARAMETER_VALUE";

    //ConsultationCounter table

    public static final String CONSULTATION_COUNTER = "ConsultationCounter";
    public static final String DATE = "DATE";
    public static final String APPOINTMENT_TYPE = "APPOINTMENT_TYPE";
    public static final String COUNT = "COUNT";

    //Consultation table

    public static final String CONSULTATION = "Consultation";
    public static final String CONSULTATION_ID = "CONSULTATION_ID";
    public static final String TOKEN_NUMBER = "TOKEN_NUMBER";
    public static final String CONSULTATION_DATE = "CONSULTATION_DATE";
    public static final String BOOKED_DATE_TIME = "BOOKED_DATE_TIME";
    public static final String CONSULTATION_STATUS = "CONSULTATION_STATUS";

    //Appointments table
    public static final String APPOINTMENTS = "Appointments";

    //Admittance table

    public static final String ADMITTANCE = "Admittance";
    public static final String ADMITTANCE_ID = "ADMITTANCE_ID";
    public static final String PRIMARY_INCHARGE_DOCTOR = "PRIMARY_INCHARGE_DOCTOR";
    public static final String ADMISSION_DATE_TIME = "ADMISSION_DATE_TIME";
    public static final String DISCHARGE_DATE = "DISCHARGE_DATE";

    //AdmittanceDoctors table

    public static final String ADMITTANCE_DOCTORS = "AdmittanceDoctors";

    //Bill table
    public static final String BILL = "Bill";
    public static final String BILL_AMOUNT = "BILL_AMOUNT";
    public static final String BILL_STATUS = "BILL_STATUS";

    //InchargeNurses table
    public static final String INCHARGE_NURSES = "InchargeNurses";

    //GeneralAdmittance table
    public static final String GENERAL_ADMITTANCE = "GeneralAdmittance";

    //GeneralAdmittanceDiagnosis table
    public static final String GENERAL_ADMITTANCE_DIAGNOSIS = "GeneralAdmittanceDiagnosis";

    //EmergencyAdmittance table
    public static final String EMERGENCY_ADMITTANCE = "EmergencyAdmittance";

    //Test table
    public static final String TEST = "Test";
    public static final String TEST_ID = "TEST_ID";
    public static final String FEE = "FEE";

    //Examination table
    public static final String EXAMINATION = "Examination";
    public static final String EXAMINATION_ID = "EXAMINATION_ID";
    //ExaminationResult table
    public static final String EXAMINATION_RESULT = "ExaminationResult";
    public static final String RESULT = "RESULT";
    public static final String DATE_TIME = "DATE_TIME";

    //ExaminationResultScript table
    public static final String EXAMINATION_RESULT_SCRIPT = "ExaminationResultScript";
    public static final String FILE_NAME = "FILE_NAME";
    public static final String FILE_PATH = "FILE_PATH";

    //Prescription table
    public static final String PRESCRIPTION = "Prescription";
    public static final String PRESCRIPTION_ID = "PRESCRIPTION_ID";

    //PrescribedMedicine table
    public static final String PRESCRIBED_MEDICINE = "PrescribedMedicine";
    public static final String DURATION = "DURATION";
    public static final String DURATION_UNIT = "DURATION_UNIT";
    public static final String FREQUENCY = "FREQUENCY";
    public static final String WHEN = "WHEN";
    public static final String TOTAL_QUANTITY = "TOTAL_QUANTITY";

    //Medicine table
    public static final String MEDICINE = "Medicine";
    public static final String MEDICINE_ID = "MEDICINE_ID";

    //PrescriptionNote table

    public static final String PRESCRIPTION_NOTE = "PrescriptionNote";
    public static final String NOTE = "NOTE";

    //ConsultationReport table
    public static final String CONSULTATION_REPORT = "ConsultationReport";
    public static final String NEXT_VISIT = "NEXT_VISIT";
    public static final String DOCTOR_NOTE = "DOCTOR_NOTE";

    //Notes table
    public static final String NOTES = "Notes";
    public static final String NOTE_ID = "NOTE_ID";
    public static final String DOCTOR_NOTES = "DoctorNotes";
    public static final String NURSE_NOTES = "NurseNotes";

}

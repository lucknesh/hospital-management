package com.hospital.manage.database;

import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.*;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;
import java.sql.*;

public class GenericDao extends DbMeta{

    private static final RelationalAPI RELATIONAL_API = RelationalAPI.getInstance();
    public static void updateParameter(Integer id,Float value)throws DaoException
    {
        try {
            UpdateQuery updateQuery = new UpdateQueryImpl(APPLICATION_PARAMETER);
            updateQuery.setCriteria(new Criteria(new Column(APPLICATION_PARAMETER,ID),id,QueryConstants.EQUAL));
            updateQuery.setUpdateColumn(PARAMETER_VALUE,value);
            DataAccess.update(updateQuery);
            AppointmentDao.setMaximumEBookingLimit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }


    public static ResultSet getAllParameter()throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(APPLICATION_PARAMETER));
            selectQuery.addSelectColumn(new Column(APPLICATION_PARAMETER,"*"));
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }


    public static ResultSet getAllStates()throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(STATE));
            selectQuery.addSelectColumn(new Column(STATE,"*"));

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (QueryConstructionException|SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }
    public static ResultSet getAllDistrict()throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(DISTRICT));
            selectQuery.addSelectColumn(new Column(DISTRICT,"*"));

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    public static ResultSet getDiagnosisIdAndName()throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(DIAGNOSIS));
            selectQuery.addSelectColumn(new Column(DIAGNOSIS,"*"));
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;

            }
            catch (SQLException|QueryConstructionException e)
            {
                e.printStackTrace();
                throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
            }

    }

    public static Float getTax()throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(APPLICATION_PARAMETER));
            selectQuery.addSelectColumn(new Column(APPLICATION_PARAMETER,PARAMETER_VALUE));
            selectQuery.setCriteria(new Criteria(new Column(APPLICATION_PARAMETER,PARAMETER_NAME),"TAX",QueryConstants.EQUAL));

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            dataSet.next();

            return (Float) dataSet.getValue(PARAMETER_VALUE);
        }
        catch (Exception e)
        {
            e.printStackTrace();
             throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

}

package com.hospital.manage.database;

import com.adventnet.mfw.bean.BeanUtil;
import com.hospital.manage.booking.Appointment;
import com.hospital.manage.database.bean.AppointmentDaoImpl;
import com.hospital.manage.database.bean.AppointmentDaoInterface;
import com.hospital.manage.enums.AppointmentType;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.exception.NoDataFoundException;
import java.sql.ResultSet;

public class AppointmentDao
{
    public static void setMaximumEBookingLimit()throws DaoException
    {
        try
        {
            ((AppointmentDaoInterface) BeanUtil.lookup("AppointmentDaoBean")).setMaximumEBookingLimit();
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }
    public static void addAppointment(Appointment appointment) throws DaoException
    {
        try
        {
            ((AppointmentDaoInterface) BeanUtil.lookup("AppointmentDaoBean")).addAppointment(appointment);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }
    public static Integer getTokenNumber(Integer doctorId,String date,String type,Integer status) throws DaoException
    {
        try
        {
            return ((AppointmentDaoInterface) BeanUtil.lookup("AppointmentDaoBean")).getTokenNumber(doctorId, date, type, status);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }
    public static Integer isOnlineBookingAvailable(Integer doctorId,String date,String type)throws DaoException
    {
        try
        {
            return ((AppointmentDaoInterface) BeanUtil.lookup("AppointmentDaoBean")).isOnlineBookingAvailable( doctorId, date, type);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }
    public static void updateConsultationStatus(Integer cId,String status)throws DaoException
    {
        try
        {
            ((AppointmentDaoInterface) BeanUtil.lookup("AppointmentDaoBean")).updateConsultationStatus( cId, status);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }

    }
    public static ResultSet getAllMyConsultations(Integer pId)throws DaoException
    {
        try
        {
            return new AppointmentDaoImpl().getAllMyConsultations(pId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }
    public static void toggleBookingTo(Boolean status,Integer doctorId) throws DaoException
    {
        try
        {
            ((AppointmentDaoInterface) BeanUtil.lookup("AppointmentDaoBean")).toggleBookingTo( status, doctorId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }
    public static ResultSet getAllConsultations(Integer userId)throws DaoException
    {
        try
        {
            return new AppointmentDaoImpl().getAllConsultations( userId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }

    public static ResultSet getAllAdmittance(Integer userId)throws DaoException
    {
        try
        {
            return new AppointmentDaoImpl().getAllAdmittance( userId);
        }
        catch (Exception e)
        {
            throw new DaoException(e.getMessage());
        }
    }
    public static AppointmentType isValidateAppointmentType(Integer appointmentId)throws NoDataFoundException
    {
        try
        {
            return ((AppointmentDaoInterface) BeanUtil.lookup("AppointmentDaoBean")).isValidateAppointmentType( appointmentId);
        }
        catch (Exception e)
        {
            throw new NoDataFoundException(e.getMessage());
        }
    }
}

package com.hospital.manage.database.bean;

import com.hospital.manage.exception.DaoException;
import com.hospital.manage.record.AdmittanceNote;
import com.hospital.manage.record.ConsultationReport;
import com.hospital.manage.record.Prescription;

import java.sql.ResultSet;

public interface NoteDaoImplInterface {
    void addPrescription(Prescription prescription) throws DaoException;

    void addConsultationReport(ConsultationReport conReport) throws DaoException;

    void addAdmittanceNote(AdmittanceNote note) throws DaoException;

    ResultSet getPrescription(Integer appointmentId, Integer doctorID) throws DaoException;

    ResultSet getAllPrescription(Integer appointmentId) throws DaoException;

    ResultSet getMedicineList() throws DaoException;

    Integer getPrescriptionId(Integer appointmentId, Integer docId) throws DaoException;

    ResultSet getAdmittanceNurseNotes(Integer appointmentId) throws DaoException;

    ResultSet getAdmittanceDoctorNotes(Integer appointmentId) throws DaoException;

    ResultSet getConsultationNote(Integer appointmentId) throws DaoException;
}

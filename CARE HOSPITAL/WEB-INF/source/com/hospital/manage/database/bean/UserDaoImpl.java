package com.hospital.manage.database.bean;

import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.*;
import com.hospital.manage.database.DbMeta;
import com.hospital.manage.enums.*;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.users.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
public class UserDaoImpl extends DbMeta implements UserDaoInterface
{

    private static final RelationalAPI RELATIONAL_API = RelationalAPI.getInstance();
    public  void insertUser(User user) throws Exception
    {

        try(Connection connection = RELATIONAL_API.getConnection())
        {
            InsertQueryImpl insertQuery = new InsertQueryImpl(USER,1);

            Row row = new Row(USER);
            row.set(USER_TYPE,user.getUserType().toString());
            row.set(NAME,user.getName());
            row.set(MOBILE,user.getMobile());
            row.set(EMAIL,user.getEmail());
            row.set(PASSWORD,user.getPassword());

            insertQuery.addRow(row);
            DataAccess.add(insertQuery);//INSERT INTO USER TABLE

            SelectQuery selectQuery = new SelectQueryImpl(new Table(USER));

            Criteria criteria = new Criteria(new Column(USER,EMAIL),user.getEmail(),QueryConstants.EQUAL);
            criteria = criteria.or(new Criteria(new Column(USER,MOBILE),user.getMobile(),QueryConstants.EQUAL));

            selectQuery.addSelectColumn(new Column(USER,USER_ID));
            selectQuery.setCriteria(criteria);

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            dataSet.next();//WILL ALWAYS NOT NULL
            Integer userId = (Integer) dataSet.getValue(1);
            user.setUserId(userId);

            switch (user.getUserType()) {
                case PATIENT :
                    insertPatient((Patient) user);
                    break;

                case DOCTOR:
                    insertDoctor((Doctor) user);
                    break;

                case NURSE:
                    insertNurse((Nurse) user);
                    break;
            }

        }catch (DataAccessException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_USER_INFO.toString());
        }

    }

    public  void invalidateAccount(Integer userId)throws DaoException
    {
        try
        {
            UpdateQuery updateQuery = new UpdateQueryImpl(USER);
            updateQuery.setCriteria(new Criteria(new Column(USER,USER_ID),userId,QueryConstants.EQUAL));
            updateQuery.setUpdateColumn(ACCOUNT_STATUS, AccountStatus.INACTIVE.toString());
            DataAccess.update(updateQuery);
        }
        catch (DataAccessException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_USER_INFO);
        }
    }

    private static void insertPatient(Patient patient) throws DaoException
    {
        try
        {
            Integer patientId = patient.getUserId();
            InsertQueryImpl insertQuery = new InsertQueryImpl(PATIENT,1);

            Row row = new Row(PATIENT);
            row.set(PATIENT_ID,patientId);
            row.set(BLOOD_GROUP,patient.getBbloodGroup().toString());
            row.set(DATE_OF_BIRTH,patient.getDateOfBirth().toString());

            insertQuery.addRow(row);
            DataAccess.add(insertQuery);

            Address address = patient.getAddress();
            insertAddress(address,patientId);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static void insertAddress(Address address,Integer patientId) throws DaoException
    {
        try
        {
            InsertQueryImpl insertQuery = new InsertQueryImpl(ADDRESS,1);
            Row row = new Row(ADDRESS);

            row.set(PATIENT_ID,patientId);
            row.set(DOOR_NO,address.getDoorNumber());
            row.set(STREET,address.getStreet());
            row.set(CITY,address.getCity());
            row.set(DISTRICT_ID,address.getDistrictId());
            row.set(STATE_ID,address.getStateId());
            row.set(PINCODE,address.getPincode());

            insertQuery.addRow(row);
            DataAccess.add(insertQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static void insertDoctor(Doctor doctor) throws DaoException
    {
        try
        {
            DoctorType docType = doctor.getDoctorType();
            Integer doctorId = doctor.getUserId();

            InsertQueryImpl insertQuery = new InsertQueryImpl(DOCTOR,1);

            Row row = new Row(DOCTOR);
            row.set(DOCTOR_ID,doctorId);
            row.set(SPECIALISATION,doctor.getSpecialisation().toString());
            row.set(IN_SERVICE_SINCE,doctor.getDateSincePractising());
            row.set(CONSULTATION_FEE,doctor.getConsultationFee());
            row.set(TYPE,docType.toString());

            insertQuery.addRow(row);
            DataAccess.add(insertQuery);
            insertIntoDoctorAvailability(doctor);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static void insertIntoDoctorAvailability(Doctor doctor) throws DaoException
    {
        try
        {
            ArrayList<Days> availableDays = doctor.getDaysAvailable();
            DoctorType docType = doctor.getDoctorType();
            Integer doctorId = doctor.getUserId();
            String tableName = DUTY_DOCTOR;

            HashMap<Days,String> shiftStart = doctor.getShiftStartTime();
            HashMap<Days,String> shiftEnd = doctor.getShiftEndTime();
            HashMap<Days,Integer> cabinNumber = doctor.getCabinNumbers();

                for (Days day:availableDays)
                {
                    if(docType.equals(DoctorType.VISITING_DOCTOR)) tableName = VISITING_DOCTOR;
                    InsertQueryImpl insertQuery = new InsertQueryImpl(tableName,1);
                    Row row = new Row(tableName);
                    row.set(DOCTOR_ID,doctorId);
                    row.set(AVAILABLE_DAY_ID,day.getDayId());
                    row.set(FROM_TIME,shiftStart.get(day));
                    row.set(TO_TIME,shiftEnd.get(day));
                    if (tableName.equals(VISITING_DOCTOR)) row.set(CABIN_ID,cabinNumber.get(day));

                    insertQuery.addRow(row);
                    DataAccess.add(insertQuery);
                }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static void insertNurse(Nurse nurse) throws DaoException
    {
        try
        {
            NurseDuty dutyType = nurse.getNurseDuty();
            Integer nurseId = nurse.getUserId();

            InsertQueryImpl insertQuery = new InsertQueryImpl(NURSE,1);

            Row row = new Row(NURSE);
            row.set(NURSE_ID,nurseId);
            row.set(SPECIALISATION,nurse.getSpecialisation().toString());
            row.set(DUTY,dutyType.toString());
            row.set(STATUS, NurseStatus.AVAILABLE.toString());

            insertQuery.addRow(row);
            DataAccess.add(insertQuery);
            insertIntoNurseAvailability(nurse);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static void insertIntoNurseAvailability(Nurse nurse) throws DaoException
    {
        try
        {
            NurseDuty dutyType = nurse.getNurseDuty();
            String tableName = CONSULTATION_NURSE;

            if(dutyType.equals(NurseDuty.ADMITTANCE_DUTY)) tableName = ADMITTANCE_NURSE;
            InsertQueryImpl insertQuery = new InsertQueryImpl(tableName,1);
            Row row = new Row(tableName);
            row.set(NURSE_ID,nurse.getUserId());
            if (tableName.equals(CONSULTATION_NURSE)) row.set(CABIN_ID,nurse.getCabinId());
            if (tableName.equals(ADMITTANCE_NURSE)) row.set(FROM_TIME,nurse.getShiftFromTime());
            if (tableName.equals(ADMITTANCE_NURSE)) row.set(TO_TIME,nurse.getShiftToTime());
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }



}

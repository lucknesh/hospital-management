package com.hospital.manage.database.bean;

import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.*;
import com.hospital.manage.booking.*;
import com.hospital.manage.booking.*;
import com.hospital.manage.database.DbMeta;
import com.hospital.manage.enums.*;
import com.hospital.manage.database.AppointmentDao;
import com.hospital.manage.enums.*;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.exception.NoDataFoundException;
import com.hospital.manage.time.DateTime;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class AppointmentDaoImpl extends DbMeta implements AppointmentDaoInterface{
        private static int maximumLimit;
        private static final RelationalAPI RELATIONAL_API = RelationalAPI.getInstance();
        static
        {
            try
            {
                new AppointmentDaoImpl().setMaximumEBookingLimit();

            }
            catch (DaoException e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void setMaximumEBookingLimit()throws DaoException
        {
            try(Connection connection = RELATIONAL_API.getConnection())
            {
                SelectQuery selectQuery = new SelectQueryImpl(new Table(APPLICATION_PARAMETER));
                selectQuery.addSelectColumn(new Column(APPLICATION_PARAMETER,PARAMETER_VALUE));
                selectQuery.setCriteria(new Criteria(new Column(APPLICATION_PARAMETER,PARAMETER_NAME),"E_BOOKING_LIMIT",QueryConstants.EQUAL));

                DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

                if(dataSet.next()) maximumLimit = ((Float) dataSet.getValue(PARAMETER_VALUE)).intValue();
                else throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);

            }catch (Exception e)
            {
                e.printStackTrace();
                throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
            }
        }

        @Override
        public void addAppointment(Appointment appointment) throws DaoException
        {
            try(Connection connection = RELATIONAL_API.getConnection())
            {
                AppointmentType appointmentType = appointment.getAppointmentType();
                Integer patientId = appointment.getPatientId();
                RelationalAPI relationalAPI = RelationalAPI.getInstance();

                InsertQueryImpl insertQuery = new InsertQueryImpl(APPOINTMENTS,1);
                Row row = new Row(APPOINTMENTS);
                row.set(PATIENT_ID,patientId);
                row.set(TYPE,appointmentType.toString());
                insertQuery.addRow(row);
                DataAccess.add(insertQuery);

                DataSet dataSet =  relationalAPI.executeQuery("SELECT MAX(APPOINTMENT_ID) FROM APPOINTMENTS",connection);
                Integer appointmentId = 0;
                if(dataSet.next())  appointmentId = (Integer) dataSet.getValue(1);
                else throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);

                appointment.setAppointmentId(appointmentId);
                switch (appointmentType) {
                    case CONSULTATION:
                        addConsultation((Consultation) appointment);
                        break;
                    case ADMITTANCE :
                        addAdmittance((Admittance) appointment);
                        break;
                }
            }
            catch (Exception e)
            {
                System.out.printf(e.getMessage());
                e.printStackTrace();
                throw new DaoException(ExceptionMessage.ERROR_UPDATING_APPOINTMENT);
            }
        }

        @Override
        public Integer getTokenNumber(Integer doctorId, String date, String bookingType, Integer status) throws DaoException
        {
            try
            {
                switch (status) {
                    case (0) :
                        return createAndGetTokenNumber(doctorId, bookingType, date);
                    case (1) :
                        return incrementAndGetTokenNumber(doctorId, bookingType, date);
                    default:
                        return -1;
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
            }
        }

        @Override
        public Integer isOnlineBookingAvailable(Integer doctorId, String date, String bookingType)throws DaoException
        {
            try(Connection connection = RELATIONAL_API.getConnection()) {

                SelectQuery selectQuery = new SelectQueryImpl(new Table(CONSULTATION_COUNTER));
                selectQuery.addSelectColumn(new Column(CONSULTATION_COUNTER,STATUS));

                Criteria criteria = new Criteria(new Column(CONSULTATION_COUNTER,DATE),date,QueryConstants.EQUAL);
                criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,DOCTOR_ID),doctorId,QueryConstants.EQUAL));
                criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,APPOINTMENT_TYPE), BookingType.valueOf(bookingType).getTypeId(),QueryConstants.EQUAL));

                selectQuery.setCriteria(criteria);

                ResultSet resultSet = RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet();
                if (!resultSet.next()) return 0;
                if (EbookingStatus.valueOf(resultSet.getString(1)).equals(EbookingStatus.OPEN)) return 1;
                return -1;
            }catch (Exception e)
            {
                e.printStackTrace();
                throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
            }
        }


        @Override
        public  void updateConsultationStatus(Integer consultationId, String status)throws DaoException
        {
            try
            {
                UpdateQuery updateQuery = new UpdateQueryImpl(CONSULTATION);
                updateQuery.setCriteria(new Criteria(new Column(CONSULTATION,CONSULTATION_ID),consultationId,QueryConstants.EQUAL));
                updateQuery.setUpdateColumn(CONSULTATION_STATUS,status);
                DataAccess.update(updateQuery);
            }
            catch (DataAccessException e)
            {
                e.printStackTrace();
                throw new DaoException(ExceptionMessage.ERROR_UPDATING_APPOINTMENT);
            }
        }


        @Override
        public ResultSet getAllMyConsultations(Integer patientId)throws DaoException
        {
            try(Connection connection = RELATIONAL_API.getConnection())
            {
                ArrayList<Integer> appointmentIds = getAllMyAppointments(patientId);
                SelectQuery selectQuery = new SelectQueryImpl(new Table(CONSULTATION));
                selectQuery.addSelectColumn(new Column(CONSULTATION,CONSULTATION_ID));
                selectQuery.addSelectColumn(new Column(USER,NAME));
                selectQuery.addSelectColumn(new Column(CONSULTATION,CONSULTATION_DATE));

                Join join = new Join(CONSULTATION,USER,new String[]{DOCTOR_ID},new String[]{USER_ID},Join.INNER_JOIN);
                selectQuery.addJoin(join);

                Criteria criteria = new Criteria(new Column(CONSULTATION,CONSULTATION_ID),appointmentIds.toArray(new Integer[appointmentIds.size()]),QueryConstants.IN);
                criteria = criteria.and(new Criteria(new Column(CONSULTATION,CONSULTATION_STATUS), ConsultationStatus.COMPLETED.toString(),QueryConstants.EQUAL));
                selectQuery.setCriteria(criteria);

                CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
                cachedRowSet.populate(RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet());

                return cachedRowSet;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
            }
        }
        @Override
        public void toggleBookingTo(Boolean status, Integer doctorId) throws DaoException
        {
                if (!status)
                {
                    closeConsultation(doctorId);
                }
                else
                {
                    openOnlineBooking(doctorId);
                    openOfflineBooking(doctorId);
                }
        }

        @Override
        public ResultSet getAllConsultations(Integer userId)throws DaoException
        {
            try(Connection connection = RELATIONAL_API.getConnection())
            {
                SelectQuery selectQuery = new SelectQueryImpl(new Table(CONSULTATION));

                selectQuery.addSelectColumn(new Column(CONSULTATION,CONSULTATION_ID));
                selectQuery.addSelectColumn(new Column(CONSULTATION,CONSULTATION_DATE));
                selectQuery.addSelectColumn(new Column(USER,NAME));
                selectQuery.addSelectColumn(new Column(CONSULTATION,CONSULTATION_STATUS));

                Join join = new Join(CONSULTATION,APPOINTMENTS,new String[]{CONSULTATION_ID},new String[]{APPOINTMENT_ID},Join.INNER_JOIN);
                selectQuery.addJoin(join);
                join = new Join(APPOINTMENTS,USER,new String[]{PATIENT_ID},new String[]{USER_ID},Join.INNER_JOIN);
                selectQuery.addJoin(join);

                CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
                cachedRowSet.populate(RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet());

                return cachedRowSet;
            }
            catch (QueryConstructionException|SQLException e)
            {
                e.printStackTrace();
                throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
            }
        }
        @Override
        public ResultSet getAllAdmittance(Integer userId)throws DaoException
        {
            try(Connection connection = RELATIONAL_API.getConnection())
            {
                ResultSet resultSet =  RELATIONAL_API.executeQuery("SELECT AD.ADMITTANCE_ID,AD.TYPE,AD.ADMISSION_DATE_TIME,IFNULL(AD.DISCHARGE_DATE,'-NIL-') FROM ADMITTANCE AD,APPOINTMENTS WHERE AD.ADMITTANCE_ID = APPOINTMENT_ID AND PATIENT_ID = " + userId,connection).getResultSetAdapter().getResultSet();
                CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
                cachedRowSet.populate(resultSet);
                return cachedRowSet;
            }
            catch (SQLException|QueryConstructionException e)
            {
                e.printStackTrace();
                throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
            }
        }
        @Override
        public AppointmentType isValidateAppointmentType(Integer appointmentId)throws NoDataFoundException
        {
            try(Connection connection = RELATIONAL_API.getConnection()) {
                SelectQuery selectQuery = new SelectQueryImpl(new Table(APPOINTMENTS));
                selectQuery.addSelectColumn(new Column(APPOINTMENTS,TYPE));
                selectQuery.setCriteria(new Criteria(new Column(APPOINTMENTS,APPOINTMENT_ID),appointmentId,QueryConstants.EQUAL));
                ResultSet resultSet =RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet();

                if (resultSet.next()) {
                    switch (AppointmentType.valueOf(resultSet.getString(1)))
                    {
                        case CONSULTATION :
                            return isConsultation(appointmentId);
                        case ADMITTANCE :
                            return isAdmittance(appointmentId);
                    };
                }
            }
            catch (SQLException|QueryConstructionException e)
            {
                e.printStackTrace();
            }
            throw new NoDataFoundException("NOT A VALID APPOINTMENT ID");
        }

        private static void addConsultation(Consultation consultation) throws DaoException
        {
            try {
                Integer appointmentId = consultation.getAppointmentId();
                Integer doctorId = consultation.getDoctorId();
                String consultationDate = consultation.getConsultationDate();
                String bookedDatetime = consultation.getBookedDateTime();

                Integer onlineBookingStatus = AppointmentDao.isOnlineBookingAvailable(doctorId, LocalDate.now().toString(), consultation.getBookingType().toString());
                Integer tokenNumber = AppointmentDao.getTokenNumber(doctorId, LocalDate.now().toString(), consultation.getBookingType().toString(), onlineBookingStatus);

                InsertQueryImpl insertQuery = new InsertQueryImpl(CONSULTATION, 1);

                Row row = new Row(CONSULTATION);
                row.set(CONSULTATION_ID, appointmentId);
                row.set(DOCTOR_ID, doctorId);
                row.set(TOKEN_NUMBER, tokenNumber);
                row.set(CONSULTATION_DATE, consultationDate);
                row.set(BOOKED_DATE_TIME, bookedDatetime);
                row.set(CONSULTATION_STATUS, ConsultationStatus.NEW.toString());

                insertQuery.addRow(row);
                DataAccess.add(insertQuery);
            }catch (Exception e)
            {
                System.out.printf(e.getMessage());
                e.printStackTrace();
                throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
            }
        }

        private static void addAdmittance(Admittance admittance) throws DaoException
        {
            try {
                ArrayList<Integer> doctorIds = admittance.getSecondaryInchargeDoctorIds();
                insertAdmittance(admittance);
                insertInchargeNurse(admittance);

                for (Integer doctorId : doctorIds) {
                    InsertQueryImpl insertQuery = new InsertQueryImpl(ADMITTANCE_DOCTORS, 1);
                    Row row = new Row(ADMITTANCE_DOCTORS);
                    row.set(ADMITTANCE_ID, admittance.getAppointmentId());
                    row.set(DOCTOR_ID, doctorId);
                    insertQuery.addRow(row);
                    DataAccess.add(insertQuery);
                }
                switch (admittance.getAdmittanceType()) {
                    case GENERAL :
                        addGeneralAdmittance((GeneralAdmittance) admittance);
                        break;
                    case EMERGENCY :
                        addEmergencyAdmittance((EmergencyAdmittance) admittance);
                        break;
                }
            }catch (Exception e)
            {
                e.printStackTrace();
                throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
            }
        }

    private static void insertAdmittance(Admittance admittance) throws DaoException
    {
        try
        {
            InsertQueryImpl insertQuery = new InsertQueryImpl(ADMITTANCE, 1);
            Row row = new Row(ADMITTANCE);
            row.set(ADMITTANCE_ID, admittance.getAppointmentId());
            row.set(TYPE, admittance.getAdmittanceType().toString());
            row.set(PRIMARY_INCHARGE_DOCTOR, admittance.getPrimaryInchargeDoctorId());
            row.set(ADMISSION_DATE_TIME, admittance.getAdmissionDateTime());

            insertQuery.addRow(row);
            DataAccess.add(insertQuery);

        }catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static void insertInchargeNurse(Admittance admittance) throws DaoException
    {
        try
        {
            DataObject dataObject = new WritableDataObject();

            Row row = new Row(INCHARGE_NURSES);
            row.set(ADMITTANCE_ID, admittance.getAppointmentId());
            row.set(TYPE, InchargeNurseType.DAY_INCHARGE.getTypeId());
            row.set(NURSE_ID, admittance.getDayInchargeNurseId());
            dataObject.addRow(row);

            row = new Row(INCHARGE_NURSES);
            row.set(ADMITTANCE_ID, admittance.getAppointmentId());
            row.set(TYPE, InchargeNurseType.NIGHT_INCHARGE.getTypeId());
            row.set(NURSE_ID, admittance.getNightInchargeNurseId());
            dataObject.addRow(row);

            DataAccess.add(dataObject);

        }catch (DataAccessException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static void addGeneralAdmittance(GeneralAdmittance generalAdmittance) throws DaoException
    {
        try
        {
            Integer appointmentId = generalAdmittance.getAppointmentId();
            Integer consultationId = generalAdmittance.getConsultationId();
            ArrayList<Integer> diagnosisIds = generalAdmittance.getDiagnosisIds();

            DataObject dataObject = new WritableDataObject();

            Row row = new Row(GENERAL_ADMITTANCE);
            row.set(ADMITTANCE_ID,appointmentId);
            row.set(CONSULTATION_ID,consultationId);

            dataObject.addRow(row);

            for (Integer diagnosisId : diagnosisIds) {
                row = new Row(GENERAL_ADMITTANCE_DIAGNOSIS);
                row.set(ADMITTANCE_ID,appointmentId);
                row.set(DIAGNOSIS_ID,diagnosisId);
                dataObject.addRow(row);
            }

            DataAccess.add(dataObject);
        }
        catch (DataAccessException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static void addEmergencyAdmittance(EmergencyAdmittance emergencyAdmittance) throws DaoException
    {
        try
        {
            Integer appointmentId = emergencyAdmittance.getAppointmentId();
            Integer diagnosisId = emergencyAdmittance.getDiagnosisId();
            InsertQueryImpl insertQuery = new InsertQueryImpl(EMERGENCY_ADMITTANCE,1);
            Row row = new Row(EMERGENCY_ADMITTANCE);
            row.set(ADMITTANCE_ID,appointmentId);
            row.set(DIAGNOSIS_ID,diagnosisId);
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);

        }catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }


    private static AppointmentType isConsultation(Integer appointmentId) throws NoDataFoundException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(BILL));
            selectQuery.addSelectColumn(new Column(BILL,APPOINTMENT_ID));
            selectQuery.setCriteria(new Criteria(new Column(BILL,APPOINTMENT_ID),appointmentId,QueryConstants.EQUAL).and(new Criteria(new Column(BILL,BILL_STATUS), BillStatus.PAID.toString(),QueryConstants.EQUAL)));

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            ArrayList<Integer> appointmentIds = new ArrayList<>();
            appointmentIds.add(0);
            while (dataSet.next())
            {
                appointmentIds.add((Integer) dataSet.getValue(APPOINTMENT_ID));
            }

            selectQuery = new SelectQueryImpl(new Table(CONSULTATION));
            selectQuery.addSelectColumn(new Column(CONSULTATION,DOCTOR_ID));

            Criteria criteria = new Criteria(new Column(CONSULTATION,CONSULTATION_STATUS),ConsultationStatus.DELAYED.toString(),QueryConstants.NOT_EQUAL);
            criteria = criteria.and(new Criteria(new Column(CONSULTATION,CONSULTATION_STATUS),ConsultationStatus.EXPIRED.toString(),QueryConstants.NOT_EQUAL));
            criteria = criteria.and(new Criteria(new Column(CONSULTATION,CONSULTATION_ID),appointmentId,QueryConstants.EQUAL));
            criteria = criteria.and(new Criteria(new Column(CONSULTATION,CONSULTATION_ID),appointmentIds.toArray(new Integer[appointmentIds.size()]),QueryConstants.NOT_IN));

            selectQuery.setCriteria(criteria);

            ResultSet resultSet = RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet();
            if (resultSet.next()) return AppointmentType.CONSULTATION;
            else throw new NoDataFoundException("NOT A VALID APPOINTMENT ID");
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
        }
        throw new NoDataFoundException(ExceptionMessage.ERROR_FETCHING_DATA);
    }

    private static AppointmentType isAdmittance(Integer appointmentId) throws NoDataFoundException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(ADMITTANCE));
            selectQuery.addSelectColumn(new Column(ADMITTANCE,ADMITTANCE_ID));
            selectQuery.setCriteria(new Criteria(new Column(ADMITTANCE,DISCHARGE_DATE),null,QueryConstants.EQUAL).and(new Criteria(new Column(ADMITTANCE,ADMITTANCE_ID),appointmentId,QueryConstants.EQUAL)));

            ResultSet resultSet = RelationalAPI.getInstance().executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet();
            if (resultSet.next()) return AppointmentType.ADMITTANCE;
            else throw new NoDataFoundException("NOT A VALID APPOINTMENT ID");
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
        }
        throw new NoDataFoundException(ExceptionMessage.ERROR_FETCHING_DATA);
    }

    private static Integer createAndGetTokenNumber(Integer doctorId,String bookingType,String date) throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            InsertQueryImpl insertQuery = new InsertQueryImpl(CONSULTATION_COUNTER, 1);
            Row row = new Row(CONSULTATION_COUNTER);
            row.set(DOCTOR_ID, doctorId);
            row.set(DATE, date);
            row.set(APPOINTMENT_TYPE, BookingType.valueOf(bookingType).getTypeId());
            row.set(COUNT, 1);
            row.set(STATUS, EbookingStatus.OPEN.toString());
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);

            DataSet dataSet = RELATIONAL_API.executeQuery("SELECT SUM(COUNT) FROM  CONSULTATIONCOUNTER WHERE DATE = '" + date + "' AND DOCTOR_ID=" + doctorId + " ",connection);
            if(dataSet.next()) return dataSet.getInt(1);
            else throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    private static Integer incrementAndGetTokenNumber(Integer doctorId,String bookingType,String date) throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            Integer count = 0;

            SelectQuery selectQuery = new SelectQueryImpl(new Table(CONSULTATION_COUNTER));
            selectQuery.addSelectColumn(new Column(CONSULTATION_COUNTER,COUNT));
            Criteria criteria = new Criteria(new Column(CONSULTATION_COUNTER,DATE),date,QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,DOCTOR_ID),doctorId,QueryConstants.EQUAL));
            criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,APPOINTMENT_TYPE),BookingType.valueOf(bookingType).getTypeId(),QueryConstants.EQUAL));

            selectQuery.setCriteria(criteria);
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            if(dataSet.next()) count = (Integer) dataSet.getValue(COUNT);
            else throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);

            UpdateQuery updateQuery = new UpdateQueryImpl(CONSULTATION_COUNTER);
            updateQuery.setCriteria(criteria);
            updateQuery.setUpdateColumn(COUNT,count+1);
            DataAccess.update(updateQuery);

            updateEBookingStatus(date,doctorId);
            ResultSet resultSet = RELATIONAL_API.executeQuery("SELECT SUM(COUNT) FROM CONSULTATIONCOUNTER WHERE DOCTOR_ID=" + doctorId + " AND DATE='" + date + "' ",connection).getResultSetAdapter().getResultSet();
            if(resultSet.next()) return resultSet.getInt(1);
            else throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);

        }catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    private static void updateEBookingStatus(String date,Integer doctorId) throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            int onlineBookingCount = 0;

            SelectQuery selectQuery = new SelectQueryImpl(new Table(CONSULTATION_COUNTER));
            selectQuery.addSelectColumn(new Column(CONSULTATION_COUNTER,COUNT));

            Criteria criteria = new Criteria(new Column(CONSULTATION_COUNTER,DATE),date,QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,DOCTOR_ID),doctorId,QueryConstants.EQUAL));
            criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,APPOINTMENT_TYPE),BookingType.ONLINE.getTypeId(),QueryConstants.EQUAL));
            selectQuery.setCriteria(criteria);

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            if(dataSet.next()) onlineBookingCount = dataSet.getResultSetAdapter().getResultSet().getInt(1);

            if (onlineBookingCount ==  maximumLimit)
            {
                UpdateQuery updateQuery = new UpdateQueryImpl(CONSULTATION_COUNTER);
                updateQuery.setCriteria(criteria);
                updateQuery.setUpdateColumn(STATUS,EbookingStatus.CLOSED.toString());
                DataAccess.update(updateQuery);
            }

        }catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    private static void closeConsultation(Integer doctorId) throws DaoException
    {
        try
        {

            UpdateQuery updateQuery = new UpdateQueryImpl(CONSULTATION_COUNTER);
            Criteria criteria = new Criteria(new Column(CONSULTATION_COUNTER,DATE), DateTime.getDate(),QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,DOCTOR_ID),doctorId,QueryConstants.EQUAL));
            updateQuery.setCriteria(criteria);
            updateQuery.setUpdateColumn(STATUS,EbookingStatus.CLOSED.toString());
            DataAccess.update(updateQuery);

        }catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static void openOnlineBooking(Integer doctorId) throws DaoException
    {
        try
        {
            UpdateQuery updateQuery = new UpdateQueryImpl(CONSULTATION_COUNTER);

            Criteria criteria = new Criteria(new Column(CONSULTATION_COUNTER,COUNT),maximumLimit,QueryConstants.LESS_THAN)
                    .and(new Criteria(new Column(CONSULTATION_COUNTER,DATE),DateTime.getDate(),QueryConstants.EQUAL))
                    .and(new Criteria(new Column(CONSULTATION_COUNTER,DOCTOR_ID),doctorId,QueryConstants.EQUAL))
                    .and(new Criteria(new Column(CONSULTATION_COUNTER,APPOINTMENT_TYPE),BookingType.ONLINE.getTypeId(),QueryConstants.EQUAL));

            updateQuery.setUpdateColumn(STATUS,EbookingStatus.OPEN.toString());
            updateQuery.setCriteria(criteria);
            DataAccess.update(updateQuery);
        }catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static void openOfflineBooking(Integer doctorId) throws DaoException
    {
        try
        {
            UpdateQuery updateQuery = new UpdateQueryImpl(CONSULTATION_COUNTER);

            Criteria criteria = new Criteria(new Column(CONSULTATION_COUNTER,DATE),DateTime.getDate(),QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,DOCTOR_ID),doctorId,QueryConstants.EQUAL));
            criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,APPOINTMENT_TYPE),BookingType.OFFLINE.getTypeId(),QueryConstants.EQUAL));
            updateQuery.setCriteria(criteria);
            updateQuery.setUpdateColumn(STATUS,EbookingStatus.OPEN.toString());
            DataAccess.update(updateQuery);
        }catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    public static ArrayList<Integer> getAllMyAppointments(Integer patientId) throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(APPOINTMENTS));
            selectQuery.addSelectColumn(new Column(APPOINTMENTS,APPOINTMENT_ID));
            selectQuery.setCriteria(new Criteria(new Column(APPOINTMENTS,PATIENT_ID),patientId,QueryConstants.EQUAL));
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            ArrayList<Integer> appointmentIds = new ArrayList<>();
            appointmentIds.add(0);
            while (dataSet.next())
            {
                appointmentIds.add((Integer) dataSet.getValue(APPOINTMENT_ID));
            }
            return appointmentIds;
        }
        catch (QueryConstructionException|SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }
}

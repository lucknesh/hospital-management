package com.hospital.manage.database.bean;

import com.hospital.manage.exception.DaoException;

import java.sql.ResultSet;
import java.util.HashMap;

public interface DoctorDaoImplInterface {
    ResultSet getAvailableDoctors(String day, String specialisation) throws DaoException;

    ResultSet getAllAvailableDoctors(String dayOfWeek, String date, String specialisation, String bookingType) throws DaoException;

    ResultSet getDoctorAndName() throws DaoException;

    void setCabinIdNull(Integer uId) throws DaoException;

    ResultSet getAvailableDays(Integer uId) throws DaoException;

    void reAllocateCabin(HashMap<String, Integer> daysAndCabin, Integer uId) throws DaoException;

    String getDoctorDuty(Integer userId) throws DaoException;

    Integer getTodayAppointmentsCount(Integer docId) throws DaoException;

    ResultSet getMyAppointments(String date, Integer docId) throws DaoException;

    void updateStatus(Integer consultationId, String status) throws DaoException;

    ResultSet getDutyDoctorDetails() throws DaoException;

    ResultSet getDutyDoctors(Integer primaryDoctorId) throws DaoException;

    ResultSet getMyInPatients(Integer doctorId) throws DaoException;

    Boolean getMyAppointmentAvailabilityStatus(Integer doctorId) throws DaoException;

    Float getConsultationFee(Integer consultationId) throws DaoException;

    Float getAdmittanceDoctorFee(Integer appointmentId) throws DaoException;
}

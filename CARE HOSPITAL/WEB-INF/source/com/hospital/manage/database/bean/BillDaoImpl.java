package com.hospital.manage.database.bean;

import com.adventnet.ds.query.*;
import com.adventnet.persistence.DataAccess;
import com.adventnet.persistence.Row;
import com.hospital.manage.database.DbMeta;
import com.hospital.manage.enums.BillStatus;
import com.hospital.manage.enums.ConsultationStatus;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.record.Bill;
import com.hospital.manage.time.DateTime;

public class BillDaoImpl extends DbMeta implements BillDaoImplInterface {
    @Override
    public void addBill(Bill bill, boolean admittance)throws DaoException
    {
        try {
            Integer appointmentId = bill.getAppointmentId();
            Float amount = bill.getBillAmount();
            String billStatus = BillStatus.PAID.toString();

            InsertQueryImpl insertQuery = new InsertQueryImpl(BILL,1);
            Row row = new Row(BILL);
            row.set(APPOINTMENT_ID,appointmentId);
            row.set(BILL_AMOUNT,amount);
            row.set(BILL_STATUS,billStatus);
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);

            if (admittance)
            {
                updateDischargeDate(appointmentId);
            }
            else
            {
                updateConsultationStatus(appointmentId);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_BILL);
        }

    }

    private static void updateDischargeDate(Integer appointmentId)throws DaoException
    {
        try
        {
            UpdateQuery updateQuery = new UpdateQueryImpl(ADMITTANCE);
            updateQuery.setCriteria(new Criteria(new Column(ADMITTANCE, ADMITTANCE_ID), appointmentId, QueryConstants.EQUAL));
            updateQuery.setUpdateColumn(DISCHARGE_DATE, DateTime.getDate());
            DataAccess.update(updateQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static void updateConsultationStatus(Integer appointmentId)throws DaoException
    {
        try
        {
            UpdateQuery updateQuery = new UpdateQueryImpl(CONSULTATION);
            updateQuery.setCriteria(new Criteria(new Column(CONSULTATION,CONSULTATION_ID),appointmentId,QueryConstants.EQUAL));
            updateQuery.setUpdateColumn(CONSULTATION_STATUS, ConsultationStatus.COMPLETED.toString());
            DataAccess.update(updateQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }
}

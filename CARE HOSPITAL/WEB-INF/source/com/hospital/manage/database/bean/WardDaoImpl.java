package com.hospital.manage.database.bean;

import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.*;
import com.hospital.manage.database.DbMeta;
import com.hospital.manage.enums.Days;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.ward.Cabin;
import com.hospital.manage.ward.Ward;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WardDaoImpl extends DbMeta implements WardDaoImplInterface {
    private static final RelationalAPI RELATIONAL_API = RelationalAPI.getInstance();


    @Override
    public void addWard(Ward ward) throws DaoException
    {
        String name = ward.getName();
        Integer from = ward.getFromCabinNumber();
        Integer to = ward.getToCabinNumber();
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            DataObject dataObject = new WritableDataObject();
            Row row = new Row(WARD);
            row.set(NAME,name);
            dataObject.addRow(row);

            if (from > 0 && to > 0)
            {
                SelectQuery selectQuery = new SelectQueryImpl(new Table(WARD));
                selectQuery.addSelectColumn(new Column(WARD,WARD_ID));
                Criteria criteria = new Criteria(new Column(WARD,NAME),name,QueryConstants.EQUAL);
                selectQuery.setCriteria(criteria);

                DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
                dataSet.next();
                Integer wardId = (Integer) dataSet.getValue(WARD_ID);

                for (Integer i = from; i <= to; i++)
                {
                    Cabin cabin = new Cabin();
                    cabin.setWardId(wardId);
                    cabin.setCabinNumber(i);
                    dataObject.addRow(getCabinRow(cabin));
                }
            }
            DataAccess.add(dataObject);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA.toString());

        }

    }


    @Override
    public void addCabin(Cabin cabin)throws DaoException
    {
        try
        {
            InsertQueryImpl insertQuery = new InsertQueryImpl(CABIN,1);
            Row row = new Row(CABIN);
            row.set(WARD_ID,cabin.getWardId());
            row.set(CABIN_NUMBER,cabin.getCabinNumber());
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);

        }catch (DataAccessException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO.toString());
        }
    }

    @Override
    public void updateWard(Ward ward)throws DaoException
    {
        try
        {
            UpdateQuery updateQuery = new UpdateQueryImpl(WARD);
            Criteria criteria = new Criteria(new Column(WARD,WARD_ID),ward.getWardId(),QueryConstants.EQUAL);
            updateQuery.setCriteria(criteria);
            updateQuery.setUpdateColumn(NAME,ward.getName());
            DataAccess.update(updateQuery);

        }catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO);
        }
    }

    @Override
    public void removeWard(Integer wardId)throws DaoException
    {
        try {
            DeleteQuery deleteQuery = new DeleteQueryImpl(WARD);
            deleteQuery.setCriteria(new Criteria(new Column(WARD,WARD_ID),wardId,QueryConstants.EQUAL));
            DataAccess.delete(deleteQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO);
        }
    }

    @Override
    public void removeCabin(Integer cabinNumber)throws DaoException
    {
        try
        {
            DeleteQuery deleteQuery = new DeleteQueryImpl(CABIN);
            deleteQuery.setCriteria(new Criteria(new Column(CABIN,CABIN_NUMBER),cabinNumber,QueryConstants.EQUAL));
            DataAccess.delete(deleteQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO);
        }
    }

    @Override
    public ResultSet getAllWards()throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(WARD));
            selectQuery.addSelectColumn(new Column(WARD,"*"));

            DataSet dataSet = RelationalAPI.getInstance().executeQuery(selectQuery,connection);
            ResultSet resultSet = dataSet.getResultSetAdapter().getResultSet();
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(resultSet);
            return cachedRowSet;
        }
        catch (QueryConstructionException|SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }


    @Override
    public ResultSet getAvailableCabins(Integer wardId, String day)throws DaoException
    {
        Integer dayId = Days.valueOf(day).getDayId();
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            DataSet dataSet ;

            SelectQuery subQuery = new SelectQueryImpl(new Table(VISITING_DOCTOR));
            subQuery.addSelectColumn(new Column(VISITING_DOCTOR,CABIN_ID));
            subQuery.setCriteria(new Criteria(new Column(VISITING_DOCTOR,AVAILABLE_DAY_ID),dayId,QueryConstants.EQUAL));
            dataSet = RelationalAPI.getInstance().executeQuery(subQuery,connection);

            List<Integer> cabinIdsList = new ArrayList<>();

            while (dataSet.next())
            {
                cabinIdsList.add((Integer) dataSet.getValue(CABIN_ID));
            }

            Integer[] cabinIds = cabinIdsList.toArray(new Integer[cabinIdsList.size()]);

            SelectQuery query = new SelectQueryImpl(new Table(CABIN));
            query.addSelectColumn(new Column(CABIN,ID));
            query.addSelectColumn(new Column(CABIN,CABIN_NUMBER));

            Criteria criteria = new Criteria(new Column(CABIN,ID),cabinIds,QueryConstants.NOT_IN);
            criteria = criteria.and(new Criteria(new Column(CABIN,WARD_ID),wardId,QueryConstants.EQUAL));

            query.setCriteria(criteria);

            dataSet = RelationalAPI.getInstance().executeQuery(query,connection);
            ResultSet resultSet = dataSet.getResultSetAdapter().getResultSet();
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(resultSet);
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    @Override
    public ResultSet getNoNurseCabins()throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {

            SelectQuery subQuery = new SelectQueryImpl(new Table(CONSULTATION_NURSE));
            subQuery.addSelectColumn(new Column(CONSULTATION_NURSE,CABIN_ID));

            DataSet dataSet = RelationalAPI.getInstance().executeQuery(subQuery,connection);
            ArrayList<Integer> cabinIds = new ArrayList<>();

            while (dataSet.next())
            {
                cabinIds.add((Integer) dataSet.getValue(CABIN_ID));
            }
            SelectQuery selectQuery = new SelectQueryImpl(new Table(CABIN));

            selectQuery.addSelectColumn(new Column(CABIN,ID));
            selectQuery.addSelectColumn(new Column(CABIN,CABIN_NUMBER));
            selectQuery.setCriteria(new Criteria(new Column(CABIN,ID),cabinIds.toArray(new Integer[cabinIds.size()]),QueryConstants.NOT_IN));

            dataSet = RelationalAPI.getInstance().executeQuery(selectQuery,connection);
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (QueryConstructionException|SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    @Override
    public ResultSet getNoNurseCabins(Integer wardId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {

            SelectQuery subQuery = new SelectQueryImpl(new Table(CONSULTATION_NURSE));
            subQuery.addSelectColumn(new Column(CONSULTATION_NURSE,CABIN_ID));

            DataSet dataSet = RelationalAPI.getInstance().executeQuery(subQuery,connection);

            ArrayList<Integer> cabinIds = new ArrayList<>();
            while (dataSet.next()){cabinIds.add((Integer) dataSet.getValue(CABIN_ID)); }

            SelectQuery selectQuery = new SelectQueryImpl(new Table(CABIN));

            Criteria criteria = new Criteria(new Column(CABIN,ID),cabinIds.toArray(new Integer[cabinIds.size()]),QueryConstants.NOT_IN);
            criteria = criteria.and(new Criteria(new Column(CABIN,WARD_ID),wardId,QueryConstants.EQUAL));

            selectQuery.addSelectColumn(new Column(CABIN,ID));
            selectQuery.addSelectColumn(new Column(CABIN,CABIN_NUMBER));
            selectQuery.setCriteria(criteria);

            dataSet = RelationalAPI.getInstance().executeQuery(selectQuery,connection);
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (QueryConstructionException|SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    public Row getCabinRow(Cabin cabin)throws DaoException
    {
        try
        {
            Row row = new Row(CABIN);
            row.set(WARD_ID,cabin.getWardId());
            row.set(CABIN_NUMBER,cabin.getCabinNumber());
            return row;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO.toString());
        }
    }
}

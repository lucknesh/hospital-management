package com.hospital.manage.database.bean;

import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.DataAccess;
import com.adventnet.persistence.DataAccessException;
import com.adventnet.persistence.Row;
import com.hospital.manage.database.DbMeta;
import com.hospital.manage.enums.*;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.time.DateTime;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DoctorDaoImpl extends DbMeta implements DoctorDaoImplInterface {

    private static final RelationalAPI RELATIONAL_API = RelationalAPI.getInstance();


    @Override
    public ResultSet getDoctorAndName()throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            DataSet dataSet = RELATIONAL_API.executeQuery("SELECT DISTINCT(D.DOCTOR_ID),U.NAME FROM VISITINGDOCTOR D,USER U WHERE D.DOCTOR_ID = U.USER_ID",connection);
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    @Override
    public void setCabinIdNull(Integer doctorId) throws DaoException
    {
        try
        {
            UpdateQuery updateQuery = new UpdateQueryImpl(VISITING_DOCTOR);
            updateQuery.setCriteria(new Criteria(new Column(VISITING_DOCTOR,DOCTOR_ID),doctorId,QueryConstants.EQUAL));
            updateQuery.setUpdateColumn(CABIN_ID,null);
            DataAccess.update(updateQuery);
        }
        catch (DataAccessException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }

    }

    @Override
    public ResultSet getAvailableDays(Integer doctorId)throws DaoException
    {
        try (Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(VISITING_DOCTOR));
            selectQuery.addSelectColumn(new Column(VISITING_DOCTOR,AVAILABLE_DAY_ID));
            selectQuery.setCriteria(new Criteria(new Column(VISITING_DOCTOR,DOCTOR_ID),doctorId,QueryConstants.EQUAL));
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }


    @Override
    public void reAllocateCabin(HashMap<String, Integer> daysAndCabin, Integer doctorId)throws DaoException
    {

        try {
            for (Map.Entry<String, Integer> entry : daysAndCabin.entrySet()) {
                String day = entry.getKey().trim();
                Integer dayId = Days.valueOf(day).getDayId();
                Integer cabin = entry.getValue();
                System.out.println(day+" : "+dayId+" : "+cabin);
                UpdateQuery updateQuery = new UpdateQueryImpl(VISITING_DOCTOR);
                updateQuery.setCriteria(new Criteria(new Column(VISITING_DOCTOR,AVAILABLE_DAY_ID),dayId,QueryConstants.EQUAL).and(new Criteria(new Column(VISITING_DOCTOR,DOCTOR_ID),doctorId,QueryConstants.EQUAL)));
                updateQuery.setUpdateColumn(CABIN_ID,cabin);
                DataAccess.update(updateQuery);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_WARD_INFO);
        }

    }


    @Override
    public String getDoctorDuty(Integer doctorId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(DOCTOR));
            selectQuery.addSelectColumn(new Column(DOCTOR,TYPE));
            selectQuery.setCriteria(new Criteria(new Column(DOCTOR,DOCTOR_ID),doctorId,QueryConstants.EQUAL));
            ResultSet resultSet = RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet();
            if (resultSet.next()) return resultSet.getString(1);
            else throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }


    @Override
    public Integer getTodayAppointmentsCount(Integer doctorId)throws DaoException
    {

        try(Connection connection = RELATIONAL_API.getConnection()) {
            DataSet dataSet = RELATIONAL_API.executeQuery("SELECT COUNT(COUNT) FROM CONSULTATIONCOUNTER WHERE DATE='" + LocalDate.now()+ "' AND DOCTOR_ID=" + doctorId,connection);
            if (dataSet.next()) return  dataSet.getInt(1);
            else return 0;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }


    @Override
    public ResultSet getMyAppointments(String date, Integer doctorId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(USER));

            selectQuery.addSelectColumn(new Column(USER,NAME));
            selectQuery.addSelectColumn(new Column(CONSULTATION,CONSULTATION_ID));

            Criteria criteria = new Criteria(new Column(CONSULTATION,CONSULTATION_DATE),date,QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(CONSULTATION,CONSULTATION_STATUS), ConsultationStatus.CONSULTING.toString(),QueryConstants.EQUAL));
            criteria = criteria.and(new Criteria(new Column(CONSULTATION,DOCTOR_ID),doctorId,QueryConstants.EQUAL));

            selectQuery.setCriteria(criteria);

            Join join = new Join(USER,APPOINTMENTS,new String[]{USER_ID},new String[]{PATIENT_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(APPOINTMENTS,CONSULTATION,new String[]{APPOINTMENT_ID},new String[]{CONSULTATION_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }


    @Override
    public void updateStatus(Integer consultationId, String status)throws DaoException
    {
        try {
            UpdateQuery updateQuery = new UpdateQueryImpl(CONSULTATION);
            updateQuery.setCriteria(new Criteria(new Column(CONSULTATION,CONSULTATION_ID),consultationId,QueryConstants.EQUAL));
            updateQuery.setUpdateColumn(CONSULTATION_STATUS,status);
            DataAccess.update(updateQuery);
        }
        catch (DataAccessException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_APPOINTMENT);
        }
    }


    @Override
    public ResultSet getDutyDoctorDetails()throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection()) {

            ArrayList<Integer> doctorIds = getAllAvailableDoctorIds(DUTY_DOCTOR);
            SelectQuery selectQuery = new SelectQueryImpl(new Table(DOCTOR));

            selectQuery.addSelectColumn(new Column(DOCTOR,DOCTOR_ID));
            selectQuery.addSelectColumn(new Column(USER,NAME));
            selectQuery.addSelectColumn(new Column(DOCTOR,SPECIALISATION));

            Join join = new Join(DOCTOR,USER,new String[]{DOCTOR_ID},new String[]{USER_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);

            Criteria criteria = new Criteria(new Column(DOCTOR,DOCTOR_ID),doctorIds.toArray(new Integer[doctorIds.size()]),QueryConstants.IN);
            selectQuery.setCriteria(criteria);

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (QueryConstructionException|SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }


    @Override
    public ResultSet getDutyDoctors(Integer primaryDoctorId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(DUTY_DOCTOR));
            selectQuery.addSelectColumn(new Column(DUTY_DOCTOR,DOCTOR_ID));
            selectQuery.setCriteria(new Criteria(new Column(DUTY_DOCTOR,DOCTOR_ID),primaryDoctorId,QueryConstants.NOT_EQUAL));

            ArrayList<Integer> doctorIds = new ArrayList<>();
            doctorIds.add(0);
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            while (dataSet.next()){doctorIds.add((Integer) dataSet.getValue(DOCTOR_ID));}

            selectQuery = new SelectQueryImpl(new Table(DOCTOR));

            selectQuery.addSelectColumn(new Column(DOCTOR,DOCTOR_ID));
            selectQuery.addSelectColumn(new Column(USER,NAME));
            selectQuery.addSelectColumn(new Column(DOCTOR,SPECIALISATION));

            Join join = new Join(DOCTOR,USER,new String[]{DOCTOR_ID},new String[]{USER_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);

            Criteria criteria = new Criteria(new Column(USER,ACCOUNT_STATUS), AccountStatus.ACTIVE.toString(),QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(DOCTOR,DOCTOR_ID),doctorIds.toArray(new Integer[doctorIds.size()]),QueryConstants.IN));
            selectQuery.setCriteria(criteria);

            dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;

        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }


    @Override
    public ResultSet getMyInPatients(Integer doctorId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(ADMITTANCE));

            selectQuery.addSelectColumn(new Column(ADMITTANCE,ADMITTANCE_ID));
            selectQuery.addSelectColumn(new Column(USER,NAME));
            selectQuery.addSelectColumn(new Column(APPOINTMENTS,PATIENT_ID));

            Join join = new Join(ADMITTANCE,APPOINTMENTS,new String[]{ADMITTANCE_ID},new String[]{APPOINTMENT_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(ADMITTANCE,ADMITTANCE_DOCTORS,new String[]{ADMITTANCE_ID},new String[]{ADMITTANCE_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(APPOINTMENTS,USER,new String[]{PATIENT_ID},new String[]{USER_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);

            Criteria criteria = new Criteria(new Column(ADMITTANCE,PRIMARY_INCHARGE_DOCTOR),doctorId,QueryConstants.EQUAL);
            criteria = criteria.or(new Criteria(new Column(ADMITTANCE_DOCTORS,DOCTOR_ID),doctorId,QueryConstants.EQUAL));
            criteria = criteria.and(new Criteria(new Column(ADMITTANCE,DISCHARGE_DATE),null,QueryConstants.EQUAL));
            criteria = criteria.and(new Criteria(new Column(APPOINTMENTS,TYPE), AppointmentType.ADMITTANCE.toString(),QueryConstants.EQUAL));

            selectQuery.setCriteria(criteria);

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }


    @Override
    public Boolean getMyAppointmentAvailabilityStatus(Integer doctorId) throws DaoException
    {
        try
        {
            EbookingStatus onlineBookingStatus ;
            EbookingStatus offlineBookingStatus ;
            ResultSet resultSet = getOfflineBookingStatus(doctorId);

            if (resultSet.next()) offlineBookingStatus = EbookingStatus.valueOf(resultSet.getString(1));
            else
            {
                openBooking(doctorId, BookingType.OFFLINE);
                offlineBookingStatus = EbookingStatus.OPEN;
            }
            resultSet = getOnlineBookingStatus(doctorId);
            if (resultSet.next()) onlineBookingStatus = EbookingStatus.valueOf(resultSet.getString(1));
            else
            {
                openBooking(doctorId,BookingType.ONLINE);
                onlineBookingStatus = EbookingStatus.OPEN;
            }
            return !offlineBookingStatus.equals(EbookingStatus.CLOSED) || !onlineBookingStatus.equals(EbookingStatus.CLOSED);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }



    @Override
    public Float getConsultationFee(Integer consultationId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection()) {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(CONSULTATION));
            selectQuery.addSelectColumn(new Column(CONSULTATION,DOCTOR_ID));
            selectQuery.setCriteria(new Criteria(new Column(CONSULTATION,CONSULTATION_ID),consultationId,QueryConstants.EQUAL));
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            dataSet.next();
            Integer doctorId = (Integer) dataSet.getValue(DOCTOR_ID);

            selectQuery = new SelectQueryImpl(new Table(DOCTOR));
            selectQuery.addSelectColumn(new Column(DOCTOR,CONSULTATION_FEE));
            selectQuery.setCriteria(new Criteria(new Column(DOCTOR,DOCTOR_ID),doctorId,QueryConstants.EQUAL));
            dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            dataSet.next();
            return (Float) dataSet.getValue(CONSULTATION_FEE);
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    @Override
    public ResultSet getAllAvailableDoctors(String dayOfWeek, String date, String specialisation, String bookingType)throws DaoException
    {
        Integer dayId = Days.valueOf(dayOfWeek).getDayId();
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            ArrayList<Integer> doctorIds = getDoctorIdsWithSpecialization(specialisation);

            SelectQuery subQuery = new SelectQueryImpl(new Table(CONSULTATION_COUNTER));
            subQuery.addSelectColumn(new Column(CONSULTATION_COUNTER,DOCTOR_ID));

            Criteria criteria = new Criteria(new Column(CONSULTATION_COUNTER,DATE),date,QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,STATUS),EbookingStatus.CLOSED.toString(),QueryConstants.EQUAL));
            criteria = new Criteria(new Column(CONSULTATION_COUNTER,APPOINTMENT_TYPE),BookingType.valueOf(bookingType).getTypeId(),QueryConstants.EQUAL).and(criteria);

            subQuery.setCriteria(criteria);

            ArrayList<Integer> counterDoctorIds = new ArrayList<>();
            counterDoctorIds.add(0);
            try
            {
                DataSet dataSet = RELATIONAL_API.executeQuery(subQuery, connection);
                while (dataSet.next()) {
                    counterDoctorIds.add((Integer) dataSet.getValue(DOCTOR_ID));
                }
            }
            catch (SQLException|NullPointerException|QueryConstructionException se)
            {
                se.printStackTrace();
            }
            SelectQuery selectQuery = new SelectQueryImpl(new Table(VISITING_DOCTOR));
            selectQuery.addSelectColumn(new Column(VISITING_DOCTOR,DOCTOR_ID));
            selectQuery.setCriteria(new Criteria(new Column(VISITING_DOCTOR,AVAILABLE_DAY_ID),dayId,QueryConstants.EQUAL));
            selectQuery.setCriteria(new Criteria(new Column(VISITING_DOCTOR,DOCTOR_ID),doctorIds.toArray(new Integer[doctorIds.size()]),QueryConstants.IN));
            selectQuery.setCriteria(new Criteria(new Column(VISITING_DOCTOR,DOCTOR_ID),counterDoctorIds.toArray(new Integer[counterDoctorIds.size()]),QueryConstants.NOT_IN));

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());

            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    @Override
    public Float getAdmittanceDoctorFee(Integer appointmentId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection()) {
            RelationalAPI relationalAPI = RelationalAPI.getInstance();
            DataSet dataSet = relationalAPI.executeQuery("SELECT SUM(CONSULTATION_FEE) FROM DOCTOR WHERE DOCTOR_ID IN (SELECT DOCTOR_ID FROM ADMITTANCE_DOCTORS WHERE ADMITTANCE_ID=" + appointmentId + ") OR DOCTOR_ID=(SELECT PRIMARY_INCHARGE_DOCTOR FROM ADMITTANCE WHERE ADMITTANCE_ID=" + appointmentId + ")",connection);
            ResultSet resultSet = dataSet.getResultSetAdapter().getResultSet();
            resultSet.next();
            float feePerDay = resultSet.getFloat(1);
            dataSet = relationalAPI.executeQuery("SELECT TIMESTAMPDIFF(SECOND,ADMISSION_DATE_TIME,'" + DateTime.getTimeStamp() + "') FROM ADMITTANCE WHERE ADMITTANCE_ID=" + appointmentId,connection);
            resultSet = dataSet.getResultSetAdapter().getResultSet();
            resultSet.next();
            int epochSeconds =  resultSet.getInt(1);
            int days =  (((epochSeconds / 60) / 60) / 24);
            if (days == 0) return feePerDay;
            return days * feePerDay;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    @Override
    public ResultSet getAvailableDoctors(String day, String specialisation)throws DaoException
    {

        try(Connection connection = RELATIONAL_API.getConnection()) {

            ArrayList<Integer> doctorIds = getAllAvailableDoctorIds(VISITING_DOCTOR);

             SelectQuery selectQuery = new SelectQueryImpl(new Table(USER));

            selectQuery.addSelectColumn(new Column(USER,NAME));
            selectQuery.addSelectColumn(new Column(DOCTOR,DOCTOR_ID));
            selectQuery.addSelectColumn(new Column(DOCTOR,IN_SERVICE_SINCE));
            selectQuery.addSelectColumn(new Column(DOCTOR,CONSULTATION_FEE));


            Criteria criteria = new Criteria(new Column(USER,ACCOUNT_STATUS), AccountStatus.ACTIVE.toString(),QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(DOCTOR,SPECIALISATION),specialisation,QueryConstants.EQUAL));
            criteria = criteria.and(new Criteria(new Column(DOCTOR,DOCTOR_ID),doctorIds.toArray(new Integer[doctorIds.size()]),QueryConstants.IN));

            selectQuery.setCriteria(criteria);

            Join join = new Join(USER,DOCTOR,new String[]{USER_ID},new String[]{DOCTOR_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());

            return cachedRowSet;

        }
        catch (SQLException | QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    private static void openBooking(Integer doctorId,BookingType type)throws DaoException
    {
        try
        {
            InsertQueryImpl insertQuery = new InsertQueryImpl(CONSULTATION_COUNTER,1);
            Row row = new Row(CONSULTATION_COUNTER);
            row.set(DOCTOR_ID,doctorId);
            row.set(DATE,DateTime.getDate());
            row.set(APPOINTMENT_TYPE,type.getTypeId());
            row.set(COUNT,0);
            row.set(STATUS,EbookingStatus.OPEN.toString());
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);
        }
        catch (DataAccessException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }


    private static ResultSet getOnlineBookingStatus(Integer doctorId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(CONSULTATION_COUNTER));
            selectQuery.addSelectColumn(new Column(CONSULTATION_COUNTER,STATUS));
            Criteria criteria = new Criteria(new Column(CONSULTATION_COUNTER,DOCTOR_ID),doctorId,QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,APPOINTMENT_TYPE),BookingType.ONLINE.getTypeId(),QueryConstants.EQUAL));
            criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,DATE),DateTime.getDate(),QueryConstants.EQUAL));
            selectQuery.setCriteria(criteria);
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static ResultSet getOfflineBookingStatus(Integer doctorId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            DataSet dataSet ;
            SelectQuery selectQuery = new SelectQueryImpl(new Table(CONSULTATION_COUNTER));
            selectQuery.addSelectColumn(new Column(CONSULTATION_COUNTER,STATUS));
            Criteria criteria = new Criteria(new Column(CONSULTATION_COUNTER,DOCTOR_ID),doctorId,QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,APPOINTMENT_TYPE),BookingType.OFFLINE.getTypeId(),QueryConstants.EQUAL));
            criteria = criteria.and(new Criteria(new Column(CONSULTATION_COUNTER,DATE),DateTime.getDate(),QueryConstants.EQUAL));
            selectQuery.setCriteria(criteria);

            dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }



    private static ArrayList<Integer> getAllAvailableDoctorIds(String tableName) throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            String dayOfWeek = DateTime.getDayOfWeek();
            Integer dayId = Days.valueOf(dayOfWeek).getDayId();
            String time = DateTime.getTime();

            SelectQuery selectQuery = new SelectQueryImpl(new Table(tableName));
            selectQuery.addSelectColumn(new Column(tableName,DOCTOR_ID));

            Criteria criteria = new Criteria(new Column(tableName,TO_TIME),time,QueryConstants.GREATER_EQUAL);
            criteria = criteria.and(new Criteria(new Column(tableName,FROM_TIME),time,QueryConstants.LESS_EQUAL));
            criteria = criteria.and(new Criteria(new Column(tableName,AVAILABLE_DAY_ID),dayId,QueryConstants.EQUAL));

            selectQuery.setCriteria(criteria);
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery,connection);
            ArrayList<Integer> doctorIds = new ArrayList<>();
            doctorIds.add(0);
            while (dataSet.next())
            {
                doctorIds.add((Integer) dataSet.getValue(DOCTOR_ID));
            }
            return doctorIds;
        }
        catch (QueryConstructionException|SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    private static ArrayList<Integer> getDoctorIdsWithSpecialization(String specialisation) throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery subQuery = new SelectQueryImpl(new Table(DOCTOR));
            subQuery.addSelectColumn(new Column(DOCTOR,DOCTOR_ID));
            subQuery.setCriteria(new Criteria(new Column(DOCTOR,SPECIALISATION),specialisation,QueryConstants.EQUAL));

            ArrayList<Integer> doctorIds = new ArrayList<>();
            doctorIds.add(0);
            DataSet dataSet = RELATIONAL_API.executeQuery(subQuery,connection);
            while (dataSet.next())
            {
                doctorIds.add((Integer) dataSet.getValue(DOCTOR_ID));
            }

            return doctorIds;
        }
        catch (QueryConstructionException|SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }
}

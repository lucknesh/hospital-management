package com.hospital.manage.database.bean;

import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.DataAccess;
import com.adventnet.persistence.Row;
import com.hospital.manage.database.DbMeta;
import com.hospital.manage.enums.*;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.record.AdmittanceNote;
import com.hospital.manage.record.ConsultationReport;
import com.hospital.manage.record.Prescription;
import com.hospital.manage.time.DateTime;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class NoteDaoImpl extends DbMeta implements NoteDaoImplInterface {
    private static final RelationalAPI RELATIONAL_API = RelationalAPI.getInstance();

    @Override
    public void addConsultationReport(ConsultationReport conReport)throws DaoException
    {
            String nextVisitDate = conReport.getNextVisitDate().trim();
            insertConsultationReport(conReport, nextVisitDate.length() > 0);
    }

    @Override
    public void addAdmittanceNote(AdmittanceNote note) throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {

            AdmittanceNoteType noteType = note.getAdmittanceNoteType();
            Integer writerId = note.getWriterId();

            InsertQueryImpl insertQuery = new InsertQueryImpl(NOTES,1);
            Row row = new Row(NOTES);
            row.set(ADMITTANCE_ID,note.getAdmittanceId());
            row.set(DATE_TIME,DateTime.getTimeStamp());
            row.set(NOTE,note.getNote());
            row.set(TYPE,note.getAdmittanceNoteType().toString());
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);

            DataSet dataSet =  RELATIONAL_API.executeQuery("SELECT MAX(NOTE_ID) FROM NOTES",connection);
            dataSet.next();
            Integer noteId = (Integer) dataSet.getValue(1);

            switch (noteType) {
                case DOCTOR_NOTE:
                    insertIntoDoctorNotes(noteId,writerId);
                    break;
                case NURSE_NOTE:
                    insertIntoNurseNotes(noteId,writerId);
                    break;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_RECORD);
        }
    }

    @Override
    public ResultSet getPrescription(Integer appointmentId, Integer doctorID)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(MEDICINE));
            selectQuery.addSelectColumn(new Column(MEDICINE,NAME));
            selectQuery.addSelectColumn(new Column(PRESCRIBED_MEDICINE,DURATION));
            selectQuery.addSelectColumn(new Column(PRESCRIBED_MEDICINE,DURATION_UNIT));
            selectQuery.addSelectColumn(new Column(PRESCRIBED_MEDICINE,FREQUENCY));
            selectQuery.addSelectColumn(new Column(PRESCRIBED_MEDICINE,WHEN));
            selectQuery.addSelectColumn(new Column(PRESCRIBED_MEDICINE,TOTAL_QUANTITY));

            Join join = new Join(MEDICINE,PRESCRIBED_MEDICINE,new String[]{MEDICINE_ID},new String[]{MEDICINE_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(PRESCRIBED_MEDICINE,PRESCRIPTION,new String[]{PRESCRIPTION_ID},new String[]{PRESCRIPTION_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);

            Criteria criteria = new Criteria(new Column(PRESCRIPTION,APPOINTMENT_ID),appointmentId,QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(PRESCRIPTION,DOCTOR_ID),doctorID,QueryConstants.EQUAL));

            selectQuery.setCriteria(criteria);

            ResultSet resultSet =  RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet();

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(resultSet);
            return cachedRowSet;
        }
        catch (QueryConstructionException | SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }
    @Override
    public ResultSet getAllPrescription(Integer appointmentId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(USER));

            selectQuery.addSelectColumn(new Column(USER,NAME));
            selectQuery.addSelectColumn(new Column(MEDICINE,NAME));
            selectQuery.addSelectColumn(new Column(PRESCRIBED_MEDICINE,DURATION));
            selectQuery.addSelectColumn(new Column(PRESCRIBED_MEDICINE,DURATION_UNIT));
            selectQuery.addSelectColumn(new Column(PRESCRIBED_MEDICINE,FREQUENCY));
            selectQuery.addSelectColumn(new Column(PRESCRIBED_MEDICINE,WHEN));
            selectQuery.addSelectColumn(new Column(PRESCRIBED_MEDICINE,TOTAL_QUANTITY));

            Join join = new Join(USER,PRESCRIPTION,new String[]{USER_ID},new String[]{DOCTOR_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(PRESCRIPTION,PRESCRIBED_MEDICINE,new String[]{PRESCRIPTION_ID},new String[]{PRESCRIPTION_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(PRESCRIBED_MEDICINE,MEDICINE,new String[]{MEDICINE_ID},new String[]{MEDICINE_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);

            selectQuery.setCriteria(new Criteria(new Column(PRESCRIPTION,APPOINTMENT_ID),appointmentId,QueryConstants.EQUAL));

            ResultSet resultSet =  RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet();

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(resultSet);
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    @Override
    public ResultSet getMedicineList()throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(MEDICINE));
            selectQuery.addSelectColumn(new Column(MEDICINE,NAME));
            selectQuery.addSelectColumn(new Column(MEDICINE,MEDICINE_ID));

            ResultSet resultSet =  RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet();

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(resultSet);
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    @Override
    public Integer getPrescriptionId(Integer appointmentId, Integer docId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(PRESCRIPTION));
            selectQuery.addSelectColumn(new Column(PRESCRIPTION,PRESCRIPTION_ID));

            Criteria criteria = new Criteria(new Column(PRESCRIPTION,DOCTOR_ID),docId,QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(PRESCRIPTION,APPOINTMENT_ID),appointmentId,QueryConstants.EQUAL));

            selectQuery.setCriteria(criteria);

            ResultSet resultSet = RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet();

            resultSet.next();
            return resultSet.getInt(1);
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
        }
        throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
    }

    @Override
    public ResultSet getAdmittanceNurseNotes(Integer appointmentId) throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(NOTES));

            selectQuery.addSelectColumn(new Column(NOTES,NOTE));
            selectQuery.addSelectColumn(new Column(NOTES,TYPE));
            selectQuery.addSelectColumn(new Column(NOTES,DATE_TIME));
            selectQuery.addSelectColumn(new Column(USER,NAME));

            Join join = new Join(NOTES,NURSE_NOTES,new String[]{NOTE_ID},new String[]{NOTE_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(NURSE_NOTES,USER,new String[]{NURSE_ID},new String[]{USER_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);

            Criteria criteria = new Criteria(new Column(NOTES,ADMITTANCE_ID),appointmentId,QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(NOTES,TYPE), NoteType.NURSE_NOTE.toString(),QueryConstants.EQUAL));

            selectQuery.setCriteria(criteria);

            ResultSet resultSet =  RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet();

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(resultSet);
            return cachedRowSet;
        }
        catch (SQLException|QueryConstructionException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    @Override
    public ResultSet getAdmittanceDoctorNotes(Integer appointmentId) throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(NOTES));

            selectQuery.addSelectColumn(new Column(NOTES,NOTE));
            selectQuery.addSelectColumn(new Column(NOTES,TYPE));
            selectQuery.addSelectColumn(new Column(NOTES,DATE_TIME));
            selectQuery.addSelectColumn(new Column(USER,NAME));

            Join join = new Join(NOTES,DOCTOR_NOTES,new String[]{NOTE_ID},new String[]{NOTE_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(DOCTOR_NOTES,USER,new String[]{DOCTOR_ID},new String[]{USER_ID},Join.INNER_JOIN);
            selectQuery.addJoin(join);

            Criteria criteria = new Criteria(new Column(NOTES,ADMITTANCE_ID),appointmentId,QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(NOTES,TYPE),NoteType.DOCTOR_NOTE.toString(),QueryConstants.EQUAL));

            selectQuery.setCriteria(criteria);

            ResultSet resultSet =  RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet();

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(resultSet);
            return cachedRowSet;
        }
        catch (QueryConstructionException|SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    @Override
    public ResultSet getConsultationNote(Integer appointmentId)throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(CONSULTATION_REPORT));

            selectQuery.addSelectColumn(new Column(CONSULTATION_REPORT,NEXT_VISIT));
            selectQuery.addSelectColumn(new Column(CONSULTATION_REPORT,DOCTOR_NOTE));

            selectQuery.setCriteria(new Criteria(new Column(CONSULTATION_REPORT,CONSULTATION_ID),appointmentId,QueryConstants.EQUAL));

            ResultSet resultSet =  RELATIONAL_API.executeQuery(selectQuery,connection).getResultSetAdapter().getResultSet();

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(resultSet);
            return cachedRowSet;
        }
        catch (QueryConstructionException|SQLException e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    @Override
    public void addPrescription(Prescription prescription)throws DaoException
    {
        try
        {
            ArrayList<Integer> medicineIds = prescription.getMedicineIds();
            HashMap<Integer, Integer> duration = prescription.getDuration();
            HashMap<Integer, MedicineDurationUnit> durationUnit = prescription.getDurationUnit();
            HashMap<Integer, MedicineFrequency> frequency = prescription.getFrequency();
            HashMap<Integer, WhenToTakeMedicine> when = prescription.getWhen();
            HashMap<Integer, Integer> totalQuantity = prescription.getTotalQuantity();
            HashMap<Integer, String> notes = prescription.getNotes();
            Integer prescriptionId = prescription.getPrescriptionId();

            if (prescriptionId <= 0)
            {
                prescriptionId = insertIntoPrescription(prescription);
            }

            for (Integer drugId : medicineIds) {
                InsertQueryImpl insertQuery = new InsertQueryImpl(PRESCRIBED_MEDICINE,1);
                Row row = new Row(PRESCRIBED_MEDICINE);
                row.set(PRESCRIPTION_ID,prescriptionId);
                row.set(MEDICINE_ID,drugId);
                row.set(DURATION,duration.get(drugId));
                row.set(DURATION_UNIT,durationUnit.get(drugId).toString());
                row.set(FREQUENCY,frequency.get(drugId).toString());
                row.set(WHEN,when.get(drugId).toString());
                row.set(TOTAL_QUANTITY,totalQuantity.get(drugId));
                insertQuery.addRow(row);
                DataAccess.add(insertQuery);

                if (notes.get(drugId) != null && notes.get(drugId).trim().length() != 0)
                {
                    insertIntoPrescriptionNotes(prescriptionId,drugId,notes.get(drugId));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_RECORD);
        }
    }

    private static Integer insertIntoPrescription(Prescription prescription) throws DaoException
    {
        try(Connection connection = RELATIONAL_API.getConnection())
        {
            InsertQueryImpl insertQuery = new InsertQueryImpl(PRESCRIPTION,1);
            Row row = new Row(PRESCRIPTION);
            row.set(APPOINTMENT_ID,prescription.getAppointmentId());
            row.set(DOCTOR_ID,prescription.getDoctorId());
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);

            DataSet dataSet = RELATIONAL_API.executeQuery("SELECT MAX(PRESCRIPTION_ID) FROM PRESCRIPTION",connection);
            return (Integer) dataSet.getValue(1);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_RECORD);
        }
    }


    private static void insertConsultationReport(ConsultationReport consultationReport,Boolean nextVisit) throws DaoException
    {
        try
        {
            InsertQueryImpl insertQuery = new InsertQueryImpl(CONSULTATION_REPORT,1);
            Row row = new Row(CONSULTATION_REPORT);
            row.set(CONSULTATION_ID,consultationReport.getConsultationId());
            row.set(DIAGNOSIS_ID,consultationReport.getDiagnosisId());
            if (nextVisit) row.set(NEXT_VISIT, consultationReport.getNextVisitDate());
            row.set(DOCTOR_NOTE,consultationReport.getDoctorNote());
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_RECORD);
        }
    }

    private static void insertIntoDoctorNotes(Integer noteId,Integer doctorId) throws DaoException
    {
        try
        {
            InsertQueryImpl insertQuery = new InsertQueryImpl(DOCTOR_NOTES,1);
            Row row = new Row(DOCTOR_NOTES);
            row.set(NOTE_ID,noteId);
            row.set(DOCTOR_ID,doctorId);
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_RECORD);
        }
    }

    private static void insertIntoNurseNotes(Integer noteId,Integer nurseId) throws DaoException
    {
        try
        {
            InsertQueryImpl insertQuery = new InsertQueryImpl(NURSE_NOTES,1);
            Row row = new Row(NURSE_NOTES);
            row.set(NOTE_ID,noteId);
            row.set(NURSE_ID,nurseId);
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_RECORD);
        }
    }

    private static void insertIntoPrescriptionNotes(Integer prescriptionId,Integer drugId,String note) throws DaoException
    {
        try
        {
            InsertQueryImpl insertQuery = new InsertQueryImpl(PRESCRIPTION_NOTE,1);
            Row row = new Row(PRESCRIPTION_NOTE);
            row.set(PRESCRIPTION_ID,prescriptionId);
            row.set(MEDICINE_ID,drugId);
            row.set(NOTE,note);
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_RECORD);
        }
    }



}

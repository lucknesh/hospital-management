package com.hospital.manage.database.bean;

import com.hospital.manage.Test.Examination;
import com.hospital.manage.Test.Test;
import com.hospital.manage.exception.DaoException;

import java.sql.ResultSet;
import java.util.ArrayList;

public interface ExaminationDaoImplInterface {
    void addTest(Test test) throws DaoException;

    void updateTest(Test test) throws DaoException;

    void removeTest(Integer testId) throws DaoException;

    ResultSet getAllTest() throws DaoException;

//    boolean addExamination(Examination examination) throws DaoException;

    void updateExaminationResult(ArrayList<Examination> examinations) throws DaoException;

    void addExaminationRequest(ArrayList<Integer> testIds, Integer consultationId) throws DaoException;

    ResultSet getRequestedExaminations(Integer appointmentId) throws DaoException;

    ResultSet getExaminationResult(Integer appointmentId) throws DaoException;

    ResultSet getMyExaminations(Integer appointmentId) throws DaoException;
}

package com.hospital.manage.database.bean;

import com.hospital.manage.booking.Appointment;
import com.hospital.manage.enums.AppointmentType;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.exception.NoDataFoundException;

import java.sql.ResultSet;

public interface AppointmentDaoInterface
{
    void setMaximumEBookingLimit()throws DaoException;
    void addAppointment(Appointment appointment) throws DaoException;
    Integer getTokenNumber(Integer doctorId,String date,String type,Integer status) throws DaoException;
    Integer isOnlineBookingAvailable(Integer doctorId,String date,String type)throws DaoException;
    void updateConsultationStatus(Integer cId,String status)throws DaoException;
    ResultSet getAllMyConsultations(Integer pId)throws DaoException;
    void toggleBookingTo(Boolean status,Integer doctorId) throws DaoException;
    ResultSet getAllConsultations(Integer userId)throws DaoException;
    ResultSet getAllAdmittance(Integer userId)throws DaoException;
    AppointmentType isValidateAppointmentType(Integer appointmentId)throws NoDataFoundException;
}

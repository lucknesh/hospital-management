package com.hospital.manage.database.bean;

import com.hospital.manage.exception.DaoException;
import com.hospital.manage.ward.Cabin;
import com.hospital.manage.ward.Ward;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface WardDaoImplInterface {
    void addWard(Ward ward) throws DaoException, SQLException;

    void addCabin(Cabin cabin) throws DaoException;

    //changed
    void updateWard(Ward ward) throws DaoException;

    void removeWard(Integer wardId) throws DaoException;

    void removeCabin(Integer cabinNumber) throws DaoException;

    ResultSet getAllWards() throws DaoException;

    ResultSet getAvailableCabins(Integer wardId, String day) throws DaoException;

    ResultSet getNoNurseCabins() throws DaoException;

    ResultSet getNoNurseCabins(Integer wardId) throws DaoException;
}

package com.hospital.manage.database.bean;

import com.adventnet.db.api.RelationalAPI;
import com.adventnet.ds.query.*;
import com.adventnet.persistence.DataAccess;
import com.adventnet.persistence.DataAccessException;
import com.adventnet.persistence.Row;
import com.hospital.manage.Test.Examination;
import com.hospital.manage.Test.Test;
import com.hospital.manage.database.DbMeta;
import com.hospital.manage.enums.ExaminationStatus;
import com.hospital.manage.enums.ExceptionMessage;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.time.DateTime;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ExaminationDaoImpl extends DbMeta implements ExaminationDaoImplInterface {

    private static final RelationalAPI RELATIONAL_API = RelationalAPI.getInstance();

    @Override
    public void addTest(Test test) throws DaoException {
        try {
            InsertQueryImpl insertQuery = new InsertQueryImpl(TEST, 1);
            Row row = new Row(TEST);
            row.set(NAME, test.getName());
            row.set(FEE, test.getFee());
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_EXAMINATION.toString());
        }
    }

    @Override
    public void updateTest(Test test) throws DaoException {
        try {
            UpdateQuery updateQuery = new UpdateQueryImpl(TEST);
            updateQuery.setCriteria(new Criteria(new Column(TEST, TEST_ID), test.getTestId(), QueryConstants.EQUAL));
            updateQuery.setUpdateColumn(NAME, test.getName());
            updateQuery.setUpdateColumn(FEE, test.getFee());
            DataAccess.update(updateQuery);
        } catch (DataAccessException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_EXAMINATION);
        }
    }

    @Override
    public void removeTest(Integer testId) throws DaoException {
        try {
            DeleteQuery deleteQuery = new DeleteQueryImpl(TEST);
            deleteQuery.setCriteria(new Criteria(new Column(TEST, TEST_ID), testId, QueryConstants.EQUAL));
            DataAccess.delete(deleteQuery);
        } catch (DataAccessException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_EXAMINATION);
        }

    }

    @Override
    public ResultSet getAllTest() throws DaoException {
        try (Connection connection = RELATIONAL_API.getConnection()) {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(TEST));
            selectQuery.addSelectColumn(new Column(TEST, "*"));
            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery, connection);
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        } catch (SQLException | QueryConstructionException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }


    @Override
    public void addExaminationRequest(ArrayList<Integer> testIds, Integer consultationId) throws DaoException {
        try {
            for (Integer testId : testIds) {
                InsertQueryImpl insertQuery = new InsertQueryImpl(EXAMINATION, 1);
                Row row = new Row(EXAMINATION);
                row.set(APPOINTMENT_ID, consultationId);
                row.set(TEST_ID, testId);
                row.set(STATUS, ExaminationStatus.YET_TO_TAKE.toString());
                insertQuery.addRow(row);
                DataAccess.add(insertQuery);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_EXAMINATION);
        }

    }

    @Override
    public ResultSet getRequestedExaminations(Integer appointmentId) throws DaoException {
        try (Connection connection = RELATIONAL_API.getConnection()) {
            DataSet dataSet;
            SelectQuery selectQuery;

            selectQuery = new SelectQueryImpl(new Table(EXAMINATION));
            selectQuery.addSelectColumn(new Column(EXAMINATION, TEST_ID));

            Criteria criteria = new Criteria(new Column(EXAMINATION, APPOINTMENT_ID), appointmentId, QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(EXAMINATION, STATUS), ExaminationStatus.YET_TO_TAKE.toString(), QueryConstants.EQUAL));

            selectQuery.setCriteria(criteria);

            dataSet = RELATIONAL_API.executeQuery(selectQuery, connection);

            ArrayList<Integer> testIds = new ArrayList<>();
            testIds.add(0);

            while (dataSet.next()) {
                testIds.add((Integer) dataSet.getValue(TEST_ID));
            }

            selectQuery = new SelectQueryImpl(new Table(TEST));

            selectQuery.addSelectColumn(new Column(TEST, NAME));
            selectQuery.addSelectColumn(new Column(TEST, TEST_ID));
            selectQuery.addSelectColumn(new Column(EXAMINATION, EXAMINATION_ID));

            Join join1 = new Join(TEST, EXAMINATION, new String[]{TEST_ID}, new String[]{TEST_ID}, Join.INNER_JOIN);
            selectQuery.addJoin(join1);

            criteria = new Criteria(new Column(EXAMINATION, APPOINTMENT_ID), appointmentId, QueryConstants.EQUAL);
            criteria = criteria.and(new Criteria(new Column(TEST, TEST_ID), testIds.toArray(new Integer[testIds.size()]), QueryConstants.IN));

            selectQuery.setCriteria(criteria);

            dataSet = RELATIONAL_API.executeQuery(selectQuery, connection);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;

        } catch (SQLException | QueryConstructionException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    @Override
    public ResultSet getExaminationResult(Integer appointmentId) throws DaoException {
        try (Connection connection = RELATIONAL_API.getConnection()) {
            SelectQuery selectQuery = new SelectQueryImpl(new Table(TEST));

            selectQuery.addSelectColumn(new Column(TEST, NAME));
            selectQuery.addSelectColumn(new Column(EXAMINATION_RESULT, RESULT));
            selectQuery.addSelectColumn(new Column(EXAMINATION_RESULT, DATE_TIME));

            Join join = new Join(TEST, EXAMINATION, new String[]{TEST_ID}, new String[]{TEST_ID}, Join.INNER_JOIN);
            selectQuery.addJoin(join);
            join = new Join(EXAMINATION, EXAMINATION_RESULT, new String[]{EXAMINATION_ID}, new String[]{EXAMINATION_ID}, Join.INNER_JOIN);
            selectQuery.addJoin(join);
            selectQuery.setCriteria(new Criteria(new Column(EXAMINATION, APPOINTMENT_ID), appointmentId, QueryConstants.EQUAL));

            DataSet dataSet = RELATIONAL_API.executeQuery(selectQuery, connection);

            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(dataSet.getResultSetAdapter().getResultSet());
            return cachedRowSet;
        } catch (SQLException | QueryConstructionException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }

    }

    @Override
    public ResultSet getMyExaminations(Integer appointmentId) throws DaoException {
        try (Connection connection = RELATIONAL_API.getConnection()) {
            String sql = "SELECT DISTINCT(E.TEST_ID),T.NAME,COUNT(E.TEST_ID),T.FEE*COUNT(E.TEST_ID) FROM TEST T,EXAMINATION E WHERE E.APPOINTMENT_ID = " + appointmentId + " AND E.TEST_ID=T.TEST_ID GROUP BY E.TEST_ID";
            ResultSet resultSet = RELATIONAL_API.executeQuery(sql , connection).getResultSetAdapter().getResultSet();
            CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();
            cachedRowSet.populate(resultSet);
            return cachedRowSet;
        } catch (SQLException | QueryConstructionException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_FETCHING_DATA);
        }
    }

    @Override
    public void updateExaminationResult(ArrayList<Examination> examinations) throws DaoException {
        try {
            for (Examination examination : examinations)
            {
                Integer examinationId = examination.getExaminationId();
                updateExaminationResult(examination);
                updateExaminationStatus(examinationId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_EXAMINATION);
        }
    }

    private static void updateExaminationStatus(Integer examinationId) throws DaoException {
        try {
            UpdateQuery updateQuery = new UpdateQueryImpl(EXAMINATION);
            updateQuery.setCriteria(new Criteria(new Column(EXAMINATION, EXAMINATION_ID), examinationId, QueryConstants.EQUAL));
            updateQuery.setUpdateColumn(STATUS, ExaminationStatus.TAKEN.toString());
            DataAccess.update(updateQuery);
        } catch (DataAccessException e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

    private static void updateExaminationResult(Examination examination) throws DaoException {
        try {
            InsertQueryImpl insertQuery = new InsertQueryImpl(EXAMINATION_RESULT, 1);
            Row row = new Row(EXAMINATION_RESULT);
            row.set(EXAMINATION_ID, examination.getExaminationId());
            row.set(DATE_TIME, DateTime.getTimeStamp());
            row.set(RESULT, examination.getResult());
            insertQuery.addRow(row);
            DataAccess.add(insertQuery);
        } catch (Exception e) {
            e.printStackTrace();
            throw new DaoException(ExceptionMessage.ERROR_UPDATING_DATA);
        }
    }

}
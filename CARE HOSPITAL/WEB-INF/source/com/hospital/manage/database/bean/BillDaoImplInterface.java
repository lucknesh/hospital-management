package com.hospital.manage.database.bean;

import com.hospital.manage.exception.DaoException;
import com.hospital.manage.record.Bill;

public interface BillDaoImplInterface {
    void addBill(Bill bill, boolean admittance) throws DaoException;
}

package com.hospital.manage.database.bean;

import com.hospital.manage.exception.DaoException;
import com.hospital.manage.users.*;
import com.hospital.manage.users.User;

public interface UserDaoInterface
{
        void insertUser(User user) throws Exception;
        void invalidateAccount(Integer userId)throws DaoException;
}

package com.hospital.actionClasses.nurseDoctor;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.hospital.manage.database.NoteDao;
import com.hospital.manage.enums.AdmittanceNoteType;
import com.hospital.manage.enums.UserType;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.record.AdmittanceNote;

public class AddAdmittanceNote extends ActionSupport implements ModelDriven
{
    private static final String DOCTOR_REDIRECT = "doctorInterface";
    private static final String NURSE_REDIRECT = "nurseInterface";
    private static final String REDIRECT_URL = "redirectUrl";
    private AdmittanceNote admittanceNote = new AdmittanceNote();
    private UserType userType;
    public String execute()
    {

        try {
                admittanceNote = getAdmittanceNote();
                //admittanceNote.setDateTime(DateTime.getTimeStamp());
               if (getUserType().equals(UserType.DOCTOR))
               {
                   admittanceNote.setAdmittanceNoteType(AdmittanceNoteType.DOCTOR_NOTE);
                   ActionContext.getContext().getValueStack().set(REDIRECT_URL, DOCTOR_REDIRECT);
               }
               if (getUserType().equals(UserType.NURSE))
               {
                   admittanceNote.setAdmittanceNoteType(AdmittanceNoteType.NURSE_NOTE);
                   ActionContext.getContext().getValueStack().set(REDIRECT_URL, NURSE_REDIRECT);
               }

                NoteDao.addAdmittanceNote(getAdmittanceNote());

                return SUCCESS;

            }
        catch (DaoException e)
        {

            return ERROR;
        }

    }

//    public AdmittanceNote getAdmittanceNote(HttpServletRequest request)
//    {
//        AdmittanceNote note = new AdmittanceNote();
//
//        UserType userType = UserType.valueOf(request.getParameter("userType"));
//        note.setNote(request.getParameter("note"));
//
//
//
//        note.setAdmittanceId((Integer)request.getSession().getAttribute("appointmentId"));
//        note.setDateTime(DateTime.getTimeStamp());
//        note.setWriterId(Integer.parseInt(request.getParameter("writerId")));
//
//        return note;
//    }

    public Object getModel()
    {
        return admittanceNote;
    }

    public AdmittanceNote getAdmittanceNote() {
        return admittanceNote;
    }

    public void setAdmittanceNote(AdmittanceNote admittanceNote) {
        this.admittanceNote = admittanceNote;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }
}

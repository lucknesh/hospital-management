package com.hospital.actionClasses.authentication;

import com.hospital.manage.enums.AccountStatus;
import com.hospital.manage.enums.DoctorType;
import com.hospital.manage.enums.LoginStatus;
import com.hospital.manage.enums.NurseDuty;
import com.hospital.manage.users.User;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.AuthenticatorDao;
import com.hospital.manage.database.DoctorDao;
import com.hospital.manage.database.NurseDao;
import com.hospital.manage.exception.DaoException;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import java.sql.SQLException;
import java.util.Map;

public class Authenticator extends ActionSupport implements SessionAware
{
    private static final String SUPER_ADMIN_HOME = "super_admin";
    private static final String ADMIN_HOME = "admin";
    private static final String PATIENT_HOME = "patient";
    private static final String DOCTOR_HOME = "doctor";
    private static final String NURSE_HOME = "nurse";
    private static final String RECEPTIONIST_HOME = "receptionist";


    private String username;
    private String password;
    private User user;
    private LoginStatus loginStatus;
    private DoctorType doctorType;
    private NurseDuty nurseDuty;
    private SessionMap<String,Object> sessionMap;
    public void setSession(Map<String, Object> map) {
        sessionMap=(SessionMap)map;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DoctorType getDoctorType() {
        return doctorType;
    }

    public void setDoctorType(DoctorType doctorType) {
        this.doctorType = doctorType;
    }

    public NurseDuty getNurseDuty() {
        return nurseDuty;
    }

    public void setNurseDuty(NurseDuty nurseDuty) {
        this.nurseDuty = nurseDuty;
    }
    public LoginStatus getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(LoginStatus loginStatus) {
        this.loginStatus = loginStatus;
    }


    public String execute() throws Exception
    {
        try {

                User user = AuthenticatorDao.getUserInfo(username, password);

                if (user.getAccountStatus()== AccountStatus.ACTIVE)
                {
                    Integer userId = user.getUserId();

                    user.setPassword(password);
                    user.setUsername(username);

                    sessionMap.put("user",user);
                    sessionMap.put("loginStatus",LoginStatus.TRUE);

                    switch (user.getUserType())
                    {
                        case SUPER_ADMIN:
                            return SUPER_ADMIN_HOME;

                        case ADMIN:
                            return ADMIN_HOME;

                        case DOCTOR:
                            DoctorType doctorType = DoctorType.valueOf(DoctorDao.getDoctorDuty(userId));
                            setDoctorType(doctorType);
                            sessionMap.put("doctorType",doctorType);
                            return DOCTOR_HOME;

                        case PATIENT:
                            return PATIENT_HOME;

                        case NURSE:
                            NurseDuty nurseDuty = NurseDuty.valueOf(NurseDao.getNurseDuty(userId));
                            setNurseDuty(nurseDuty);
                            sessionMap.put("nurseDuty",nurseDuty);
                            return NURSE_HOME;

                        case RECEPTIONIST:
                            return RECEPTIONIST_HOME;
                    }
                }
                else
                {
                    return ERROR;
                }

        }catch (DaoException|SQLException e)
        {
            System.out.println(e);
            e.printStackTrace();
        }

        return ERROR;
    }

    public String logout() {
        sessionMap.remove("user");
        sessionMap.remove("loginStatus");
        sessionMap.remove("doctorType");
        sessionMap.remove("nurseDuty");
        sessionMap.invalidate();
        return SUCCESS;
    }
}

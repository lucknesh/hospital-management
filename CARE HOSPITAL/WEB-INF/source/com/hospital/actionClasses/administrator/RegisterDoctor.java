package com.hospital.actionClasses.administrator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.UserDao;
import com.hospital.manage.enums.Days;
import com.hospital.manage.enums.DoctorType;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.users.Doctor;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.*;
import java.sql.SQLException;
import java.util.*;

public class RegisterDoctor extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "manageUser";
    private static final String ERROR_REDIRECT = "newDoctorRegistrationForm";

    public String execute() throws Exception
    {
            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession();
            Doctor doctor = (Doctor) session.getAttribute("doctor");
            ArrayList<Days> days =  doctor.getDaysAvailable();
            HashMap<Days,String> from = new HashMap<>();
            HashMap<Days,String> to = new HashMap<>();

            for (Days day : days)
            {
                from.put(day,request.getParameter(day.toString()+"FROM")+":00");
                to.put(day,request.getParameter(day.toString()+"TO")+":00");
            }

            if(doctor.getDoctorType().equals(DoctorType.VISITING_DOCTOR))
            {
                HashMap<Days, Integer> cabin = new HashMap<>();
                for (Days day : days)
                {
                    cabin.put(day, Integer.parseInt(request.getParameter(day.toString() + "ID")));
                }
                doctor.setCabinNumbers(cabin);
            }

            doctor.setShiftStartTime(from);
            doctor.setShiftEndTime(to);
            session.setAttribute("doctor",doctor);

        try {
                UserDao.insertUser(doctor);
            System.out.println(doctor.getDaysAvailable());
                ActionContext.getContext().getValueStack().set("redirectUrl", SUCCESS_REDIRECT);
                return SUCCESS;

        }catch (SQLException | DaoException e)
        {
            ActionContext.getContext().getValueStack().set("redirectUrl", ERROR_REDIRECT);
            return ERROR;
        }
    }
}

package com.hospital.actionClasses.administrator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.InventoryDao;
import com.hospital.manage.exception.DaoException;

public class RemoveResource extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "manageInventory";
    private static final String ERROR_REDIRECT = "removeResourceForm";
    private static final String REDIRECT_URL = "redirectUrl";
    private Integer itemID;

    public String execute()
    {
        try
        {
            InventoryDao.removeItem(getItemID());
            ActionContext.getContext().getValueStack().set(REDIRECT_URL,SUCCESS_REDIRECT);
            return SUCCESS;
        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL,ERROR_REDIRECT);
            return ERROR;
        }
    }

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }
}

package com.hospital.actionClasses.administrator;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.hospital.manage.database.WardDao;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.ward.Cabin;

public class AddCabin extends ActionSupport implements ModelDriven
{
    private static final String SUCCESS_REDIRECT = "manageWard";
    private static final String ERROR_REDIRECT = "addCabinForm";
    private static final String REDIRECT_URL = "redirectUrl";
    private Cabin cabin = new Cabin();

    public String execute()
    {
        try
        {
                WardDao.addCabin(this.cabin);
                ActionContext.getContext().getValueStack().set(REDIRECT_URL, SUCCESS_REDIRECT);
                return SUCCESS;
        }
        catch (DaoException e)
        {
                ActionContext.getContext().getValueStack().set(REDIRECT_URL, ERROR_REDIRECT);
                return ERROR;
        }
    }

    public Object getModel()
    {
        return cabin;
    }

    public Cabin getCabin() {
        return cabin;
    }

    public void setCabin(Cabin cabin) {
        this.cabin = cabin;
    }
}

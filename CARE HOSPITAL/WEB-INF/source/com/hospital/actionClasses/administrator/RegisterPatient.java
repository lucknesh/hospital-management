package com.hospital.actionClasses.administrator;


import com.adventnet.persistence.DataAccessException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.hospital.manage.database.UserDao;
import com.hospital.manage.users.Address;
import com.hospital.manage.users.Patient;

import java.sql.SQLException;


public class RegisterPatient extends ActionSupport implements ModelDriven
{
    private Patient patient = new Patient();
//    private String dateOfBirth;
    private Integer stateId;
    private Integer districtId;
    private String pincode;
    private String doorNumber;
    private String street;
    private String city;
    private static final String SUCCESS_REDIRECT = "newPatientRegistrationForm";
    private static final String ERROR_REDIRECT = "newPatientRegistrationForm";
    private static final String REDIRECT_URL = "redirectUrl";
    public String execute() throws Exception
    {

        try {
//            patient.setDob(getDateOfBirth());
            patient.setAddress(getAddress());
            UserDao.insertUser(getPatient());
            ActionContext.getContext().getValueStack().set(REDIRECT_URL,SUCCESS_REDIRECT);
            return SUCCESS;
    }
    catch (SQLException | DataAccessException e)
    {
        ActionContext.getContext().getValueStack().set(REDIRECT_URL,ERROR_REDIRECT);
        return ERROR;
    }
    }

    public Object getModel()
    {
        return patient;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Address getAddress()
    {
        Address address = new Address();

        address.setDoorNumber(getDoorNumber());
        address.setStreet(getStreet());
        address.setCity(getCity());
        address.setDistrictId(getDistrictId());
        address.setStateId(getStateId());
        address.setPincode(getPincode());

        return address;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getDoorNumber() {
        return doorNumber;
    }

    public void setDoorNumber(String doorNumber) {
        this.doorNumber = doorNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

//    public String getDateOfBirth() {
//        return dateOfBirth;
//    }
//
//    public void setDateOfBirth(String dateOfBirth) {
//        this.dateOfBirth = dateOfBirth;
//    }
}

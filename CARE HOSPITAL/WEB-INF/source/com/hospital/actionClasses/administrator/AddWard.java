package com.hospital.actionClasses.administrator;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.hospital.manage.database.WardDao;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.ward.Ward;

import java.sql.SQLException;

public class AddWard extends ActionSupport implements ModelDriven<Ward>
{
    private static final String SUCCESS_REDIRECT = "manageWard";
    private static final String ERROR_REDIRECT = "addWardForm";
    private static final String REDIRECT_URL = "redirectUrl";
    private Ward ward = new Ward();

    public String execute()
    {
        try
        {
                WardDao.addWard(this.ward);
                ActionContext.getContext().getValueStack().set(REDIRECT_URL, SUCCESS_REDIRECT);
                return SUCCESS;
        }
        catch (DaoException | SQLException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, ERROR_REDIRECT);
            return ERROR;
        }
    }
    public Ward getModel()
    {
        return ward;
    }



    public void setWard(Ward ward) {
        this.ward = ward;
    }
    //    private Ward getWard(HttpServletRequest request)
//    {
//        Ward ward = new Ward();
//        ward.setName(request.getParameter("name"));
//        ward.setFromCabinNumber(Integer.parseInt(request.getParameter("from")));
//        ward.setToCabinNumber(Integer.parseInt(request.getParameter("to")));
//
//        return ward;
//    }
}

package com.hospital.actionClasses.administrator;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.WardDao;
import com.hospital.manage.exception.DaoException;

public class RemoveWard extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "manageWard";
    private static final String ERROR_REDIRECT = "removeWardForm";
    private static final String REDIRECT_URL = "redirectUrl";
    private Integer wardId;

    public Integer getWardId() {
        return wardId;
    }

    public void setWardId(Integer wardId) {
        this.wardId = wardId;
    }

    public String execute()
    {
        try
        {
                WardDao.removeWard(getWardId());
                ActionContext.getContext().getValueStack().set(REDIRECT_URL, SUCCESS_REDIRECT);
                return SUCCESS;
        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, ERROR_REDIRECT);
            return ERROR;
        }
    }
}

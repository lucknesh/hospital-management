package com.hospital.actionClasses.administrator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.database.WardDao;
import com.hospital.manage.exception.DaoException;

public class RemoveCabin extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "manageWard";
    private static final String ERROR_REDIRECT = "removeCabinForm";
    private static final String REDIRECT_URL = "redirectUrl";
    private Integer cabinNumber;

    public Integer getCabinNumber() {
        return cabinNumber;
    }

    public void setCabinNumber(Integer cabinNumber) {
        this.cabinNumber = cabinNumber;
    }

    public String execute()
    {
        try
        {
            WardDao.removeCabin(cabinNumber);
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, SUCCESS_REDIRECT);
            return SUCCESS;
        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL, ERROR_REDIRECT);
            return ERROR;
        }
    }
}

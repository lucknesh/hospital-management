package com.hospital.actionClasses.administrator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.hospital.manage.database.InventoryDao;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.inventory.Item;

public class AddResource extends ActionSupport implements ModelDriven
{
    private static final String SUCCESS_REDIRECT = "manageInventory";
    private static final String ERROR_REDIRECT = "addNewResourceForm";
    private static final String REDIRECT_URL = "redirectUrl";
    private Item item = new Item();

    public String execute()
    {

        try
        {
            InventoryDao.addItem(getItem());
            ActionContext.getContext().getValueStack().set(REDIRECT_URL,SUCCESS_REDIRECT);
            return SUCCESS;
        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL,ERROR_REDIRECT);
            return ERROR;
        }
    }

    public Object getModel()
    {
        return item;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}

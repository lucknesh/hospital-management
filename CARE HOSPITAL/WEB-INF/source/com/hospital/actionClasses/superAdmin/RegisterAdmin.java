package com.hospital.actionClasses.superAdmin;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.hospital.manage.database.UserDao;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.users.Admin;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.*;
import java.io.PrintWriter;
import java.sql.SQLException;

public class RegisterAdmin extends ActionSupport implements ModelDriven
{
    private static final String SUCCESS_REDIRECT = "superAdminInterface";
    private static final String ERROR_REDIRECT = "newAdminForm";
    private  Admin admin = new Admin();

    public String execute()throws Exception
    {

        HttpServletResponse response = ServletActionContext.getResponse();
        PrintWriter out = response.getWriter();
        out.print(getAdmin().getName());

        try {
                UserDao.insertUser(getAdmin());
                ActionContext.getContext().getValueStack().set("redirectUrl", SUCCESS_REDIRECT);
                return SUCCESS;
        }
        catch (SQLException | DaoException e)
        {
                ActionContext.getContext().getValueStack().set("redirectUrl", ERROR_REDIRECT);
                return ERROR;
        }
    }

    public Object getModel()
    {
        return admin;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

}

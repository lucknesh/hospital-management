package com.hospital.actionClasses.appointment;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.hospital.manage.booking.EmergencyAdmittance;
import com.hospital.manage.booking.GeneralAdmittance;
import com.hospital.manage.database.AppointmentDao;
import com.hospital.manage.enums.AdmittanceType;
import com.hospital.manage.enums.AppointmentType;
import com.hospital.manage.exception.DaoException;
import com.hospital.manage.time.DateTime;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class AddAdmittance extends ActionSupport
{
    private static final String SUCCESS_REDIRECT = "receptionistInterface";
    private static final String ERROR_REDIRECT = "newAdmittanceFormOne";
    private static final String REDIRECT_URL = "redirectUrl";

    public String execute()
    {
            HttpServletRequest request = ServletActionContext.getRequest();
            String[] diagnosisIDs = request.getParameterValues("diagnosisIDSelectedValue");
            String[] doctorIDs = request.getParameterValues("doctorIDSelectedValue");

            AdmittanceType admittanceType = AdmittanceType.valueOf(request.getParameter("admittanceType"));

            Integer patientId = Integer.parseInt(request.getParameter("patientId"));
            Integer primaryDoctorId = Integer.parseInt(request.getParameter("primaryDoctorId"));
            Integer dayNurseId = Integer.parseInt(request.getParameter("dayNurseId"));
            Integer eveningNurseId = Integer.parseInt(request.getParameter("eveningNurseId"));
            Integer diagnosisId = -1;
            Integer consultationId = -1;

            ArrayList<Integer> diagnosisIds = new ArrayList<>();
            ArrayList<Integer> secondaryDoctorIds = new ArrayList<>();

            for (int i = 0; i < doctorIDs.length-1; i++) {
                System.out.println(doctorIDs[i]);
                secondaryDoctorIds.add( Integer.parseInt(doctorIDs[i]) );
            }

            if (admittanceType.equals(AdmittanceType.GENERAL)) {
                consultationId = Integer.parseInt(request.getParameter("consultationId"));
                for (int i = 0; i < diagnosisIDs.length-1; i++) {
                    System.out.println(diagnosisIDs[i]);
                    diagnosisIds.add( Integer.parseInt(diagnosisIDs[i]) );
                }

            }
            if (admittanceType.equals(AdmittanceType.EMERGENCY)) {
                diagnosisId = Integer.parseInt(request.getParameter("diagnosisId"));
            }

        try {
            if (admittanceType.equals(AdmittanceType.GENERAL)) {
                GeneralAdmittance admittance = new GeneralAdmittance();
                admittance.setConsultationId(consultationId);
                admittance.setDiagnosisIds(diagnosisIds);
                admittance.setAdmittanceType(admittanceType);
                admittance.setPatientId(patientId);
                admittance.setPrimaryInchargeDoctorId(primaryDoctorId);
                admittance.setAdmissionDateTime(DateTime.getTimeStamp());
                admittance.setSecondaryInchargeDoctorIds(secondaryDoctorIds);
                admittance.setNightInchargeNurseId(eveningNurseId);
                admittance.setDayInchargeNurseId(dayNurseId);
                admittance.setAppointmentType(AppointmentType.ADMITTANCE);
                AppointmentDao.addAppointment(admittance);
                System.out.println(diagnosisIds);

            } else {
                EmergencyAdmittance admittance = new EmergencyAdmittance();
                admittance.setDiagnosisId(diagnosisId);
                admittance.setAdmittanceType(admittanceType);
                admittance.setPatientId(patientId);
                admittance.setPrimaryInchargeDoctorId(primaryDoctorId);
                admittance.setAdmissionDateTime(DateTime.getTimeStamp());
                admittance.setSecondaryInchargeDoctorIds(secondaryDoctorIds);
                admittance.setNightInchargeNurseId(eveningNurseId);
                admittance.setDayInchargeNurseId(dayNurseId);
                admittance.setAppointmentType(AppointmentType.ADMITTANCE);
                AppointmentDao.addAppointment(admittance);
            }

            ActionContext.getContext().getValueStack().set(REDIRECT_URL,SUCCESS_REDIRECT);
            return SUCCESS;

        }
        catch (DaoException e)
        {
            ActionContext.getContext().getValueStack().set(REDIRECT_URL,ERROR_REDIRECT);
            return ERROR;
        }
    }
}

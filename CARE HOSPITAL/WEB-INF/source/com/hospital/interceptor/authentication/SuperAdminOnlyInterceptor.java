package com.hospital.interceptor.authentication;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.hospital.manage.enums.UserType;
import com.hospital.manage.users.User;

import java.util.Map;

public class SuperAdminOnlyInterceptor extends ActionSupport implements Interceptor
{
    @Override
    public void destroy() {

    }

    @Override
    public void init() {

    }

    @Override
    public String intercept(ActionInvocation actionInvocation) throws Exception {
        Map<String,Object> session = actionInvocation.getInvocationContext().getSession();
        User user = (User) session.get("user");
        UserType userType = user.getUserType();
        if (userType==UserType.SUPER_ADMIN) return SUCCESS;
        else return ERROR;
    }

}


<%@ page errorPage="errorpage.jsp" %>
<%
    String title = "title";
    String message = "message";
    String to = "index.jsp";

    try
    {
        title = (String)request.getAttribute("title");
        message = (String)request.getAttribute("message");
        to = (String)request.getAttribute("to");
    }
    catch(Exception e)
    {
        e.printStackTrace();
    }
%>
<!DOCTYPE html>
<html>
<style>
.pop-up-button {
  background-color: orange;
  border: none;
  color: white;
  padding: 12px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  transition-duration: 0.4s;
  cursor: pointer;
  border-radius : 2px;
}

.pop-up-button1 {
  background-color: orange;
  color: white;
}

.pop-up-button1:hover {
  color: #1f456e ;
}
</style>
<head>
<title>Popup Message</title>

</head>
<body>
<link rel="stylesheet" href="design.css" >
<div class="popup-container">

  <div class="popup-box">

    <div class="popup-message">
      <h2 class="popup-title"> <% out.print(title); %> </h2>
      <p class="popup-text"> <% out.print(message); %> </p>
    </div>

    <a href=<% out.print(to);  %> class='pop-up-button pop-up-button1'>OK</a>
  </div>
</div>
</body>
</html>

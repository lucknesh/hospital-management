
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.io.*" %>
<%@ page import="javax.servlet.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="com.hospital.manage.database.*" %>
          <%!
              private int diagnosisIdNumber= 1;
              private int doctorIdNumber = 1;
          %>
<%@ page import="com.hospital.manage.users.*,com.hospital.manage.enums.*" %>
          <%
                User user = (User)session.getAttribute("user");
                Integer userId = user.getUserId();
                UserType userType = user.getUserType();
                String userName = user.getName();

            String admittanceType = request.getParameter("admittanceType");
            int primaryDoctorId = Integer.parseInt(request.getParameter("primaryDoctorId"));
            int consultationId = -1;
            int patientId = Integer.parseInt(request.getParameter("patientId"));

            if(admittanceType.equals("GENERAL"))
            {
                consultationId = Integer.parseInt(request.getParameter("consultationId"));
            }

            ResultSet rs = null;
            diagnosisIdNumber = 1;
            doctorIdNumber = 1;
          %>

<html xmlns="http://www.w3.org/1999/html">
        <body style="background-color:#1f456e;">
    <link rel="stylesheet" type="text/css" href="design.css">
<head>
<style>
.diagnosis-id{
    width: 400px;

    position: fixed;
    top: 36%;
    left: 10%;
}
.nurse-id{
    width: 400px;

    position: fixed;
    top: 32.5%;
    left: 40%;
}
.doctor-id{
    width: 400px;

    position: fixed;
    top: 36%;
    left: 70%;
}
</style>
         <title>nurse</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <div class="header">
        <a href="receptionistInterface"><div class="logo_holder"><h class="hospital"><b>CARE + </b></h></div></a>
        </div><br>
        <div class="logout_holder">
        <form action="Logout">
        <input type="submit" value="Logout" class="button button1">
        </form>
        </div>
                <div class="header_bottom"><div class='name-holder'><% out.print(userType+" : "+userName); %></div></div>
        <div class='frame'>
  <title>receptionist</title>
</head>
<form id="dynamicForm" action="AddAdmittance" method="POST">
<body>
   <div class='diagnosis-id'>
   <%
        if(admittanceType.equals("GENERAL"))
        {
        out.print("<button type='button' class='form-button form-button1' onclick="+"addDropdown('diagnosisID')"+" > ADD DIAGNOSIS + </button><br>");
        out.print("<br><div id='diagnosisID'><div>");
        }
        else
        {
            rs = GenericDao.getDiagnosisIdAndName();
            out.print("<h class='form-text'>DIAGNOSIS ID : </h>");
            out.print("<br> <br><select class='get' name='diagnosisId' ><br>");
            while(rs.next())
            {
            out.print("<option value='"+rs.getInt(1)+"'>"+rs.getString(2)+"</option>");
            }
            out.print("</select>");
        }

   %>
   <div class='nurse-id'>
   <%
        rs = NurseDao.getDayNurses();
        out.print("<h class='form-text'>DAY CARE NURSE ID : </h>");
        out.print("<br> <br><select class='get' name='dayNurseId' ><br>");
        while(rs.next())
        {
        out.print("<option value='"+rs.getInt(1)+"'>"+rs.getString(2)+" ("+rs.getString(3)+")</option>");
        }
        out.print("</select>");

        rs = NurseDao.getEveningNurses();
        out.print("<br><br><h class='form-text'>EVENING CARE NURSE ID : </h>");
        out.print("<br><br><select class='get' name='eveningNurseId' ><br>");
        while(rs.next())
        {
        out.print("<option value='"+rs.getInt(1)+"'>"+rs.getString(2)+" ("+rs.getString(3)+")</option>");
        }
        out.print("</select>");
   %>
           <div class='doctor-id'>
           <button type='button' class='form-button form-button1' onclick="addDropdown('doctorID')"> ADD DOCTOR + </button><br>
           <br><div id='doctorID'> </div>
           <div>

         <script>
             var dropdownValues = {
                  <%
                   String diagnosisIdAndValue = "";
                   String doctorIdAndValue="";

                   rs = GenericDao.getDiagnosisIdAndName();
                   while(rs.next())
                   {
                    diagnosisIdAndValue += "\"";
                    diagnosisIdAndValue += rs.getInt(1);
                    diagnosisIdAndValue += "\"";
                    diagnosisIdAndValue += ",";
                    diagnosisIdAndValue += "\"";
                    diagnosisIdAndValue += rs.getString(2);
                    diagnosisIdAndValue += "\"";
                    diagnosisIdAndValue += ",";
                   }
                    rs = DoctorDao.getDutyDoctors(primaryDoctorId);
                    while(rs.next())
                      {
                       doctorIdAndValue += "\"";
                       doctorIdAndValue += rs.getInt(1);
                       doctorIdAndValue += "\"";
                       doctorIdAndValue += ",";
                       doctorIdAndValue += "\"";
                       doctorIdAndValue += rs.getString(2)+" ("+rs.getString(3)+")";
                       doctorIdAndValue += "\"";
                       doctorIdAndValue += ",";
                      }
                   %>

                 diagnosisID: [<% out.print(diagnosisIdAndValue.substring(0,diagnosisIdAndValue.length())); %>],
                 doctorID: [<% out.print(doctorIdAndValue.substring(0,doctorIdAndValue.length())); %>],

             };

             function addDropdown(containerId) {
                 var dropdownContainer = document.getElementById(containerId);

                 var selectList = document.createElement("select");
                 selectList.name = containerId + "SelectedValue";

                 var values = dropdownValues[containerId];
                 for (var i = 0; i < values.length; i++) {
                     selectList.setAttribute("class", "get");
                     var option = document.createElement("option");
                     option.value = values[i++];
                     option.text = values[i];
                     selectList.appendChild(option);
                 }

                    var linebreak = document.createElement("br");
                    dropdownContainer.appendChild(linebreak);
                 dropdownContainer.appendChild(selectList);



             }

             function submitForm() {
                 var form = document.getElementById("dynamicForm");


                 var containerIds = ["diagnosisID", "doctorID"];

                 for (var i = 0; i < containerIds.length; i++) {
                     var containerId = containerIds[i];


                     var select = document.querySelector("#" + containerId + " select");


                     var hiddenInput = document.createElement("input");
                     hiddenInput.type = "hidden";
                     hiddenInput.name = containerId + "SelectedValue";
                     hiddenInput.value = select.value;


                     form.appendChild(hiddenInput);
                 }


                 form.submit();
             }
         </script>


   <%
    out.print("<input type='hidden' name='doctorIdNumber' value='"+doctorIdNumber+"' >");
    out.print("<input type='hidden' name='diagnosisIdNumber'  value='"+diagnosisIdNumber+"' >");
    out.print("<input type='hidden' name='consultationId'  value='"+consultationId+"' >");
    out.print("<input type='hidden' name='primaryDoctorId'  value='"+primaryDoctorId+"' >");
    out.print("<input type='hidden' name='admittanceType'  value='"+admittanceType+"' >");
    out.print("<input type='hidden' name='patientId'  value='"+patientId+"' >");

   %>
<br><br><input class='form-button form-button1' onclick='submitForm()' value='REGISTER'></form>
</body>
</html>

<%@ page import="com.hospital.manage.database.*,com.hospital.manage.users.*,com.hospital.manage.enums.*,java.sql.*" %>
<%@ page import="com.hospital.manage.users.*,com.hospital.manage.enums.*" %>
          <%
                User user = (User)session.getAttribute("user");
                Integer userId = user.getUserId();
                UserType userType = user.getUserType();
                String userName = user.getName();
                LoginStatus loginStatus = (LoginStatus)session.getAttribute("loginStatus");
                if(!( userType.equals(UserType.NURSE) && (loginStatus==LoginStatus.TRUE) ))
            {
                RequestDispatcher rd = request.getRequestDispatcher("authenticationFailed.html");
                rd.forward(request,response);
            }
      %>
<%
    String name = "name";
    int consultationId = -1;
    int tokenNumber = -1;

    try
    {
        consultationId = Integer.parseInt(request.getParameter("appointmentId"));
        tokenNumber = Integer.parseInt(request.getParameter("tokenNumber"));
        name = request.getParameter("patientName");
    }
    catch(Exception e)
    {
        e.printStackTrace();
    }
%>
<!DOCTYPE html>
<html>
<style>
.pop-up-button {
  background-color: orange;
  border: none;
  color: white;
  padding: 12px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  transition-duration: 0.4s;
  cursor: pointer;
  border-radius : 2px;
}

.pop-up-button1 {
  background-color: orange;
  color: white;
}

.pop-up-button1:hover {
  color: #1f456e ;
  }

.close-button {
    position: absolute;
    top: 41%;
    right: 35.7%;
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: red;
    color: white;
    font-weight: bold;
    text-align: center;
    line-height: 20px;
    cursor: pointer;
}
</style>
<head>
<title>Popup Message</title>

</head>
<body>
<link rel="stylesheet" href="design.css" >
<div class="popup-container">
  <div class="popup-box">
    <div class="popup-message">
      <h2 class="popup-title"> <% out.print("NAME : "+name); %> </h2>
      <p class="popup-text"> <% out.print("CONSULTATION ID : "+consultationId+" | TOKEN NUMBER : "+tokenNumber); %> </p>
      <a href='consultationQueue' class='close-button'>X</a>
    </div>

    <form action='UpdateConsultationStatus' method='post'>
    <input type='hidden' name='userType' value= <% out.print(userType); %> >
    <input type='hidden' name='patientName' value= <% out.print(name); %> >
    <input type='hidden' name='appointmentId' value= <% out.print(consultationId); %> >
    <input type='submit' name='status' value='DELAYED' class='pop-up-button pop-up-button1' style='background-color: #bf80ff;' %> <input type='submit' name='status' value='ALLOW' class='pop-up-button pop-up-button1' %>
  </div>
</div>
</body>
</html>

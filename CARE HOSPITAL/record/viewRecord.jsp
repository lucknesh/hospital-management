<%@ page import="com.hospital.manage.users.*,com.hospital.manage.database.*,java.sql.*" %>
<%@ page import="com.hospital.manage.users.*,com.hospital.manage.enums.*" %>
          <%
                User user = (User)session.getAttribute("user");
                Integer userId = user.getUserId();
                UserType userType = user.getUserType();
                String userName = user.getName();

                int appointmentId = Integer.parseInt(request.getParameter("appointmentId"));
                session.setAttribute("appointmentId",appointmentId);
                String appointmentType = request.getParameter("appointmentType");

                String homeUrl = "patientInterface";

                if((userType.equals(UserType.NURSE) || userType.equals(UserType.DOCTOR) ))
                {
                    if(userType==UserType.DOCTOR) homeUrl="doctorInterface";
                    else homeUrl="nurseInterface";
                }
          %>
<html>
<style>
.test-result {
            position : fixed;
            top : 90%;
            left : 36%;
            width: 800px;
            height: 500px;
            overflow: auto;
        }
.prescription-list{
          position : fixed;
          top : 30%;
          left : 36%;
          width: 800px;
          height: 500px;
          overflow: auto;
     }
.note-view{
          position : fixed;
          top : 15%;
          left : 21%;
          width: 800px;
          height: 500px;
          overflow: auto;
     }
</style>
        <body style="background-color:#1f456e;">
    <link rel="stylesheet" type="text/css" href="design.css">

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <div class="header">
        <a href= <% out.print(homeUrl); %> ><div class="logo_holder"><h class="hospital"><b>CARE + </b></h></div></a>
        <div class="logout_holder">
        <form action="Logout">
        <input type="submit" value="Logout" class="button button1">
        </form>
        <div>
        </div><br>
                <div class="header_bottom"><div class='name-holder'><% out.print(userType+" : "+userName); %></div></div>
        <div class="frame">
        <title>patient</title>
    </head>
    <body>
    <br><br>
    <div class='prescription-list'>
    <%
        RequestDispatcher rd = request.getRequestDispatcher("viewAllPrescription.jsp");
        rd.include(request,response);
    %>
    </div><br><br>

    <div class='test-result'>
    <%
        rd = request.getRequestDispatcher("viewTestResults.jsp");
        rd.include(request,response);
    %>
    </div><br><br>

    <div class='note-view'>
    <h class='form-text'>
    <%
    if(appointmentType.equals("2"))
    {
        rd = request.getRequestDispatcher("viewAdmittanceNotes.jsp");
        rd.include(request,response);
        out.print("<br>");
    }
    else if(appointmentType.equals("1"))
    {
        ResultSet rs = NoteDao.getConsultationNote(appointmentId);
        if(rs.next())
        {
        out.print("<pre>RE-VISIT SUGGESTED DATE : "+rs.getString(1));
        out.print("<br>DOCTOR NOTE             : "+rs.getString(2)+"</pre>");
        out.print("<br>");
        }
    }
    %>
    </div><br><br>

    </div>
    </body></pre>
</html>
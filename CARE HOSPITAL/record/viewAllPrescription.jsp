
<%@ page import="javax.servlet.ServletException,javax.servlet.http.*,java.io.*,java.sql.ResultSet" %>
<%@ page import="com.hospital.manage.users.*,com.hospital.manage.database.*,com.hospital.manage.enums.*" %>
          <%
                User user = (User)session.getAttribute("user");
                Integer userId = user.getUserId();
                UserType userType = user.getUserType();
                String userName = user.getName();

                int appointmentId = (int) session.getAttribute("appointmentId");
                int doctorId = -1;
                ResultSet rs = null;

                doctorId = userId;
                rs = NoteDao.getAllPrescription(appointmentId);

                String homeUrl = "patientInterface";

                if((userType.equals(UserType.NURSE) || userType.equals(UserType.DOCTOR) ))
                {
                    if(userType==UserType.DOCTOR) homeUrl="doctorInterface";
                    else homeUrl="nurseInterface";
                }
          %>
<html>
<style>
        .table-frame {
        height: 500px;
        width: 800px;

        position: fixed;
        top: 20%;
        left: 21%;
        }

        .table-element{
        background-color:white;
        border : solid grey;
        font-family: Arial, Helvetica, sans-serif;
        }
</style>
        <body style="background-color:#1f456e;">
        <link rel="stylesheet" type="text/css" href="design.css">
<head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <div class="header">
        <a href= <% out.print(homeUrl);%> ><div class="logo_holder"><h class="hospital"><b>CARE + </b></h></div></a>
        <div class="logout_holder">
        <form action="Logout">
        <input type="submit" value="Logout" class="button button1">
        </form>
        <div>
        </div><br>
                <div class="header_bottom"><div class='name-holder'><% out.print(userType+" : "+userName); %></div></div>
        <title>admin</title>
</head>
<div class="table-frame">
<body>
    <%
    try
    {
        out.print("<html> <style> th, td{   border-style:solid; border-color: grey; }</style>");
        out.print("<table class='table-element' style='width:100%'> <tr> <th>DOCTOR NAME</th> <th>MEDICINE NAME</th> <th>DURATION</th> <th>FREQUENCY</th> <th>WHEN</th> <th>TOTAL QUANTITY</th>  </tr>");
        while (rs.next())
        {
            out.print("<tr> <td class='table-element' style='text-align: center'>"+rs.getString(1)+"</td> <td class='table-element' style='text-align: center'>"+rs.getString(2)+"</td> <td style='text-align: center'>"+rs.getInt(3)+" "+rs.getString(4)+"</td> <td style='text-align: center'>"+rs.getString(5)+"</td> <td style='text-align: center'>"+rs.getString(6)+"</td> <td style='text-align: center'>"+rs.getInt(7)+"</td> </tr>");
        }
    }
    catch(Exception e)
    {
    e.printStackTrace();
    }
    %>
</body>
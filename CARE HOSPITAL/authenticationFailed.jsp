<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Authentication Failed</title>
  <link rel="stylesheet" type="text/css" href="design.css">
  <style>
    body {
      background-color: #1f456e;
      font-family: Arial, sans-serif;
      display: flex;
      align-items: center;
      justify-content: center;
      height: 100vh;
    }

    .container {
      background-color: #ffffff;
      border-radius: 10px;
      padding: 30px;
      max-width: 400px;
      text-align: center;
      box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
    }

    h1 {
      color: #333333;
      font-size: 24px;
      margin-bottom: 20px;
    }

    a {
      color: #ffffff;
      background-color: #007bff;
      display: inline-block;
      padding: 10px 20px;
      text-decoration: none;
      border-radius: 5px;
      margin-top: 20px;
    }

    a:hover {
      background-color: #0056b3;
    }
  </style>
</head>
<body>
  <div class="container">
    <h1>Authentication Failed</h1>
    <a href="index.jsp">LOGIN TO ACCESS</a>
  </div>
</body>
</html>

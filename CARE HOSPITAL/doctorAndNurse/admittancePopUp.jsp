
<%@ page import="com.hospital.manage.database.*,com.hospital.manage.users.*,com.hospital.manage.enums.*,java.sql.*" %>
<%@ page import="com.hospital.manage.users.*,com.hospital.manage.enums.*" %>
          <%
                User user = (User)session.getAttribute("user");
                Integer userId = user.getUserId();
                UserType userType = user.getUserType();
                String userName = user.getName();

                int patientId = Integer.parseInt(request.getParameter("patientId"));

          %>
<%
    String name = "title";
    int appointmentId = 0;

    try
    {
        name = request.getParameter("patientName");
        appointmentId =Integer.parseInt(request.getParameter("appointmentId"));
        session.setAttribute("appointmentId",appointmentId);
    }
    catch(Exception e)
    {
        e.printStackTrace();
    }
%>
<!DOCTYPE html>
<html>
<style>
.pop-up-button {
  background-color: orange;
  border: none;
  color: white;
  padding: 12px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  transition-duration: 0.4s;
  cursor: pointer;
  border-radius : 2px;
  width : 300px;
}

.pop-up-button1 {
  background-color: orange;
  color: white;
}

.pop-up-button1:hover {
  color: #1f456e ;
}
</style>
<head>
<title>Popup Message</title>

</head>
<body>
<link rel="stylesheet" href="design.css" >
<div class="popup-container">

  <div class="popup-box">

    <div class="popup-message">
      <h2 class="popup-title"> <% out.print(name); %> </h2>
      <p class="popup-text"> ADMITTANCE ID : <% out.print(appointmentId); %> </p>
    </div>

    <%
    if(userType.equals(UserType.NURSE)){
    %>
<pre> <form action='AddTestResultForm'><input type='hidden' name='appointmentId' value=<% out.print(appointmentId); %> >
<input type='submit' value='ADD TEST RESULT' class='pop-up-button pop-up-button1'></form><form action='addAdmittanceNote'><input type='hidden' name='appointmentId' value=<% out.print(appointmentId); %> >
<input type='submit' value='ADD NOTES' class='pop-up-button pop-up-button1'></form><form action='addUsedResources'><input type='hidden' name='appointmentId' value=<% out.print(appointmentId); %> >
<input type='submit' value='CONSUME RESOURCE' class='pop-up-button pop-up-button1'></form><form action='viewTestResults'><input type='hidden' name='appointmentId' value=<% out.print(appointmentId); %> >
<input type='submit' value='VIEW TEST RESULTS' class='pop-up-button pop-up-button1'></form>
<%}
else if(userType.equals(UserType.DOCTOR))
{%>
<pre> <form action='requestExamination'><input type='hidden' name='appointmentId' value=<% out.print(appointmentId); %> >
<input type='submit' value='REQUEST TO TAKE TEST' class='pop-up-button pop-up-button1'></form><form action='addAdmittanceNote'><input type='hidden' name='appointmentId' value=<% out.print(appointmentId); %> >
<input type='submit' value='ADD NOTES' class='pop-up-button pop-up-button1'></form><form action='addUsedResources'><input type='hidden' name='appointmentId' value=<% out.print(appointmentId); %> >
<input type='submit' value='CONSUME RESOURCE' class='pop-up-button pop-up-button1'></form><form action='viewTestResults'><input type='hidden' name='appointmentId' value=<% out.print(appointmentId); %> >
<input type='submit' value='VIEW TEST RESULTS' class='pop-up-button pop-up-button1'></form><form action='addPrescription'><input type='hidden' name='appointmentId' value=<% out.print(appointmentId); %> >
<input type='submit' value='ADD PRESCRIPTION' class='pop-up-button pop-up-button1'></form><form action='viewMyRecords'><input type='hidden' name='appointmentId' value=<% out.print(appointmentId); %> ><input type='hidden' name='patientId' value=<% out.print(patientId); %> >
<input type='submit' value='VIEW PATIENT RECORDS' class='pop-up-button pop-up-button1'></form>
  <%
  }
  %><form action='viewAdmittanceNotes'><input type='hidden' name='appointmentId' value=<% out.print(appointmentId); %> ><input type='submit' value='VIEW NOTES' class='pop-up-button pop-up-button1'></form><form action='viewAllPrescription'><input type='hidden' name='appointmentId' value=<% out.print(appointmentId); %> >
<input type='submit' value='VIEW PRESCRIPTION' class='pop-up-button pop-up-button1'></form></pre><br>
  </div>
</div>
</body>
</html>

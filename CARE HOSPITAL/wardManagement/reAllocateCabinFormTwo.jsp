
<%@ page import="com.hospital.manage.users.Admin,com.hospital.manage.database.*,java.sql.*,java.util.*" %>
<%@ page isELIgnored="false" %>
<%@ page import="com.hospital.manage.users.*,com.hospital.manage.enums.*" %>
          <%

                User user = (User)session.getAttribute("user");
                UserType userType = user.getUserType();
                String userName = user.getName();
                ResultSet rs = null;
                UserType userKind = UserType.valueOf(request.getParameter("userKind"));
                int userId = Integer.parseInt(request.getParameter("userId"));
                int wardId = Integer.parseInt(request.getParameter("wardId"));

                ArrayList<String> availableDays = new ArrayList<>();

                if(userKind.equals(UserType.DOCTOR))
                {
                    rs = DoctorDao.getAvailableDays(userId);
                    while(rs.next())
                    {
                        availableDays.add(Days.values()[rs.getInt(1)-1].toString());
                    }
                    session.setAttribute("AvailableDays",availableDays);
                }
                else if(userKind.equals(UserType.NURSE))
                {
                    NurseDao.setCabinIdNull(userId);
                }
                rs = WardDao.getAllWards();
          %>
<html>
    <body style="background-color:#1f456e;">
    <link rel="stylesheet" type="text/css" href="design.css">
<head>
        <div class="header">
        <a href="adminInterface"><div class="logo_holder"><h class="hospital"><b>CARE + </b></h></div></a>
        <div class="logout_holder">
        <form action="Logout">
        <input type="submit" value="Logout" class="button button1">
        </form>
        <div>
        </div><br>
        <div class="header_bottom"><div class='name-holder'><% out.print(userType+" : "+userName); %></div></div>
        <div class="frame">
        <title>admin</title>
</head>
    <body>
        <pre>
<%
    out.print("<form action='updateCabinAllocation' method='post'");
    out.print("<input type='hidden' value='"+userKind.toString()+"' name='cabinUserKind' >");
    session.setAttribute("cabinUserKind",userKind.toString());
    out.print("<input type='hidden' value="+userId+" name='cabinUserId' >");
    if(userKind.equals(UserType.DOCTOR))
    {
        for(String dayOfWeek : availableDays)
        {
            rs = WardDao.getAvailableCabins(wardId,dayOfWeek);
            out.print("<br><br><label for='roomNumber'><h class='form-text'>"+dayOfWeek+" : </h></label><select class='get' name='"+dayOfWeek+"ID' >");
            while(rs.next())
            {
                out.print("<option value="+rs.getInt(1)+">"+rs.getInt(2)+"</option>");
            }
            out.print("</select>");
        }
    }
    else if(userKind.equals(UserType.NURSE))
    {
            rs = WardDao.getNoNurseCabins(wardId);
            out.print("<br><br><label for='roomNumber'><h class='form-text'>CABIN NUMBER : </h></label><select class='get' name='cabinId' >");
            while(rs.next())
            {
                out.print("<option value="+rs.getInt(1)+">"+rs.getInt(2)+"</option>");
            }
            out.print("</select>");
    }
%>
<br>    <input class='form-button form-button1' type="submit" value="go"><br></form></pre>
        </pre>
    </body>
    </div>
</html>
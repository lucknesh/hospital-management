
<%@ page import="com.hospital.manage.database.*,java.sql.*,com.hospital.manage.users.*" %>
<%@ page import="com.hospital.manage.users.*,com.hospital.manage.enums.*" %>
<%@ page import="com.adventnet.db.api.RelationalAPI" %>
<%@ page import="com.adventnet.ds.query.*" %>

          <%
                UserType userType = UserType.PATIENT;
                try
                {
                 User user = (User)session.getAttribute("user");
                 Integer userId = user.getUserId();
                 userType = user.getUserType();
                }
                catch(Exception e)
                {
                    userType = UserType.PATIENT;
                }

                String homeUrl = "index.jsp";
                if(userType.equals(UserType.ADMIN))
                {
                    homeUrl = "adminInterface";
                }
                else if(userType.equals(UserType.RECEPTIONIST))
                {
                     homeUrl = "receptionistInterface";
                }
          %>
<html>
    <body style="background-color:#1f456e;">
    <link rel="stylesheet" type="text/css" href="design.css">

<style>
    .form_frame_patient{
         height: 200px;
         width: 400px;
         position: fixed;
         top: 22%;
         left: 20%;
    }
</style>

<head>
  <script>
    function captureData() {
      var dropdown = document.getElementById("myDropdown");
      var selectedValue = dropdown.options[dropdown.selectedIndex].value;
      document.getElementById("storedValue").value = selectedValue;
    }
  </script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <div class="header">
        <a href= <% out.print(homeUrl); %> ><div class="logo_holder"><h class="hospital"><b>CARE + </b></h></div></a>
        <div class="logout_holder">
        <form action="Logout" method='post'>
        <input type="submit" value="Logout" class="button button1">
        </form>
        <div>
        </div><br>
                <div class="header_bottom"><div class='name-holder'></div></div>
        <title>new patient</title>
</head>
<div class="form_frame_patient">
<body><pre>
<form action="registerPatient" method="post">
<input type="hidden" name='who' value= <% out.print(userType); %> >
<h class='form-text'>NAME                       : </h><input  class='get' type="text" name="name" required><h class='form-text'>    DOOR NUMBER   : </h><input  class='get' type="text" name="doorNumber" required><br><br>

<label for="bloodGroup"><h class='form-text'>BLOOD GROUP      :</h></label> <select  class='get'  name="bloodGroup" id="bloodGroup">
      <option value="A_POSITIVE">A +ive</option>
      <option value="A_NEGATIVE">A -ive</option>
      <option value="B_POSITIVE">B +ive</option>
      <option value="B_NEGATIVE">B -ive</option>
      <option value="O_POSITIVE">O +ive</option>
      <option value="O_NEGATIVE">O -ive</option>
      <option value="AB_POSITIVE">AB +ive</option>
      <option value="AB_NEGATIVE">AB -ive</option>
    </select><h class='form-text'>    STREET                : </h><input  class='get' type="text" name="street" required><br><br><br>
 <h class='form-text'>DOB                       : </h><input class='get' type="date" name="dateOfBirth" required><h class='form-text'>     STATE                   : <h class='form-text'></h></label> <select  class='get'  name="stateId" id="state" onchange="captureData()">
                                                                                                                                                               <%
                                                                                                                                                                    ResultSet rs = GenericDao.getAllStates();
                                                                                                                                                                    while(rs.next())
                                                                                                                                                                    {
                                                                                                                                                                        out.print(rs.getInt(1));
                                                                                                                                                                       out.print("<option value="+rs.getInt(1)+">"+rs.getString(2)+" , "+rs.getString(3)+"</option>");
                                                                                                                                                                    }
                                                                                                                                                               %>
                                                                                                                                                               </select><h class='form-text'><input type="hidden" id="hiddenInput" name="selectedValue">
 <br><br><h class='form-text'>MOBILE                  : </h><input type="text"  class='get' name="mobile" required><h class='form-text'>      DISTRICT            :  <h class='form-text'></h></label> <select  class='get'  name="districtId" id="districtId" ">
                                                                                                                                                                             <%
                                                                                                                                                                                  rs = GenericDao.getAllDistrict();
                                                                                                                                                                                  while(rs.next())
                                                                                                                                                                                  {
                                                                                                                                                                                      out.print(rs.getInt(1));
                                                                                                                                                                                     out.print("<option value="+rs.getInt(1)+">"+rs.getString(3)+"</option>");
                                                                                                                                                                                  }
                                                                                                                                                                             %></select>

<br><h class='form-text'>EMAIL                     : </h><input type="email"  class='get' name="email" required><h class='form-text'>    CITY                       :  </h><input  class='get' type="text" name="city" required>
<br><br><h class='form-text'>SET PASSWORD   : </h><input type="password"  class='get' name="password" required>    <h class='form-text'>PINCODE               :  </h><input type="text"  class='get' name="pincode" required><br>

                                                                <input class='form-button form-button1' type="submit" value="go"><br><br></pre>

</form>



</body>
<div>
</html>

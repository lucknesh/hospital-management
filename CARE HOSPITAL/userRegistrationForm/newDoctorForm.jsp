
<%@ page import="com.hospital.manage.users.Admin,com.hospital.manage.database.*,java.sql.*" %>
<%@ page import="com.hospital.manage.users.*,com.hospital.manage.enums.*" %>
          <%
                RequestDispatcher rd = request.getRequestDispatcher("authenticationFailed.html");
                if(session.getAttribute("loginStatus")==null) rd.forward(request,response);
                User user = (User)session.getAttribute("user");
                Integer userId = user.getUserId();
                UserType userType = user.getUserType();
                String userName = user.getName();
                LoginStatus loginStatus = (LoginStatus)session.getAttribute("loginStatus");
                if(!( userType.equals(UserType.ADMIN) && (loginStatus==LoginStatus.TRUE) ))
                {
                    rd.forward(request,response);
                }

                ResultSet rs = WardDao.getAllWards();
            %>
<html>
<div>
    <head>
    <body style="background-color:#1f456e;">
    <link rel="stylesheet" type="text/css" href="design.css">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <div class="header">
            <a href="adminInterface"><div class="logo_holder"><h class="hospital"><b>CARE + </b></h></div></a>
            <div class="logout_holder">
                        <form action="Logout">
                    <input type="submit" value="Logout" class="button button1">
                </form>
                <div>
                </div><br>
                        <div class="header_bottom"><div class='name-holder'><% out.print(userType+" : "+userName); %></div></div>
                <title>new doctor</title>
    </head>
    <div class="form_frame">
<body><pre>
    <form action="newDoctorShiftAllocationForm" method="post">
<h class='form-text'>NAME                                       : </h><input class='get' type="text" name="name" required><br>
<label for="type"><h class='form-text'>TYPE                                        :</h></label> <select class='get' name="type" >
      <option value='DUTY_DOCTOR'>DUTY DOCTOR</option>
      <option value='VISITING_DOCTOR'>VISITING DOCTOR</option>
    </select><br>
<label for="specialisation"><h class='form-text'>SPECIALIZATION                    :</h></label> <select class='get' name="specialisation" >
      <option value="ENT">ENT</option>
      <option value="DENTIST">DENTIST</option>
      <option value="UROLOGIST">UROLOGIST</option>
      <option value="GENERAL">GENERAL</option>
      <option value="PSYCHIATRIST">PSYCHIATRIST</option>
    </select><br>
<h class='form-text'>EXPERIENCE                          : </h><input class='get' type="number" name="experience" min="0" required><br>
<h class='form-text'>CONSULTATION FEE              : </h><input class='get' type="number" name="consultationFee" min="0" required><br>
<h class='form-text'>MOBILE                                    : </h><input class='get' type="text" name="mobile" required><br>
<h class='form-text'>EMAIL                                       : </h><input class='get' type="email" name="email" required><br>
<h class='form-text'>SET PASSWORD                     : </h><input class='get' type="password" name="password" required><br>
<label for="wardId"><h class='form-text'>WARD                                      :</h></label> <select class='get' name="wardId" >
<%
    while(rs.next())
    {
    out.print("<option value="+rs.getInt(1)+">"+rs.getString(2)+"</option>");
    }
%>
    </select><br>
<h class='form-text'>DAYS AVAILABLE IN A WEEK :           <input type="checkbox" name="MON" value="MON">               <input type="checkbox" name="TUE" value="TUE">                <input type="checkbox" name="WED" value="WED">                 <input type="checkbox" name="THU" value="THU">
                                                     <label for="MON"> MONDAY </label><label for="TUE"> TUESDAY </label><label for="WED"> WEDNESDAY </label><label for="THU"> THURSDAY </label><br>
                                                                      <input type="checkbox" name="FRI" value="FRI">                <input type="checkbox" name="SAT" value="SAT">               <input type="checkbox" name="SUN" value="SUN">
                                                                 <label for="FRI"> FRIDAY </label><label for="SAT"> SATURDAY </label><label for="SUN"> SUNDAY </label><br>
                   <input class='form-button form-button1' type="submit" value="go"><br></pre>
</form>
</pre>
</body>
    </div>
</html>
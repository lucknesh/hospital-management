  <%@ page import="com.hospital.manage.database.*,java.sql.*,java.io.*,com.hospital.manage.users.*,com.hospital.manage.time.DateTime,com.hospital.manage.enums.DoctorSpecialisation,com.hospital.manage.enums.Days,com.hospital.manage.enums.DoctorType,java.util.*" %>
  <%@ page import="com.hospital.manage.users.Admin,com.hospital.manage.database.*" %>
<%@ page import="com.hospital.manage.users.*,com.hospital.manage.enums.*" %>
          <%

                User user = (User)session.getAttribute("user");
                Integer userId = user.getUserId();
                UserType userType = user.getUserType();
                String userName = user.getName();


                    String MON, TUE, WED, THU, FRI, SAT, SUN;

                    String name = request.getParameter("name");
                    String mobile = request.getParameter("mobile");
                    String email = request.getParameter("email");
                    String password = request.getParameter("password");
                    String specialisation = request.getParameter("specialisation");
                    Float consultationFee = Float.parseFloat(request.getParameter("consultationFee"));
                    MON = (request.getParameter("MON")!=null)? "MON":"0";
                    TUE = (request.getParameter("TUE")!=null)? "TUE":"0";
                    WED = (request.getParameter("WED")!=null)? "WED":"0";
                    THU = (request.getParameter("THU")!=null)? "THU":"0";
                    FRI = (request.getParameter("FRI")!=null)? "FRI":"0";
                    SAT = (request.getParameter("SAT")!=null)? "SAT":"0";
                    SUN = (request.getParameter("SUN")!=null)? "SUN":"0";
                    Integer experience = Integer.parseInt((String) request.getParameter("experience"));
                    String dateSincePractising = DateTime.yearBefore(experience);
                    DoctorType doctorType = DoctorType.valueOf(request.getParameter("type"));

                    Integer wardId = Integer.parseInt((String) request.getParameter("wardId"));

                    ArrayList<String> days = new ArrayList<>(List.of(MON, TUE, WED, THU, FRI, SAT, SUN));
                    ArrayList<Days> daysAvailable = new ArrayList<>();
                    days.forEach((x)->{
                        if(!x.equals("0")){
                            daysAvailable.add(Days.valueOf(x));
                        }
                    });
                    Doctor doctor = new Doctor();
                    doctor.setName(name);
                    doctor.setDoctorType(doctorType);
                    doctor.setSpecialisation(DoctorSpecialisation.valueOf(specialisation));
                    doctor.setMobile(mobile);
                    doctor.setEmail(email);
                    doctor.setDaysAvailable(daysAvailable);
                    doctor.setPassword(password);
                    doctor.setDateSincePractising(dateSincePractising);
                    doctor.setConsultationFee(consultationFee);

                    session.setAttribute("doctor",doctor);
  %>
<html>
<style>
.form_frame_doctor {
     height: 200px;
     width: 400px;

     position: fixed;
     top: 20%;
     left: 20%;
 }
</style>
<div>
    <head>
    <body style="background-color:#1f456e;">
    <link rel="stylesheet" type="text/css" href="design.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <div class="header">
            <a href="adminInterface"><div class="logo_holder"><h class="hospital"><b>CARE + </b></h></div></a>
            <div class="logout_holder">
                       <form action="Logout">
                    <input type="submit" value="Logout" class="button button1">
                </form>
                <div>
                </div><br>
                        <div class="header_bottom"><div class='name-holder'><% out.print(userType+" : "+userName); %></div></div>
                <title>new doctor</title>
    </head>
    <div class="form_frame_doctor">
<body>
    <form action="registerDoctor" method="post">
    <%
    if(doctorType.equals(DoctorType.VISITING_DOCTOR))
    {
    out.print("<pre><h1 class='form-text'>                                       FROM                                                          TO                                                  CABIN ID</h1></pre>");
    }
    else
    {
    out.print("<pre><h1 class='form-text'>                                       FROM                                                          TO                                                  </h1></pre>");
    }
        for(Days day : daysAvailable)
        {
            String dayOfWeek = day.toString().replaceAll("\\s", "");
            out.print("<br><pre>");
            out.print("<h class='form-text'> "+dayOfWeek+" </h><input class='get' type='time' value='09:00' name='"+dayOfWeek+"FROM'> <input class='get' type='time' value='18:00' name='"+dayOfWeek+"TO'>");
            if(doctorType.equals(DoctorType.VISITING_DOCTOR))
            {
                out.print("<label for='roomNumber'><h class='form-text'></h></label> <select class='get' name='"+dayOfWeek+"ID' >");
                try
                {
                ResultSet rs = WardDao.getAvailableCabins(wardId,dayOfWeek);
                while(rs.next())
                {
                out.print("<option value="+rs.getInt(1)+">"+rs.getInt(2)+"</option>");
                }
                }
                catch(Exception e)
                {
                    System.out.print(e);
                    e.printStackTrace();
                }
            }
            out.print("</select></pre>");
        }
    %>
                                        <br><input class='form-button form-button1' type="submit" value="go"><br></pre>
</form>
</body>
    </div>
</html>